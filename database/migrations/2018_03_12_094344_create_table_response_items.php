<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableResponseItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('response_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('supplier_id');
            $table->integer('request_item_id');
            $table->integer('request_id');

            $table->decimal('official_fee_currency',6,2)->nullable();; //Official Fee in Local Currency
            $table->decimal('official_fee_in_invoice',6,2)->nullable();; //Official Fee in Invoice Currency
            $table->decimal('official_fee_amount',6,2)->nullable();; //Local Costs in Local Currency
            $table->decimal('locale_cost_in_invoice',6,2)->nullable();; // Local Costs in Invoice Currency
            $table->decimal('service_fee_in_invoice',6,2)->nullable();; //Service Fee in Invoice Currency
            $table->decimal('total_charge_in_invoice',6,2)->nullable();; //Total Charge in Invoice Currency
            $table->date('local_cost_amount')->nullable();; //FX Currency Conversion Date
            $table->string('local_cost_currency')->nullable();; // FX Markup on Interbank Rate
            $table->text('comments')->nullable();; // Comments

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
