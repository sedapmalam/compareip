<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserSendRequestToResponses extends Migration
{
  /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::table('responses', function (Blueprint $table) {
                $table->integer('user_send_request_reminder')->default(0)->nullable();
                $table->integer('user_send_request_count')->default(0)->nullable();
            });
 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('responses', function (Blueprint $table) {
            $table->dropColumn('user_send_request_reminder');
            $table->dropColumn('user_send_request_count');
        });
    }

}