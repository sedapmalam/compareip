<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRequestItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('request_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_id');

            $table->string('ip_number')->nullable(); //IP number
            $table->string('item_number_code')->nullable(); //Item Number/Code
            $table->string('country')->nullable(); //Country
            $table->string('case_type')->nullable(); //Case Type
            $table->string('status', 32)->nullable(); //Status
            $table->string('entity_size')->nullable(); //Entity Size
            $table->boolean('license_of_right')->nullable(); //License of Right - Yes/No
            $table->integer('number_of_claims')->nullable(); //Number of Claims/Designs/Classes

            // field that not used, but can be in future
            $table->date('date_of_application_or_grant')->nullable(); //Date of Application or Grant
            $table->date('last_renewal_date')->nullable(); //Last Renewal Date
            $table->string('last_annuity_year')->nullable(); //Last Annuity Year
            //

            $table->date('next_renewal_date')->nullable(); //Next Renewal Date
            $table->string('next_annuity_year')->nullable(); //Next Annuity Year
            $table->string('invoice_currency', 3)->nullable(); //Invoice Currency

            // free columns that the client can name and use for their own purposes
            $table->string('additional_field_name_1')->nullable();
            $table->string('additional_field_name_2')->nullable();
            $table->string('additional_field_value_1')->nullable();
            $table->string('additional_field_value_2')->nullable();
            //

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
