<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnsInResponseItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('response_items', function (Blueprint $table) {

//            $table->renameColumn('locale_cost_in_local', 'official_fee_amount'); //OFFICIAL_FEE_AMOUNT
//            $table->renameColumn('official_fee_in_local', 'official_fee_currency'); //OFFICIAL_FEE_CURRENCY
//
//            $table->dropColumn('fx_currency_conversation_date'); //LOCAL_COST_AMOUNT
//            $table->dropColumn('fx_markup'); //LOCAL_COST_CURRENCY
//
            $table->decimal('local_cost_amount', 12,2)->nullable()->change(); //LOCAL_COST_AMOUNT
            $table->decimal('local_cost_currency', 12,2)->nullable()->change(); //LOCAL_COST_CURRENCY

            $table->string('official_fee_currency')->nullable()->change();
            $table->string('local_cost_currency')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
