<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserDownloadResponseStatusToResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('responses', function (Blueprint $table) {
            $table->integer('user_response_download_status')->default(0)->nullable();
            $table->integer('user_response_download_count')->default(0)->nullable();
            $table->integer('user_assistance_reminder_status')->default(0)->nullable();
            $table->integer('user_assistance_reminder_count')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('responses', function (Blueprint $table) {
            $table->dropColumn('user_response_download_status');
            $table->dropColumn('user_response_download_count');
            $table->dropColumn('user_assistance_reminder_status');
            $table->dropColumn('user_assistance_reminder_count');
        });
    }
}
