<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeMaxValueForDigitsColumnsInResponceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('response_items', function (Blueprint $table) {
            $table->decimal('official_fee_currency',12,2)->nullable()->change(); //Official Fee in Local Currency
            $table->decimal('official_fee_in_invoice',12,2)->nullable()->change(); //Official Fee in Invoice Currency
            $table->decimal('official_fee_amount',12,2)->nullable()->change(); //Local Costs in Local Currency
            $table->decimal('locale_cost_in_invoice',12,2)->nullable()->change(); // Local Costs in Invoice Currency
            $table->decimal('service_fee_in_invoice',12,2)->nullable()->change(); //Service Fee in Invoice Currency
            $table->decimal('total_charge_in_invoice',12,2)->nullable()->change(); //Total Charge in Invoice Currency
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
