<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProfiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('company')->nullable();
            $table->string('position')->nullable();
            $table->string('phone')->nullable();;

            $table->integer('number_of_registered_ip')->nullable();
            $table->string('patents')->nullable();
            $table->string('models')->nullable();
            $table->string('industrial_designs')->nullable();
            $table->string('trademarks')->nullable();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
