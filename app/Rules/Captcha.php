<?php
 
namespace App\Rules;
require_once '../vendor/google/recaptcha/src/autoload.php';
use Illuminate\Contracts\Validation\Rule;
use ReCaptcha\ReCaptcha;

 
class Captcha implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
 
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {  
        $captcha = new \ReCaptcha\ReCaptcha('6LcDp7YUAAAAABu9suh1RGi6GoIgD_jsI8IIwYZX');
        $response = $captcha->verify($value, $_SERVER['REMOTE_ADDR']);

        if ($response->isSuccess()) {
            return true;
        } else {
            return false;
        }
        return $response->isSuccess();
    }
 
    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Complete the reCAPTCHA to submit the form';
    }
}
