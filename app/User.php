<?php

namespace App;

use App\Models\Profiles;
use App\Models\RequestItems;
use App\Models\SuppliersRequests;
//use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    const ROLE_USER = 'user';
    const ROLE_ADMIN = 'admin';
    const ROLE_SUPPLIER = 'supplier';

    const APPROVE_STATUS_APPROVED = 'Approved';
    const APPROVE_STATUS_NOT_APPROVED = 'Not approved';

    const APPROVE_STATUS_FALSE = 0;
    const APPROVE_STATUS_TRUE = 1;

    const DISABLED_STATUS_FALSE = 0;
    const DISABLED_STATUS_TRUE = 1;

    const AGREE_STATUS_FALSE = 0;
    const AGREE_STATUS_TRUE = 1;

    use Notifiable;
    //use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'role', 'email', 'password', 'disabled', 'agree', 'approve_status', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function profile()
    {
        return $this->hasOne('App\Models\Profiles', 'user_id');
    }

    public function supplierRequest()
    {
        return $this->hasMany('App\Models\SuppliersRequests', 'supplier_id');
    }

    public function supplierAttachment()
    {
        return $this->hasMany('App\Models\AttachmentRequest', 'supplier_id');
    }

    public function response()
    {
        return $this->hasMany('App\Models\Responses', 'supplier_id');
    }

    static public function getAllSuppliers()
    {
        return self
            ::where('role', self::ROLE_SUPPLIER)
            ->where('agree', self::AGREE_STATUS_TRUE)
            ->where('approve_status', self::APPROVE_STATUS_TRUE)
            ->where('disabled', self::DISABLED_STATUS_FALSE)
            ->get();
    }

    static function getAllAdmins()
    {
        return self
            ::where('role', self::ROLE_ADMIN)
            ->where('agree', self::AGREE_STATUS_TRUE)
            ->where('approve_status', self::APPROVE_STATUS_TRUE)
            ->where('disabled', self::DISABLED_STATUS_FALSE)
            ->get();
    }

    static public function isApproved($email)
    {

        return self::where('email', $email)->first()->approve_status == self::APPROVE_STATUS_TRUE;

    }

    public function isSupplier()
    {
        return $this->role == self::ROLE_SUPPLIER;
    }

    public function isUser()
    {
        return $this->role == self::ROLE_USER;
    }

    public function isUserOrSupplier()
    {
        return $this->role == self::ROLE_SUPPLIER || $this->role == self::ROLE_USER;
    }

    public function isAdmin()
    {
        return $this->role == self::ROLE_ADMIN;
    }

    public function isAgree()
    {
        return $this->agree == self::AGREE_STATUS_TRUE;
    }

    public function isNotApproved()
    {
        return $this->approve_status == self::APPROVE_STATUS_NOT_APPROVED;
    }

    public function isDisabled()
    {
        return $this->disabled == self::DISABLED_STATUS_TRUE;
    }

    public function getApprovedStatus()
    {

        return $this->approve_status == self::APPROVE_STATUS_TRUE
            ? self::APPROVE_STATUS_APPROVED
            : self::APPROVE_STATUS_NOT_APPROVED;

    }

    public function getRoleName()
    {

        switch ($this->role) {
            case self::ROLE_USER :
                $role = self::ROLE_USER;
                break;
            case self::ROLE_SUPPLIER :
                $role = self::ROLE_SUPPLIER;
                break;
            case self::ROLE_ADMIN :
                $role = self::ROLE_ADMIN;
                break;
        }
        return $role;

    }

    public function setApproveStatus()
    {

        $this->update([
            'approve_status' => self::APPROVE_STATUS_TRUE
        ]);

    }

    public function setAndReturnPassword()
    {
        $password = str_random(8);

        $this->update([
            'password' => Hash::make($password)
        ]);

        return $password;
    }

    static public function createUserByAdmin($data)
    {
        return self::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => Hash::make(str_random(8)),
            'role' => $data['role'],
            'agree' => $data['agree'],
        ]);
    }

    static function createSupplierByAdmin($data)
    {
        return self::create([
            'email' => $data['email'],
            'password' => Hash::make(str_random(8)),
            'role' => $data['role'],
            'agree' => $data['agree'],
        ]);
    }


    public function updateUserByAdmin($data)
    {
        $this->update([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'role' => $data['role'],
            'agree' => $data['agree'],
        ]);
    }

    public function updateSupplierByAdmin($data)
    {
        $this->update([
            'email' => $data['email'],
            'role' => $data['role'],
            'agree' => $data['agree'],
        ]);
    }

    public function updateUser($data)
    {
        $this->update([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
        ]);
    }

    public function disable()
    {
        $this->update([
            'disabled' => self::DISABLED_STATUS_TRUE
        ]);
    }

    public function enable()
    {
        $this->update([
            'disabled' => self::DISABLED_STATUS_FALSE
        ]);
    }

    public function updatePassword($data)
    {
        return $this->update(
            [
                'password' => Hash::make($data['password']),
            ]);

    }

    public function setAgreements()
    {
        $this->update([
            'agree' => self::AGREE_STATUS_TRUE
        ]);
    }

    public function updateAvatar($fileName)
    {
        $this->update([
            'avatar' => $fileName
        ]);
    }

    public function getAvatarPath()
    {

        if ($this->avatar == Profiles::DEFAULT_IMAGE_NAME) {
            return asset(Profiles::PATH_TO_IMAGE . Profiles::DEFAULT_IMAGE_NAME);
        } else {
            return asset(Profiles::PATH_TO_IMAGE . $this->avatar);
        }

    }

    public function hasNote($requestId)
    {
        return $this->response->where('request_id', $requestId)->where('note', '<>', '')->count() != 0;
    }

    public function isResponded($requestId)
    {
        return $this->response->where('request_id', $requestId)->count() !== 0;
    }

    public function isDeclined($requestId)
    {
        return $this->supplierRequest
                ->where('request_id', $requestId)
                ->where('status', SuppliersRequests::STATUS_DECLINED)
                ->count() !== 0;
    }

    public function isActive()
    {
        return  $this->approve_status == self::APPROVE_STATUS_TRUE &&
                !$this->isDisabled() &&
                $this->isAgree();
    }

    /**
     * Gets date of registration.
     *
     * @author Alexey Kuzmenko <alexey.kuzmenko@greenice.net>
     *
     * @param null $format
     *
     * @return string
     */
    public function getDateOfRegistration($format = null)
    {
        return $format ? \Carbon\Carbon::parse($this->created_at)->format($format) : strtotime($this->created_at);
    }
}
