<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\RequestItems;
use App\Models\Requests;
use App\Models\ResponseItems;
use App\Models\Responses;
use App\Models\SuppliersRequests;
use App\Services\MailSender;
use App\User;
use App\Http\Requests\CreateRequest;
use http\Env\Request;
use App\Http\Controllers\RequestController;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class sendUserSelectSupplierReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:select-supplier';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /* ------------------- Reminder for user to select supplier ------------------ */
    
        $requests = Requests::get()->toArray();
       
        if(!is_array($requests))
        {
            return 'No request found';
        }
 
        if(count($requests) > 0 ){
           
        foreach($requests as $key=>$val){
          
          
           
           $user_submit_request_reminder = $val['user_submit_request_reminder'];
           $user_submit_request_count = $val['user_submit_request_count'];
           $supplier = SuppliersRequests::where('request_id',$val['id'])->get()->toArray();

           if(empty($supplier) || $supplier == [] || $supplier == null)
           {
           $user = User::find($val['user_id']);
           
           if($user != null || $user != [] || !empty($user)){
                $getRequest = Requests::find($val['id']);
               
                if($getRequest->user_select_supplier_count <= 3) {
                      
                        \Mail::send('email.sendReminderToSelectSupplier', 
                        [
                            'user' => $user,
            
                        ], function ($message) use ($user) {
                            $message->to($user->email, $user->first_name)
                                ->from(\Config::get('mail.from.address'), \Config::get('mail.from.name'))
                                ->subject(MailSender::SUBJECT_USER_SELECT_SUPPLIER_REMINDER);
                        });
                
                
            
                        if($getRequest){
                            $reminder = $getRequest->user_select_supplier_reminder;
                            $count = $getRequest->user_select_supplier_count;
                            $next_count = $count+1;

                             // $new_reminder = $reminder = 0 ? 1 : 0;


                            Requests::where('id',$val['id'])->update([
                                'user_select_supplier_reminder'=>1, 
                                'user_select_supplier_count'=> $next_count
                                ]);
                        } 

           } 

         }
       }

    }
}
}
}