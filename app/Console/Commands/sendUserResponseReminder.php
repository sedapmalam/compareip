<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\RequestItems;
use App\Models\Requests;
use App\Models\ResponseItems;
use App\Models\Responses;
use App\Models\SuppliersRequests;
use App\Services\MailSender;
use App\User;
use App\Http\Requests\CreateRequest;
use http\Env\Request;
use App\Http\Controllers\RequestController;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class sendUserResponseReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:response-reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    protected $getUser;
    protected $responses;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(User $getUser,Responses $responses)
    {
        parent::__construct();
        $this->getUser = $getUser;

        $this->responses = $responses;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /* ------------------- Reminder for user to to get response ------------------ */
    
        $user_requests = Requests::get()->toArray();
       
        if(!is_array($user_requests))
        {
            return 'No request found';
        }
 
        if(count($user_requests) > 0 ){
           
        foreach($user_requests as $key=>$val){


           $supplier = SuppliersRequests::where('request_id',$val['id'])->get()->toArray();
           $user = User::find($val['user_id']);
          
          
           if($user != null || $user != [] || !empty($user)){
              
               
                     
                // $responded = $this->responses->where('request_id',$val['id'])->where('responded','!=',null)->get()->toArray();
               
                // if($responded != null || $responded != []){

                    // $supplier_data = @$responded[0]['supplier_id'];
                    
                    
                    $response_download =  $this->responses->where('request_id',$val['id'])->get()->toArray();
                    
                    if($response_download != null || $response_download != [] || count($response_download) > 0) {

                      
                        foreach($response_download as $keys=>$values){


                             /* ------------------- Reminder for user to to download response ------------------ */
                            
                            $user_response_download_count = @$values['user_response_download_count'];
                            $user_response_download_status = @$values['user_response_download_status'];
                            $supplier_data = User::find(@$values['supplier_id']);
                           
                            if($user_response_download_count <= 3){
                                
                              
                                \Mail::send('email.sendReminderToGetResponseRequest', 
                                [
                                    'user' => @$user,
                                    'supplier'=>@$supplier_data
                                    
                                ], function ($message) use ($user) {
                                    $message->to($user->email, $user->first_name)
                                         ->from(\Config::get('mail.from.address'), \Config::get('mail.from.name'))
                                        ->subject(MailSender::SUBJECT_USER_RESPONSE_DOWNLOAD_REMINDER);
                                });
                
                              

                                $new_count = $user_response_download_count + 1;
                                $this->responses->where('request_id',$val['id'])->update(['user_response_download_status'=>1,'user_response_download_count'=>$new_count]);
                            }
                            

                             /* ------------------------- Send Assistance Reminder ------------------------ */
                            
                             $user_assistance_reminder_count = @$values['user_assistance_reminder_count'];
                             $user_assistance_reminder_status = @$values['user_assistance_reminder_status'];
                             
                            if($user_response_download_status == 1 && $user_assistance_reminder_count <= 3)
                            {    
                                
                                \Mail::send('email.sendAssistanceRequest', 
                                [
                                    'user' => @$user
                                    
                                ], function ($message) use ($user) {
                                    $message->to($user->email, $user->first_name)
                                        ->from(\Config::get('mail.from.address'), \Config::get('mail.from.name'))
                                        ->subject(MailSender::SUBJECT_USER_RESPONSE_ASSISTANCE_REMINDER);
                                });
                               
                                $new_user_assistance_count = $user_assistance_reminder_count + 1;
                                $this->responses->where('request_id',$val['id'])->update(['user_assistance_reminder_status'=>1,'user_assistance_reminder_count'=>$new_user_assistance_count]);

                            }
                           

                        }
                    }

              }
            
           } 
           

         }
       }

    }
    

