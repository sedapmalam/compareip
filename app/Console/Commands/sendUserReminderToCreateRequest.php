<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\RequestItems;
use App\Models\Requests;
use App\Models\ResponseItems;
use App\Models\Responses;
use App\Models\SuppliersRequests;
use App\Services\MailSender;
use App\User;
use App\Http\Requests\CreateRequest;
use http\Env\Request;
use App\Http\Controllers\RequestController;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class sendUserReminderToCreateRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create-request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /* ------------------- Reminder for user to create request ------------------ */
       

       
        

        $user_list = User::where('role','user')->get()->toArray();


       
        if(!is_array($user_list))
        {
            return 'No user found';
        }
        
        if(count($user_list) > 0 || $user_list != []){
    
        foreach($user_list as $key=>$val){

           $user_requests = Requests::where('user_id',$val['id'])->get()->toArray();
       
          
           if($user_requests == [] || $user_requests == null || empty($user_requests)){
                            
                            if($val['create_request_count'] <= 3)
                            {
                                
                                \Mail::send('email.sendReminderToCreateRequest', 
                                [
                                    'user' => @$val
                                    
                                ], function ($message) use ($val) {
                                    $message->to($val['email'], $val['first_name'])
                                        ->from(\Config::get('mail.from.address'),\Config::get('mail.from.name'))
                                        ->subject(MailSender::SUBJECT_USER_CREATE_REQUEST_REMINDER);
                                });
                                
                                $user_data = User::find($val['id']);
                                $count = $user_data->create_request_count;
                                $new_count = $count + 1;
                                $user_data->create_request_count = $new_count;
                                $user_data->create_request_reminder = 1;
                                $user_data->save();

                            }
                         }

                        }
                    }

              }
            
           } 

