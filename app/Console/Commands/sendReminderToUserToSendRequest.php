<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\RequestItems;
use App\Models\Requests;
use App\Models\ResponseItems;
use App\Models\Responses;
use App\Models\SuppliersRequests;
use App\Services\MailSender;
use App\User;
use App\Http\Requests\CreateRequest;
use http\Env\Request;
use App\Http\Controllers\RequestController;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class sendReminderToUserToSendRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:send-request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         /* ------------------- Reminder for user to send request ------------------ */
    
         $user_requests = Requests::get()->toArray();
       
         if(!is_array($user_requests))
         {
             return 'No request found';
         }
  
         if(count($user_requests) > 0 ){
            
         foreach($user_requests as $key=>$val){
 
            
           
            $supplier = SuppliersRequests::where('request_id',$val['id'])->get()->toArray();

            $response = Responses::where('request_id',$val['id'])->get()->toArray();
            $user = User::find($val['user_id']);

            if($supplier != [] || $supplier != null || !empty($supplier))
            {
            if($user != null || $user != [] || !empty($user)){
                 
                 if($response != null || $response  != [] || count($response) >0)
                 {     
                       foreach($response as $key=>$value) {
                           
                        if($value['user_send_request_count'] <= 3)
                         {  
                                \Mail::send('email.sendReminderToSendRequest', 
                                [
                                    'user' => $user,
                    
                                ], function ($message) use ($user) {
                                    $message->to($user->email, $user->first_name)
                                        ->from(\Config::get('mail.from.address'), \Config::get('mail.from.name'))
                                        ->subject(MailSender::SUBJECT_USER_SEND_REQUEST_REMINDER);
                                });
                            
                            $count = Responses::find($value['id']);
                            $old_count = $count->user_send_request_count;
                            $new_count = $old_count + 1;
                            Responses::where('id',$value['id'])->update([
                                'user_send_request_reminder'=> 1, 
                                'user_send_request_count'=> $new_count
                                ]);

                         }
                    
                        }
                            
                 } 
 
            } 
 
          }
        }
        }
 
     }
    }

