<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\RequestItems;
use App\Models\Requests;
use App\Models\ResponseItems;
use App\Models\Responses;
use App\Models\SuppliersRequests;
use App\Services\MailSender;
use App\User;
use App\Http\Requests\CreateRequest;
use http\Env\Request;
use App\Http\Controllers\RequestController;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class sendUserReminderToSubmitRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:submit-request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    protected $requests;
   
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(RequestController $requests)
    {
        parent::__construct();
        $this->requests = $requests;
        
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /* ------------------- Reminder for user to submit request ------------------ */
    
        $user_requests = Requests::where('status',Requests::STATUS_SAVE)->get()->toArray();
       
        if(!is_array($user_requests))
        {
            return 'No request found';
        }
 
        if(count($user_requests) > 0 ){
           
        foreach($user_requests as $key=>$val){

           
           $user_submit_request_reminder = $val['user_submit_request_reminder'];
           $user_submit_request_count = $val['user_submit_request_count'];
           $supplier = SuppliersRequests::where('request_id',$val['id'])->get()->toArray();
           $user = User::find($val['user_id']);
           
           if($user != null || $user != [] || !empty($user)){
           
              if($val['user_submit_request_count'] <=3){
               
                \Mail::send('email.sendReminderToSubmitRequest', 
                [
                    'user' => $user,
    
                ], function ($message) use ($user) {
                    $message->to($user->email, $user->first_name)
                        ->from(\Config::get('mail.from.address'), \Config::get('mail.from.name'))
                        ->subject(MailSender::SUBJECT_USER_REQUEST_SUBMIT_REMINDER);
                });
         
                $getRequest = Requests::find($val['id']);
            
                if($getRequest){
                    $reminder = $getRequest->user_submit_request_reminder;
                    $count = $getRequest->user_submit_request_count;
                    $next_count = $count+1;

                    


                    Requests::where('id',$val['id'])->update([
                        'user_submit_request_reminder'=>1, 
                        'user_submit_request_count'=> $next_count
                        ]);
                }
                
            }

           } 

         }
       }

    }
}