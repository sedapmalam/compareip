<?php

namespace App\Http\Requests;

use App\Models\ResponseItems;
use Illuminate\Foundation\Http\FormRequest;

class CreateResponse extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'items.*.official_fee_currency' => ResponseItems::OFFICIAL_FEE_CURRENCY,
            'items.*.official_fee_in_invoice' => ResponseItems::OFFICIAL_FEE_IN_INVOICE_CURRENCY,
            'items.*.official_fee_amount' => ResponseItems::OFFICIAL_FEE_AMOUNT,
            'items.*.locale_cost_in_invoice' => ResponseItems::LOCAL_COSTS_IN_INVOICE_CURRENCY,
            'items.*.service_fee_in_invoice' => ResponseItems::SERVICE_FEE_IN_INVOICE_CURRENCY,
            'items.*.total_charge_in_invoice' => ResponseItems::TOTAL_CHARGE_IN_INVOICE_CURRENCY,
            'items.*.local_cost_amount' => ResponseItems::LOCAL_COST_AMOUNT,
            'items.*.local_cost_currency' => ResponseItems::LOCAL_COST_CURRENCY,
        ];
    }

    public function messages()
    {
        return [
            'between' => 'Value must to be numeric'
        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'items.*.official_fee_currency' => 'nullable|string|max:3',
            'items.*.official_fee_in_invoice' => 'nullable|regex:/^[0-9]{1,8}+(\.[0-9]{1,2})?$/',
            'items.*.official_fee_amount' => 'nullable|regex:/^[0-9]{1,8}+(\.[0-9]{1,2})?$/',
            'items.*.locale_cost_in_invoice' => 'nullable|regex:/^[0-9]{1,8}+(\.[0-9]{1,2})?$/',
            'items.*.service_fee_in_invoice' => 'nullable|regex:/^[0-9]{1,8}+(\.[0-9]{1,2})?$/',
            'items.*.total_charge_in_invoice' => 'nullable|regex:/^[0-9]{1,8}+(\.[0-9]{1,2})?$/',
            'items.*.local_cost_amount' => 'nullable|regex:/^[0-9]{1,8}+(\.[0-9]{1,2})?$/',
            'items.*.local_cost_currency' => 'nullable|string|max:3',
            'note' => 'sometimes|nullable|string|max:4000',
        ];
    }
}
