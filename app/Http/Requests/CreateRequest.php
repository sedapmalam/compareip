<?php

namespace App\Http\Requests;

use App\Models\RequestItems;
use Illuminate\Foundation\Http\FormRequest;


class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'items.*.ip' => 'IP number',
            'items.*.item_number_code' => 'Item Number/Code',
            'items.*.country' => 'Country',
            'items.*.type' => 'Case Type',
            'items.*.status' => 'Status',
            'items.*.entity_size' => 'Entity Size',
            'items.*.number' => 'Number of Claims/Designs/Classes',
            'items.*.next_renewal_date' => 'Renewal due Date',
            'items.*.next_annuity_year' => 'Annuity Year',
            'items.*.invoice_currency' => 'Invoice Currency',
            'items.*.estimated_or_invoiced_total' => 'Estimated Or Invoiced Total',
            'reference' => 'Reference Name'
        ];
    }

    public function messages()
    {
        return [
            'items.*.estimated_or_invoiced_total.regex' => 'Value must to be numeric',
            'items.*.number.maxlength' => 'The :attribute must be shorter then 20 symbols',
            'items.*.next_annuity_year.required_unless' => 'The Annuity Year field is required',
            'items.*.next_renewal_date.date_multi_format' => 'Incorrect Renewal Due Date format (d/m/Y)',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'items.*.ip' => 'nullable|string|max:20',

            'items.*.item_number_code' => 'required|string|max:20',
            'items.*.country' => 'required|string|max:45',
            'items.*.type' => 'required|string|max:35',
            'items.*.status' => 'required|string|max:12',
            'items.*.entity_size' => 'nullable|string|max:10',

            'items.*.number' =>
                [
                    'nullable',
                    'regex:/^[0-9]{1,20}+$/',
                    function ($attribute, $value, $fail) {
                        if (strlen($value) > 20) {
                            return $fail('The Number of Claims/Designs/Classes may not be greater than 20 characters.');
                        }
                    }

                ],

            'items.*.next_renewal_date' => 'required|date_multi_format:"d/m/Y","d-m-Y","Y-m-d"|max:255',
            'items.*.next_annuity_year' =>
                [
                    'required_unless:items.*.type,Trademark,Trade mark,trade mark,trademark,Trade Mark',
                    'nullable',
                    'integer',
                    function ($attribute, $value, $fail) {
                        if (strlen($value) > 4) {
                            return $fail('The Annuity Year may not be greater than 4 characters.');
                        }
                    }

                ]
            ,
            'items.*.invoice_currency' => 'required|string|max:3',
            'items.*.estimated_or_invoiced_total' => 'regex:/^\d*(\.\d{1,2})?$/',
            'suppliers' => 'sometimes|required|array|exists:users,id',

            'reference' => 'nullable|string|max:30'

        ];
    }
}
