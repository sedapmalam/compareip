<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class UserData extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'avatar.max' => 'Max image size is 10MB'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'sometimes|required|string|max:255',
            'last_name' => 'sometimes|required|string|max:255',
            'role' => 'sometimes|required|in:'.User::ROLE_ADMIN.','.User::ROLE_SUPPLIER.','.User::ROLE_USER,
            'email' => 'required|string|email|max:255',
            'avatar' => 'sometimes|nullable|image|mimes:jpg,jpeg,png|max:10240',
            'number_of_registered_ip' => 'sometimes|required|integer',
            'organisation' => 'sometimes|required|string|max:255',
            'site' => 'sometimes|required|string|max:255',
            'business_name' => 'sometimes|required|string|max:255',
            'contact_name' => 'sometimes|required|string|max:255',
            'description' => 'sometimes|required|string|max:255',
            'agree' => 'sometimes',
            ];
    }
}
