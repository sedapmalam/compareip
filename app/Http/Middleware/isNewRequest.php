<?php

namespace App\Http\Middleware;

use App\Models\Requests;
use Closure;

class isNewRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $requestId = $request->route('requestId');

        $requestModel = Requests::findOrFail($requestId);

        if($requestModel->status !== Requests::STATUS_SAVE){
            return redirect()->route('request.index');
        }

        return $next($request);
    }
}
