<?php

namespace App\Http\Controllers\Auth;

use App\Models\Profiles;
use App\Services\MailSender;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
//use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Controllers\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use App\Rules\Captcha;
use App\Rules\ReCaptcha;
use App\Validators\ReCaptcha as ValidatorsReCaptcha;
use ReCaptcha\ReCaptcha as ReCaptchaReCaptcha;
use GuzzleHttp\Client;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/success';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {  
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'organisation' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'g-recaptcha-response' => 'required'
        ]);

        
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {  
        
        /* ------------------------- Google Recaptcha verify ------------------------ */

        $recaptcha = $data['g-recaptcha-response'];
        
        $client = new Client([
            'base_uri' => 'https://google.com/recaptcha/api/',
            'timeout' => 2.0
            ]);

        $response = $client->request('POST', 'siteverify', [
            'query' => [
            'secret' => '6LcDp7YUAAAAABu9suh1RGi6GoIgD_jsI8IIwYZX',
            'response' => $recaptcha]]);

        $res = json_decode($response->getBody());
      
        if($res->success != true){
          
            return redirect()->route('register');

        }
        
       
        $user = User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'role' => User::ROLE_USER,
            'password' => bcrypt(Hash::make(str_random(8))),
            'approve_status' => User::APPROVE_STATUS_FALSE
        ]);

        

        Profiles::create(
            [
                'user_id' => $user->id,
                'company' => $data['organisation'],
            ]
        );

        try {
            MailSender::sendMail(MailSender::TYPE_REGISTER, ['user' => $user]);
            MailSender::sendMail(MailSender::TYPE_ADMIN_NOTIFICATION_REGISTER_USER, [
                'subject' => 'New account has been created for user ' . $user->first_name . '(' . $user->email . ')',
                'user' => $user
            ]);

        } catch (\Exception $e) {

        }
       
        return $user;

    }

}
