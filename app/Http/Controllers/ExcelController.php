<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 03.04.18
 * Time: 11:54
 */

namespace App\Http\Controllers;

use App\Models\RequestItems;
use App\Models\ResponseItems;
use App\Services\Excel;
use Illuminate\Http\Request;
use App\Models\Responses;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Filesystem\Filesystem;

class ExcelController extends Controller
{

    private $excelReader;
    protected $responses;

    function __construct(Responses $responses)
    {
        $this->excelReader = new Excel();
        $this->responses = $responses;
    }

    public function downloadPattern()
    {

        $file = storage_path() . Excel::PATH_TO_PATTERN . Excel::PATTERN_NAME;

        return Response::download($file);

    }

    public function uploadForm(Request $request)
    {

        if (Input::hasFile('form')) {

            $extension = Input::file('form')->getClientOriginalExtension();
            $directory = storage_path() . Excel::PATH_TO_FORMS;
            $filename = Input::file('form')->getClientOriginalName() . ".{$extension}";
            $request->file('form')->move($directory, $filename);

            $data = $this->excelReader->getDataFromFile($directory . $filename, RequestItems::COLUMNS_IN_UPLOADED_FORM, RequestItems::COLUMNS_IN_UPLOADED_START);

            $file = new Filesystem;
            $file->cleanDirectory($directory);

            return  $this->excelReader->buildViewForCreationRequest($data);

        }


    }

    public function uploadResponse(Request $request, $requestId, $supplierId)
    {

        if (Input::hasFile('form')) {

            $extension = Input::file('form')->getClientOriginalExtension();
            $directory = storage_path() . Excel::PATH_TO_REQUEST;
            $filename = Input::file('form')->getClientOriginalName() . time() . ".{$extension}";
            $request->file('form')->move($directory, $filename);

            $data = $this->excelReader->getDataFromFile($directory . $filename, ResponseItems::COLUMNS_IN_UPLOADED_FORM, ResponseItems::COLUMNS_IN_UPLOADED_START);


            $file = new Filesystem;
            $file->cleanDirectory($directory);

            return  $this->excelReader->buildViewForCreationResponse($data, $requestId, $supplierId);

        }


    }

    public function downloadResponse($requestId)
    {  
    
        $this->excelReader->createExcelResponse($requestId);
        return Response::download(storage_path() . Excel::PATH_TO_EXPORT . Excel::EXPORT_FILE_NAME);

    }

    public function downloadRequest($requestId, $supplierId)
    {

        $this->excelReader->createExcelRequest($requestId, $supplierId);

        return Response::download(storage_path() . Excel::PATH_TO_REQUEST . Excel::EXPORT_FILE_NAME);

    }



}
