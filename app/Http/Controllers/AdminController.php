<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 16.03.18
 * Time: 12:38
 */

namespace App\Http\Controllers;

use App\Http\Requests\UserData;
use App\Models\Attachment;
use App\Models\Profiles;
use App\Services\MailSender;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Models\Responses;
use Symfony\Component\HttpFoundation\Request;
use \Image;


class AdminController extends Controller
{
    protected $responses;

    function __construct(Responses $responses)
    {
        $this->responses = $responses;
    }

    public function dashboard()
    {

        if (Auth::user()->isUser()) {

            return redirect()->route('request.index');
        }
        if (Auth::user()->isSupplier()) {

            return redirect()->route('response.index');
        }
        if (Auth::user()->isAdmin()) {

            return redirect()->route('admin.users.index');
        }
    }

    public function users()
    {
      //  $users['common'] = User::orderBy('id', 'DESC')->get();
       
        $users['user_approved'] = User::orderBy('id', 'DESC')->where('approve_status',1)->where('role','user')->get();
       
        $users['user_notapproved'] = User::orderBy('id', 'DESC')->where('approve_status',0)->where('role','user')->get();
        $users['supplier_approved'] = User::orderBy('id', 'DESC')->where('approve_status',1)->where('role','supplier')->get();
        $users['supplier_notapproved'] = User::orderBy('id', 'DESC')->where('approve_status',0)->where('role','supplier')->get();
       // echo "</pre>";
       // print_r($users);die;

        return view('admin.users.index', ['users' => $users]);
    }

    public function approveUser(Request $request)
    {

        $user = User::findOrFail($request->get('userId'));
        $user->setApproveStatus();

        $password = $user->setAndReturnPassword();

        if ($user->role == User::ROLE_USER) {

            MailSender::sendMail(MailSender::TYPE_APPROVE_USER,
                [
                    'password' => $password,
                    'user' => $user,
                ]);
        }

        if ($user->role == User::ROLE_SUPPLIER) {

            MailSender::sendMail(MailSender::TYPE_APPROVE_SUPPLIER,
                [
                    'password' => $password,
                    'user' => $user,
                ]);
        }

        MailSender::sendMail(MailSender::TYPE_ADMIN_NOTIFICATION_APPROVE_USER, [
            'subject' => 'New account has been approved for user ' . $user->first_name . '(' . $user->email . ')',
            'user' => $user
        ]);

        return redirect()->route('admin.users.index');

    }

    public function removeUsers(Request $request)
    {
        User::findOrFail($request->get('userId'))->delete();

        return redirect()->route('admin.users.index');

    }

    public function newUser()
    {
        return view('admin.users.create');
    }

    public function createUser(UserData $request)
    {
        $profile = new Profiles();

        $validator = Validator::make($request->all(), [
            'email' => 'unique:users',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $request->validate([
            'email' => 'unique:users',
        ]);

        if ($request->get('role') == User::ROLE_USER) {
            $user = User::createUserByAdmin($request->all());
        }

        if ($request->get('role') == User::ROLE_SUPPLIER) {
            $user = User::createSupplierByAdmin($request->all());
        }

        if ($request->get('role') == User::ROLE_ADMIN) {
            $user = User::createUserByAdmin($request->all());
        }

        if ($user->isUser()) {

            $profile->createOrUpdateProfile($request->all(), $user->id);

        } elseif ($user->isSupplier()) {

            $profile->createOrUpdateSupplierProfile($request->all(), $user->id);

        } elseif($user->isAdmin()) {

            $profile->createEmpty($user->id);

        }

        if ($user->isUser()) {

            MailSender::sendMail(MailSender::TYPE_REGISTER, ['user' => $user]);

        }
        return redirect()->route('admin.users.index');
    }

    public function editUser($userId)
    {

        $user = User::findOrFail($userId);

        return view('admin.users.update', ['user' => $user]);

    }

    public function updateUser(UserData $request)
    {
        $user = User::findOrFail($request->get('userId'));
        $userRole = $request->get('role');

        if ($userRole == User::ROLE_USER) {
            $user->updateUserByAdmin($request->all());
            $user->profile->createOrUpdateProfile($request->all(), $user->id);
        }

        if ($userRole == User::ROLE_SUPPLIER) {
            $user->updateSupplierByAdmin($request->all());
            $user->profile->createOrUpdateSupplierProfile($request->all(), $user->id);
        }

        if ($userRole == User::ROLE_ADMIN) {
            $user->updateUserByAdmin($request->all());
        }

        if ($request->hasFile('logo')) {

            $logo = $request->file('logo');
            $filename = time() . '.' . $logo->getClientOriginalExtension();

            Image::make($logo)->save(public_path('/uploads/avatars/' . $filename));
            $user->updateAvatar($filename);
        }

        return redirect()->route('admin.users.index');
    }

    public function disableUser(Request $request)
    {

        $user = User::findOrFail($request->get('userId'));

        $user->disable();

        return redirect()->route('admin.users.index');
    }

    public function enableUser(Request $request)
    {
        $user = User::findOrFail($request->get('userId'));

        $user->enable();

        return redirect()->route('admin.users.index');
    }

    public function changePasswordUser($userId)
    {

        return view('admin.users.reset-password',
            [
                'userId' => $userId
            ]);
    }

    public function updatePasswordUser(Request $request)
    {

        $request->validate([
            'password' => 'required|string|min:6|confirmed'
        ]);

        $user = User::findOrFail($request->get('userId'));

        $user->updatePassword($request->all());

        MailSender::sendMail(MailSender::TYPE_PASSWORD_CHANGE,
            [
                'user' => $user,
                'password' => $request->get('password')
            ]
        );

        return redirect()->route('dashboard');
    }

    public function agree()
    {

        return view('admin.agree',
            [
                'userId' => Auth::user()->id
            ]);
    }

    public function setAgree(Request $request)
    {
        $user = User::findOrFail($request->get('userId'));

        $user->setAgreements();

        return redirect()->route('dashboard');

    }

    public function download($attachmentId=null,$requestId = null)
    {
        
        if($requestId != null)
        {
            $response_download =  $this->responses->where('request_id',$requestId)->get()->toArray();
            
            if($response_download != null || $response_download != []) {
        
                $user_response_download_count = @$response_download[0]['user_response_download_count'];
                $user_response_download_status = @$response_download[0]['user_response_download_status'];
                $new_count = $user_response_download_count + 1;
                $this->responses->where('request_id',$requestId)->update(['user_response_download_status'=>1,'user_response_download_count'=>$new_count]);

            }
        }
        
        if($attachmentId != null)
        {
         $file = storage_path() . Attachment::PATH_TO_FILE . Attachment::findOrFail($attachmentId)->name;
        }
        else
        {
            $file = null;
        }
        return Response::download($file);
    }

    public function getSupplierInfo($profileId)
    {

        $profile = Profiles::findOrFail($profileId);

        return view('admin.request.supplier',
            [
                'site' => $profile->site
            ]);

    }

}
