<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 13.03.18
 * Time: 12:37
 */

namespace App\Http\Controllers;

use App\Models\RequestItems;
use App\Models\Requests;
use App\Models\ResponseItems;
use App\Models\Responses;
use App\Models\SuppliersRequests;
use App\Services\MailSender;
use App\User;
use App\Http\Requests\CreateRequest;
use http\Env\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;


class RequestController extends Controller
{

    public function index($userId = null)
    {
        $userId = $userId == null
            ? Auth::user()->id
            : $userId;

        return view('admin.request.index',
            [
                'requests' => Requests::where('user_id', $userId)->orderBy('id', 'DESC')->get(),
            ]);
    }

    public function new()
    {
        $suppliers = User::getAllSuppliers();


        return view('admin.request.new',
            [
                'suppliers' => $suppliers,
                'request' => new Requests(),
            ]);
    }

    public function create(CreateRequest $request)
    {   
         
        if ($request->get('action') == RequestItems::FORM_OPTION_SAVE) {
            
           
            
            if ($request->get('requestId')) {
               
                $requestModel = Requests::find($request->get('requestId'));
               
                $choose_supplier_reminder_count = @$requestModel['user_select_supplier_count']; 
                $new_count = $choose_supplier_reminder_count + 1;
                $requestModel->update(['reference' => $request->get('reference'),'user_select_supplier_reminder'=>1,'user_select_supplier_count'=>$new_count]);

                RequestItems::removeDeletedItems($request->get('items'), $requestModel->id);

            } else {
               
                $requestModel = Requests::new(Auth::user()->id, Requests::STATUS_SAVE, $request->get('reference'));
                Requests::where('id',$requestModel->id)->update(['user_select_supplier_reminder'=>1,'user_select_supplier_count'=>1]);
            }

            

            RequestItems::updateItems($request->get('items'), $requestModel->id);

            return redirect(route('request.index'));
        } else {
            
            
         
            
            if ($request->get('requestId')) {
                $requestModel = Requests::find($request->get('requestId'));

                $requestModel->update([
                    'status' => Requests::STATUS_NEW,
                    'received_at' => Carbon::now(),
                    'reference' => $request->get('reference')
                ]);

                RequestItems::removeDeletedItems($request->get('items'), $requestModel->id);

            } else {

                $requestModel = Requests::new(Auth::user()->id, Requests::STATUS_NEW, $request->get('reference'));

            }


            SuppliersRequests::createConnections($request->get('suppliers'), $requestModel->id);

            foreach ($request->get('suppliers') as $supplierId) {

                MailSender::sendMail(MailSender::TYPE_RECEIVED_SUPPLIER,
                    [
                        'user' => User::find($supplierId),
                        'requestUrl' => route('response.request', ['requestId' => $requestModel->id,'supplierId' => $supplierId])
                    ]);

            }

            RequestItems::createItems($request->get('items'), $requestModel->id);

            return redirect()->route('request.index')->with('status', RequestItems::VIEW_STATUS_SANDED);

        }

    }

    public function forward($requestId)
    {
        $request = Requests::findOrFail($requestId);
        $items = $request->items;
        $suppliers = User::getAllSuppliers();
       
        return view('admin.request.forward', [
            'request' => $request,
            'items' => $items,
            'suppliers' => $suppliers
        ]);
    }

    public function getItems($requestId)
    {

        $request = Requests::findOrFail($requestId);
        $items = $request->items;
        $suppliers = $request->suppliers;
        $responses = SuppliersRequests::where('request_id', $request->id)->get();

        return view('admin.request.items',
            [
                'items' => $items,
                'suppliers' => $suppliers,
                'responses' => $responses,
                'request' => $request
            ]);

    }

    public function getItem($itemId)
    {
//        if ($request->statusReplay()) {
//
//            foreach ($suppliers as $supplier) {
//
//                MailSender::sendMail(MailSender::TYPE_QUOTE_OPENED,
//                    [
//                        'user' => $supplier,
//                        'reference' => route('response.request', ['requestId' => $request->id, 'supplierId' => $supplier->id]),
//                        'now' => Carbon::now()
//                    ]);
//
//            }
//
//            $request->setStatusViewed();
//        }

        $item = RequestItems::findOrFail($itemId);

        $responses = ResponseItems::where('request_item_id', $itemId)
            ->whereHas("response", function ($q) {
                $q->where("status", "=", Responses::STATUS_SEND);
            })->get();


        return view('admin.request.item',
            [
                'item' => $item,
                'responses' => $responses,
            ]);

    }

    public function resend($requestId, $supplierId)
    {

        $items = RequestItems::where('request_id', $requestId)->get();
        $request = Requests::findOrFail($requestId);

        return view('admin.request.resend',
            [
                'items' => $items,
                'request' => $request,
                'supplierId' => $supplierId
            ]);

    }

    public function edit($requestId){

        $items = RequestItems::where('request_id', $requestId)->get();
        $suppliers = User::getAllSuppliers();

        foreach ($items as $item){
            $request = $item->request;
        }

        return view('admin.request.new',
            [
                'suppliers' => $suppliers,
                'items' => $items,
                'requestId' => $requestId,
                'request' => $request
            ]);

    }


}
