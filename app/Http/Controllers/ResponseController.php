<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 13.03.18
 * Time: 12:37
 */

namespace App\Http\Controllers;

use App\Http\Requests\CreateResponse;
use App\Models\Attachment;
use App\Models\AttachmentRequest;
use App\Models\RequestItems;
use App\Models\Requests;
use App\Models\ResponseItems;
use App\Models\Responses;
use App\Models\SuppliersRequests;
use App\Services\MailSender;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\Request;

class ResponseController extends Controller
{

    public function index($userId = null)
    {

        $userId = $userId == null
            ? Auth::user()->id
            : $userId;

        $requests = SuppliersRequests::getRequestForSuppliers($userId);

        return view('admin.response.index',
            [
                'suppliersRequests' => $requests,
                'supplierId' => $userId
            ]
        );

    }

    /**
     * List of Supplier Response.
     *
     * @author Alexey Kuzmenko <alexey.kuzmenko@greenice.net>
     *
     * @param $userId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function supplier($userId)
    {
        $requests = SuppliersRequests::getAllRequests($userId);

        return view('admin.response.supplier', [
            'suppliersRequests' => $requests,
            'supplierId' => $userId
        ]);
    }

    public function getRequest($requestId =null, $supplierId=null)
    {    
            
            $items = RequestItems::where('request_id', $requestId)->get();
        
            $request = Requests::findOrFail($requestId);

            $attachmentsRequests = AttachmentRequest
                ::where('request_id', $requestId)
                ->where('supplier_id', $supplierId)
                ->get();

            $response = Responses::
            where('supplier_id', $supplierId)
                ->where('request_id', $requestId)
                ->first();


        return view('admin.response.request',
            [
                'items' => $items,
                'request' => $request,
                'response' => $response,
                'attachmentsRequests' => $attachmentsRequests,
                'supplierId' => $supplierId
            ]);
    }


    public function send(CreateResponse $request)
    {

        $requestModel = Requests::findOrFail($request->get('requestId'));

        if ($request->get('action') == Responses::STATUS_SEND) {

            $response = Responses::createOrUpdate(
                Auth::user()->id,
                $requestModel->id,
                $request->get('note'),
                Responses::STATUS_SEND
            );

            $response->update(
                [
                    'responded' => Carbon::now()
                ]
            );

            ResponseItems::createOrUpdate(
                $request->get('items'),
                $requestModel->id,
                Auth::user()->id,
                $response->id
            );

            MailSender::sendMail(MailSender::TYPE_RECEIVED,
                [
                    'user' => $requestModel->user,
                    'supplier' => Auth::user(),
                    'linkToRequest' => route('request.items', ['requestId' => $requestModel->id])

                ]
            );

            if ($requestModel->allRequestAnswered()) {

                MailSender::sendMail(MailSender::TYPE_ALL_DONE,
                    [
                        'user' => $requestModel->user,
                    ]
                );
            }

            return redirect()->route('response.index')->with('status', Responses::VIEW_STATUS_SANDED);

        } elseif ($request->get('action') == Responses::STATUS_STORED ||
            $request->get('action') == Responses::ACTION_EXIT) {


            $response = Responses::createOrUpdate(
                Auth::user()->id,
                $requestModel->id,
                $request->get('note'),
                Responses::STATUS_STORED
            );

            ResponseItems::createOrUpdate(
                $request->get('items'),
                $requestModel->id,
                Auth::user()->id,
                $response->id);


            if ($request->get('action') == Responses::STATUS_STORED) {

                return redirect()->back();

            } elseif ($request->get('action') == Responses::ACTION_EXIT) {

                return redirect()->route('response.index');

            }


        }


    }

    public function decline(Request $request)
    {

        $requestSupplier = SuppliersRequests::findOrFail($request->get('requestSupplierId'));

        $requestSupplier->decline();

        MailSender::sendMail(MailSender::TYPE_DECLINED,
            [
                'user' => $requestSupplier->request->user,

                'supplier' => $requestSupplier->supplier,

                'dateDeclined' => $requestSupplier->updated_at,
            ]);

        return redirect(route('response.index'));
    }

    public function attachments(Request $request)
    {

        $request->validate(
            [
                'file' => 'sometimes|file',
            ]
        );


        if ($request->hasFile('file')) {

            $extension = Input::file('file')->getClientOriginalExtension();
            $directory = storage_path() . Attachment::PATH_TO_FILE;
            $filename = Input::file('file')->getClientOriginalName();

            $request->file('file')->move($directory, $filename);

            $attachment = Attachment::create(
                [
                    'name' => $filename,
                ]
            );

            AttachmentRequest::create(
                [
                    'supplier_id' => Auth::user()->id,
                    'request_id' => $request->get('requestId'),
                    'attachment_id' => $attachment->id
                ]);
        }

        return redirect()->back();
    }


    public function removeAttachment(Request $request)
    {

        Attachment::findOrFail($request->get('attachmentId'))->delete();
        AttachmentRequest::where('attachment_id', $request->get('attachmentId'))->first()->delete();

        return redirect()->back();

    }


    
}
