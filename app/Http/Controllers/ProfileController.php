<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 13.03.18
 * Time: 12:37
 */

namespace App\Http\Controllers;

use App\Http\Requests\UserData;

use Illuminate\Support\Facades\Auth;
use App\User;
use \Image;

class ProfileController extends Controller
{

    public function index()
    {

        return view('admin.profile.index',
            [
                'user' => Auth::user()
            ]
        );

    }

    public function update(UserData $request)
    {

        $user = User::findOrFail($request->get('userId'));

        if ($user->isUser()) {
            $user->updateUser($request->all());
            $user->profile->createOrUpdateProfile($request->all(), $user->id);
        }

        if ($user->isSupplier()) {
            $user->update(
                [
                    'email' => $request->get('email'),
                ]
            );
            $user->profile->where('user_id', $user->id)->update(
                [
                    'site' => $request->get('site'),
                    'business_name' => $request->get('business_name'),
                    'contact_name' => $request->get('contact_name'),
                    'description' =>  $request->get('description'),
                ]
            );
        }

        $this->updateAvatar($request);

        return redirect()->route('profile.index');
    }

    public function updateAvatar(UserData $request)
    {

        // Logic for user upload of avatar
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');

            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(128, 128)->save(public_path('/uploads/avatars/' . $filename));

            Auth::user()->updateAvatar($filename);

        }

    }

}