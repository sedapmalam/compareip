<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 16.03.18
 * Time: 12:38
 */

namespace App\Http\Controllers;


use App\Services\MailSender;
use Symfony\Component\HttpFoundation\Request;


class LandingController extends Controller
{

    public function main()
    {
        return view('landing.main');
    }

    public function contact()
    {
        return view('landing.contact');
    }

    public function faqs()
    {
        return view('landing.faqs');
    }

    public function terms()
    {
        return view('landing.terms');
    }

    public function policy()
    {
        return view('landing.policy');
    }

    public function success()
    {
        return view('landing.success');
    }

    public function disabled()
    {
        return view('landing.disabled');
    }

    public function sendEmailFromLanding(Request $request)
    {

        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email',
            'subject' => 'required|string',
            'message' => 'required|string'
        ]);


        MailSender::sendMail(MailSender::TYPE_LETTER_FROM_LANDING,
            [
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'subject' => $request->get('subject'),
                'message' => $request->get('message')
            ]
        );


        return redirect()->route('home');

    }

}