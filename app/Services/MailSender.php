<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 20.03.18
 * Time: 15:55
 */

namespace App\Services;


use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use App\User;

class MailSender
{
    const TYPE_APPROVE_USER = 'approve';
    const TYPE_REGISTER = 'register_user';
    const TYPE_DECLINED = 'declined';
    const TYPE_RECEIVED = 'received';
    const TYPE_ALL_DONE = 'all_done';
    const TYPE_APPROVE_SUPPLIER = 'register_supplier';
    const TYPE_RECEIVED_SUPPLIER = 'received_to_supplier';
    const TYPE_QUOTE_OPENED = 'quote_opened';
    const TYPE_PASSWORD_CHANGE = 'pass_change';
    const TYPE_LETTER_FROM_LANDING = 'landing';
    const TYPE_ADMIN_NOTIFICATION_REGISTER_USER = 'admin_notification_register_user';
    const TYPE_ADMIN_NOTIFICATION_APPROVE_USER = 'admin_notification_approve_user';
    const TYPE_USER_REQUEST_SUBMIT_REMINDER = 'user_notification_submit_request';
    const TYPE_USER_SELECT_SUPPLIER_REMINDER = 'user_notification_select_supplier';

    const SUBJECT_APPROVE_USER = 'Your account has been verified';
    const SUBJECT_APPROVE_SUPPLIER = 'You are now authorized as a Supplier';
    const SUBJECT_REGISTER = 'You registered an account';
    const SUBJECT_DECLINED = 'Your quote has been declined';
    const SUBJECT_RECEIVED = 'Quote response has been received';
    const SUBJECT_ALL_DONE = 'All quote requests have been answered or declined';
    const SUBJECT_RECEIVED_SUPPLIER = 'Quote request has been sent to you for response';
    const SUBJECT_QUOTE_OPENED = 'Quote has been opened/viewed';
    const SUBJECT_PASSWORD_CHANGE = 'Your password had been changed';
    const SUBJECT_LETTER_FROM_LANDING = 'New message';
    const SUBJECT_USER_REQUEST_SUBMIT_REMINDER = 'You need to submit Requests to get things moving.';
    const SUBJECT_USER_SELECT_SUPPLIER_REMINDER = 'Please select Suppliers and Send your Request';
    const SUBJECT_USER_RESPONSE_DOWNLOAD_REMINDER = 'Supplier Responses have been delivered';
    const SUBJECT_USER_RESPONSE_ASSISTANCE_REMINDER = 'CompareIP.com - Report downloaded';
    const SUBJECT_USER_SEND_REQUEST_REMINDER = 'You need to send your Request so that Suppliers can respond.';
    const SUBJECT_USER_CREATE_REQUEST_REMINDER = 'You need to create a Request to get things moving.';
    
    static public function sendMail($type, $args)
    {
       
        switch ($type) {
            case self::TYPE_APPROVE_USER :
                self::sendApproveUserLetter($args);
                break;

            case self::TYPE_REGISTER :
                self::sendRegisterLetter($args);
                break;

            case self::TYPE_DECLINED :
                self::sendDeclinedLetter($args);
                break;

            case self::TYPE_RECEIVED :
                self::sendReceivedLetter($args);
                break;

            case self::TYPE_ALL_DONE :
                self::sendAllDoneLetter($args);
                break;

            case self::TYPE_APPROVE_SUPPLIER :
                self::sendApproveSupplierLetter($args);
                break;

            case self::TYPE_RECEIVED_SUPPLIER :
                self::sendReceivedToSupplierLetter($args);
                break;

            case self::TYPE_QUOTE_OPENED :
                self::sendQuoteOpenedLetter($args);
                break;

            case self::TYPE_PASSWORD_CHANGE :
                self::changePasswordLetter($args);
                break;

            case self::TYPE_LETTER_FROM_LANDING :
                self::formLandingLetter($args);
                break;

            case self::TYPE_ADMIN_NOTIFICATION_REGISTER_USER :
                self::sendAdminNotificationRegisterUser($args);
                break;

            case self::TYPE_ADMIN_NOTIFICATION_APPROVE_USER :
                self::sendAdminNotificationApproveUser($args);
                break;
            

        }

    }

    
    static public function sendApproveUserLetter($args)
    {

        $user = $args['user'];

        Mail::send('email.approveUser',
            [
                'user' => $user,
                'password' => $args['password']

            ], function ($message) use ($user) {
                $message->to($user->email, $user->first_name)
                    ->from(Config::get('mail.from.address'), Config::get('mail.from.name'))
                    ->subject(self::SUBJECT_APPROVE_USER);
            });

    }

    static public function sendRegisterLetter($args)
    {

        $user = $args['user'];

        Mail::send('email.register',
            [
                'user' => $user,

            ], function ($message) use ($user) {
                $message->to($user->email, $user->first_name)
                    ->from(Config::get('mail.from.address'), Config::get('mail.from.name'))
                    ->subject(self::SUBJECT_REGISTER);
            });

    }

    static public function sendDeclinedLetter($args)
    {

        $user = $args['user'];
        $supplier = $args['supplier'];

        Mail::send('email.declined',
            [
                'user' => $user,
                'supplier' => $supplier,
                'dateDeclined' => $args['dateDeclined'],

            ], function ($message) use ($user) {
                $message->to($user->email, $user->first_name)
                    ->from(Config::get('mail.from.address'), Config::get('mail.from.name'))
                    ->subject(self::SUBJECT_DECLINED);
            });

    }

    static public function sendReceivedLetter($args)
    {

        $user = $args['user'];
        $supplier = $args['supplier'];
        $linkToRequest = $args['linkToRequest'];

        Mail::send('email.received',
            [
                'user' => $user,
                'supplier' => $supplier,
                'linkToRequest' => $linkToRequest

            ], function ($message) use ($user) {
                $message->to($user->email, $user->first_name)
                    ->from(Config::get('mail.from.address'), Config::get('mail.from.name'))
                    ->subject(self::SUBJECT_RECEIVED);
            });

    }

    static public function sendAllDoneLetter($args)
    {

        $user = $args['user'];

        Mail::send('email.allDone',
            [
                'user' => $user,

            ], function ($message) use ($user) {
                $message->to($user->email, $user->first_name)
                    ->from(Config::get('mail.from.address'), Config::get('mail.from.name'))
                    ->subject(self::SUBJECT_ALL_DONE);
            });

    }

    static public function sendApproveSupplierLetter($args)
    {

        $user = $args['user'];

        Mail::send('email.approveSupplier',
            [
                'user' => $user,
                'password' => $args['password']

            ], function ($message) use ($user) {
                $message->to($user->email, $user->first_name)
                    ->from(Config::get('mail.from.address'), Config::get('mail.from.name'))
                    ->subject(self::SUBJECT_APPROVE_SUPPLIER);
            });

    }

    static public function sendReceivedToSupplierLetter($args)
    {

        $user = $args['user'];
        $requestUrl = $args['requestUrl'];
        
        Mail::send('email.receivedToSupplier',
            [
                'user' => $user,
                'requestUrl' => $requestUrl

            ], function ($message) use ($user) {
                $message->to($user->email, $user->first_name)
                    ->from(Config::get('mail.from.address'), Config::get('mail.from.name'))
                    ->subject(self::SUBJECT_RECEIVED_SUPPLIER);
            });

    }

    static public function sendQuoteOpenedLetter($args)
    {

        $user = $args['user'];

        Mail::send('email.quoteOpened',
            [
                'user' => $user,
                'reference' => $args['reference'],
                'now' => $args['now']

            ], function ($message) use ($user) {
                $message->to($user->email, $user->first_name)
                    ->from(Config::get('mail.from.address'), Config::get('mail.from.name'))
                    ->subject(self::SUBJECT_QUOTE_OPENED);
            });

    }

    static public function changePasswordLetter($args)
    {

        $user = $args['user'];

        Mail::send('email.passwordChange',
            [
                'password' => $args['password'],
                'user' => $user,

            ], function ($message) use ($user) {
                $message->to($user->email, $user->first_name)
                    ->from(Config::get('mail.from.address'), Config::get('mail.from.name'))
                    ->subject(self::SUBJECT_PASSWORD_CHANGE);
            });

    }

    static public function formLandingLetter($args)
    {


        $userEmail  = \config('app.contactEmail');

        Mail::send('email.fromLanding',
            [
                'name' => $args['name'],
                'email' => $args['email'],
                'messageFromLanding' => $args['message']

            ], function ($message) use ($userEmail, $args) {
                $message->to($userEmail)
                    ->from(Config::get('mail.from.address'), Config::get('mail.from.name'))
                    ->subject($args['subject']);
            });

    }

    static public function sendAdminNotificationRegisterUser($args)
    {
        $admins = User::getAllAdmins();

        foreach ($admins as $admin) {
            $emailParams = $args + ['admin' => $admin];

            Mail::send('email.adminNotificationRegisterUser', $emailParams, function ($message) use ($admin, $args) {
                $message->to($admin->email)
                    ->from(Config::get('mail.from.address'), Config::get('mail.from.name'))
                    ->subject($args['subject']);
            });
        }
    }

    static public function sendAdminNotificationApproveUser($args)
    {
        $admins = User::getAllAdmins();

        foreach ($admins as $admin) {
            $emailParams = $args + ['admin' => $admin];

            Mail::send('email.adminNotificationApproveUser', $emailParams, function ($message) use ($admin, $args) {
                $message->to($admin->email)
                    ->from(Config::get('mail.from.address'), Config::get('mail.from.name'))
                    ->subject($args['subject']);
            });
        }
    }
}
