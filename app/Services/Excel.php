<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 20.03.18
 * Time: 15:55
 */

namespace App\Services;

use App\Models\RequestItems;
use App\Models\Requests;
use App\Models\ResponseItems;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use \PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Facades\View;

class Excel
{
    const PATTERN_NAME = 'CompareIP_Template.xlsx';
    const PATH_TO_PATTERN = '/pattern/';
    const PATH_TO_FORMS = '/forms/';
    const PATH_TO_EXPORT = '/export/';
    const PATH_TO_REQUEST = '/request/';
    const FORMAT = 'Xlsx';
    const EXPORT_FILE_NAME = 'report.xlsx';

    const MAIN_REPORT_NAME = 'Consolidated Report';
    const MAIN_DOWNLOADED_REQUEST_NAME = 'Request';

    public function getDataFromFile($file, $columnNumber, $columnStart)
    {
        $format = ucfirst(strtolower(pathinfo($file, PATHINFO_EXTENSION)));
        $reader = IOFactory::createReader($format);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($file);

        $columnsValue = [];
        $itemCount = 2; // start from 2, because in first line placed column name

        return $this->getDataFromDocumentData($spreadsheet, $columnsValue, $itemCount, $columnStart, $columnNumber);
    }

    private function getDataFromDocumentData($data, $columnsValue, $itemCount, $columnStart, $columnNumber)
    {
        $emptyFieldsCount = 0;

        for ($i = $columnStart; $i <= $columnNumber + 1; $i++) {

            $value = $data->getActiveSheet()->getCellByColumnAndRow($i, $itemCount)->getValue();

            if (strstr($value, '=') == true) {
                $value = $data->getActiveSheet()->getCellByColumnAndRow($i, $itemCount)->getOldCalculatedValue();
            }

            if ($columnStart == RequestItems::COLUMNS_IN_UPLOADED_START && $i == 2) {
                $value = $this->convertDateValue($value);
            }

            $columnsValue[$itemCount][$i] = $value;

        }


        for ($i = 1; $i <= $columnNumber; $i++) {
            if ($data->getActiveSheet()->getCellByColumnAndRow($i, $itemCount)->getValue() != 0) {

                $itemCount++;

                return $this->getDataFromDocumentData($data, $columnsValue, $itemCount, $columnStart, $columnNumber);

            } else {
                $emptyFieldsCount++;
            }

        }


        if ($emptyFieldsCount == $columnNumber) {

            unset($columnsValue[$itemCount]);

            return $columnsValue;
        }

    }

    public function buildViewForCreationRequest($data)
    {

        $view = '';

        foreach ($data as $key => $row) {


            $item = New RequestItems();

            $item->ip_number = $row[1];
            $item->next_renewal_date = $row[2];
            $item->country = $row[3];

            if ($row[4] == RequestItems::STATUS_GRANTED_VIEW || $row[4] == 'granted') {
                $item->status = RequestItems::STATUS_GRANTED;
            } elseif ($row[4] == RequestItems::STATUS_APP_SELECT_VIEW || $row[4] == 'application') {
                $item->status = RequestItems::STATUS_APP_SELECT;
            } else {
                $item->status = '';
            }
            $item->case_type = $row[5];

            if ($row[6] == RequestItems::SIZE_LARGE || $row[6] == 'large') {
                $item->entity_size = RequestItems::SIZE_LARGE;
            } elseif ($row[6] == RequestItems::SIZE_SMALL || $row[6] == 'small') {
                $item->entity_size = RequestItems::SIZE_SMALL;
            } elseif ($row[6] == RequestItems::SIZE_MICRO || $row[6] == 'micro') {
                $item->entity_size = RequestItems::SIZE_MICRO;
            } else {
                $item->entity_size = '';
            }


            $item->next_annuity_year = $row[7];
            $item->number_of_claims = $row[8];
            $item->invoice_currency = $row[9];
            $item->estimated_or_invoiced_total = $row[10];

            $view .= View::make('admin.request._item-resend',
                [
                    'index' => $key,
                    'item' => $item
                ]
            )->render();

        }

        return $view;

    }

    public function buildViewForCreationResponse($data, $requestId, $supplierId)
    {


        $view = '';

        $items = RequestItems::where('request_id', $requestId)->get();

        foreach ($items as $item) {

            if (empty($data)) {
                continue;
            }

            $rowData = !empty($data) ? array_shift($data) : '';

            $requestItem = new ResponseItems;

            $requestItem->supplier_id = $supplierId;
            $requestItem->request_item_id = $item->id;
            $requestItem->total_charge_in_invoice = $rowData[10];
            $requestItem->official_fee_in_invoice = $rowData[11];
            $requestItem->locale_cost_in_invoice = $rowData[12];
            $requestItem->service_fee_in_invoice = $rowData[13];
            $requestItem->official_fee_amount = $rowData[14];
            $requestItem->official_fee_currency = $rowData[15];
            $requestItem->local_cost_amount = $rowData[16];
            $requestItem->local_cost_currency = $rowData[17];

            $item->requestItems->pop();
            $item->requestItems->push($requestItem);
        }


        $view .= View::make('admin.response._request-response-table',
            [
                'items' => $items,
                'supplierId' => $supplierId
            ]
        )->render();

        return $view;
    }

    public function createExcelResponse($requestId)
    {

        $request = Requests::find($requestId);


        $supplierNames = [];
        $itemsAndTotal = [];
        $totalOfTotal = [];

        foreach ($request->suppliers as $supplier) {
            $supplierNames[] = $supplier->profile->business_name;
        }

        $totalCost = 0;

        foreach ($request->items as $item) {

            $totals = [];

            $itemsAndTotal[$item->id][] = $item->item_number_code;
            $itemsAndTotal[$item->id][] = $item->ip_number;
            $itemsAndTotal[$item->id][] = $item->next_renewal_date;
            $itemsAndTotal[$item->id][] = $item->country;
            $itemsAndTotal[$item->id][] = $item->statusViewName();
            $itemsAndTotal[$item->id][] = $item->case_type;
            $itemsAndTotal[$item->id][] = $item->entity_size;
            $itemsAndTotal[$item->id][] = $item->next_annuity_year;
            $itemsAndTotal[$item->id][] = $item->number_of_claims;
            $itemsAndTotal[$item->id][] = $item->invoice_currency;
            $itemsAndTotal[$item->id][] = $item->estimated_or_invoiced_total;


            foreach ($request->suppliers as $supplier) {
                $totals[] = ResponseItems::getTotal($item->id, $supplier->id);
            }

            $itemsAndTotal[$item->id] = array_merge($itemsAndTotal[$item->id], $totals);

            $totalCost += $item->estimated_or_invoiced_total;
        }


        $totalOfTotal[] = $totalCost;

        foreach ($request->suppliers as $supplier) {
            $totalOfTotal[] = ResponseItems::getSumOfTotal($item->request->id, $supplier->id);
        }

        $itemsAndTotal[$item->id + 1] = array_merge(array_fill(0, 10, ''), $totalOfTotal, ['Total']);


        $head = array_merge(
            [
                RequestItems::COLUMN_NAME_ITEM_NUMBER_CODE,
                RequestItems::COLUMN_NAME_IP_NUMBER,
                'Renewal Due Date',
                RequestItems::COLUMN_NAME_COUNTRY,
                RequestItems::COLUMN_NAME_STATUS,
                RequestItems::COLUMN_NAME_CASE_TYPE,
                RequestItems::COLUMN_NAME_ENTITY_SIZE,
                RequestItems::COLUMN_NAME_NEXT_ANNUITY_YEAR,
                RequestItems::COLUMN_NAME_NUMBER_OF_CLAIMS,
                RequestItems::COLUMN_NAME_INVOICE_CURRENCY,
                RequestItems::COLUMN_NAME_ESTIMATED_OR_INVOICED_TOTAL,
            ], $supplierNames);

        $arrayData = array_merge(
            [$head],
            $itemsAndTotal

        );


        $spreadsheet = new Spreadsheet();

        $spreadsheet->getActiveSheet()
            ->setTitle(self::MAIN_REPORT_NAME)
            ->fromArray(
                $arrayData,
                NULL
            );
        $spreadsheet
            ->getActiveSheet()
            ->getTabColor()
            ->setRGB('C3E5CD');

        $spreadsheet->getActiveSheet()->getStyle('A1:J1')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('FBE5D6');

        $spreadsheet->getActiveSheet()->getStyle('B1')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('adc5e7');

        $spreadsheet->getActiveSheet()->getStyle('K1')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('adc5e7');


//        $conditional = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
//        $conditional
//            ->getStyle()
//            ->getFill()
//            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
//            ->getStartColor()->setARGB('C3E5CD');
//
//
//        foreach (range(11, 100) as $columnID) {
//            $value = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(0, $columnID)->getValue();
//
//            if ($value) {
//                $spreadsheet->getActiveSheet()->getCellByColumnAndRow(0, $columnID)->getStyle()->setConditionalStyles($conditional);
//
//            }
//
//        }

        foreach (range('A', 'Q') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $spreadsheet->getProperties()->setCreator(Auth::user()->first_name . ' ' . Auth::user()->last_name);
        $spreadsheet->getProperties()->setLastModifiedBy(Auth::user()->first_name . ' ' . Auth::user()->last_name);
        $spreadsheet->getProperties()->setTitle('exported Response from CompareIp');
        $spreadsheet->getProperties()->setSubject("exported Response from CompareIp");
        $spreadsheet->getProperties()->setDescription("Test document with supplier responses, generated using CompareIp.");

        $pageIterator = 1;

        foreach ($request->suppliers as $supplier) {

            $itemsAndResponses = [];

            // set Name of Sheet

            $pagename = empty($supplier->profile->business_name) ? 'Page #' . $pageIterator : $supplier->profile->business_name;

            // replace \/*[]:? from pageNmae
            $vowels = array("\\", "/", "*", "[", "]", ":", "?");
            $pagename = str_replace($vowels, ' ', $pagename);

            $spreadsheet
                ->createSheet();

            $spreadsheet
                ->setActiveSheetIndex($pageIterator)
                ->setTitle($pagename);

            $pageIterator++;

            //

            // Fetch data for current page


            $head = array_merge(
                [
                    RequestItems::COLUMN_NAME_ITEM_NUMBER_CODE,
                    'Renewal Due Date',
                    RequestItems::COLUMN_NAME_COUNTRY,
                    RequestItems::COLUMN_NAME_STATUS,
                    RequestItems::COLUMN_NAME_CASE_TYPE,
                    RequestItems::COLUMN_NAME_ENTITY_SIZE,
                    RequestItems::COLUMN_NAME_NEXT_ANNUITY_YEAR,
                    RequestItems::COLUMN_NAME_NUMBER_OF_CLAIMS,
                    RequestItems::COLUMN_NAME_INVOICE_CURRENCY,
                ],
                [
                    ResponseItems::OFFICIAL_FEE_AMOUNT,
                    ResponseItems::OFFICIAL_FEE_IN_INVOICE_CURRENCY,
                    ResponseItems::OFFICIAL_FEE_CURRENCY,
                    ResponseItems::LOCAL_COSTS_IN_INVOICE_CURRENCY,
                    ResponseItems::SERVICE_FEE_IN_INVOICE_CURRENCY,
                    ResponseItems::TOTAL_CHARGE_IN_INVOICE_CURRENCY,
                    ResponseItems::LOCAL_COST_AMOUNT,
                    ResponseItems::LOCAL_COST_CURRENCY,
//                    ResponseItems::COMMENTS,
                ]);

            $itemsAndResponses = [];

            foreach ($request->items as $item) {

                $supplierFieldsValue = [];

                $itemsAndResponses[$item->id][] = $item->item_number_code;
                $itemsAndResponses[$item->id][] = $item->next_renewal_date;
                $itemsAndResponses[$item->id][] = $item->country;
                $itemsAndResponses[$item->id][] = $item->statusViewName();
                $itemsAndResponses[$item->id][] = $item->case_type;
                $itemsAndResponses[$item->id][] = $item->entity_size;
                $itemsAndResponses[$item->id][] = $item->next_annuity_year;
                $itemsAndResponses[$item->id][] = $item->number_of_claims;
                $itemsAndResponses[$item->id][] = $item->invoice_currency;

                $response = ResponseItems
                    ::where('supplier_id', $supplier->id)
                    ->where('request_item_id', $item->id)
                    ->first();

                if (empty($response)) {
                    continue;
                }

                $supplierFieldsValue[] = $response->official_fee_currency;
                $supplierFieldsValue[] = $response->official_fee_in_invoice;
                $supplierFieldsValue[] = $response->official_fee_amount;
                $supplierFieldsValue[] = $response->locale_cost_in_invoice;
                $supplierFieldsValue[] = $response->service_fee_in_invoice;
                $supplierFieldsValue[] = $response->total_charge_in_invoice;
                $supplierFieldsValue[] = $response->local_cost_amount;
                $supplierFieldsValue[] = $response->local_cost_currency;


                $itemsAndResponses[$item->id] = array_merge($itemsAndResponses[$item->id], $supplierFieldsValue);

            }

            $arrayData = array_merge(
                [$head],
                $itemsAndResponses

            );
            //

            $spreadsheet->getActiveSheet()
                ->fromArray(
                    $arrayData,
                    NULL
                );

            $spreadsheet->getActiveSheet()->getStyle('B1:J1')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FBE5D6');

            $spreadsheet->getActiveSheet()->getStyle('L1:S1')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('C3E5CD');

            $spreadsheet->getActiveSheet()->getStyle('A1')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('adc5e7');

            $spreadsheet->getActiveSheet()->getStyle('K1')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('adc5e7');

            $spreadsheet
                ->getActiveSheet()
                ->getTabColor()
                ->setRGB('FBE5D6');


            foreach (range('A', 'S') as $columnID) {
                $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                    ->setWidth(20);

                $spreadsheet->getActiveSheet()
                    ->getStyle($columnID)
                    ->getAlignment()
                    ->setWrapText(true);

            }

        }
        $spreadsheet->setActiveSheetIndex(0);

        $writer = new Xlsx($spreadsheet);
        $writer->save(storage_path() . self::PATH_TO_EXPORT . self::EXPORT_FILE_NAME);
    }

    public function createExcelRequest($requestId, $supplierId)
    {

        $request = Requests::find($requestId);
        $itemsAndResponses = [];

        $head = [
            RequestItems::COLUMN_NAME_ITEM_NUMBER_CODE,
            'Renewal Due Date',
            RequestItems::COLUMN_NAME_COUNTRY,
            RequestItems::COLUMN_NAME_STATUS,
            RequestItems::COLUMN_NAME_CASE_TYPE,
            RequestItems::COLUMN_NAME_ENTITY_SIZE,
            RequestItems::COLUMN_NAME_NEXT_ANNUITY_YEAR,
            RequestItems::COLUMN_NAME_NUMBER_OF_CLAIMS,
            RequestItems::COLUMN_NAME_INVOICE_CURRENCY,

            ResponseItems::TOTAL_CHARGE_IN_INVOICE_CURRENCY,
            ResponseItems::OFFICIAL_FEE_IN_INVOICE_CURRENCY,
            ResponseItems::LOCAL_COSTS_IN_INVOICE_CURRENCY,
            ResponseItems::SERVICE_FEE_IN_INVOICE_CURRENCY,
            ResponseItems::OFFICIAL_FEE_AMOUNT,
            ResponseItems::OFFICIAL_FEE_CURRENCY,
            ResponseItems::LOCAL_COST_AMOUNT,
            ResponseItems::LOCAL_COST_CURRENCY,
        ];

        foreach ($request->items as $item) {

            $itemsAndResponses[$item->id][] = $item->item_number_code;
            $itemsAndResponses[$item->id][] = $item->next_renewal_date;
            $itemsAndResponses[$item->id][] = $item->country;
            $itemsAndResponses[$item->id][] = $item->statusViewName();
            $itemsAndResponses[$item->id][] = $item->case_type;
            $itemsAndResponses[$item->id][] = $item->entity_size;
            $itemsAndResponses[$item->id][] = $item->next_annuity_year;
            $itemsAndResponses[$item->id][] = $item->number_of_claims;
            $itemsAndResponses[$item->id][] = $item->invoice_currency;

            $itemsAndResponses[$item->id][] = $item->requestItems->where('supplier_id', $supplierId)->first() !== null
                ? $item->requestItems->where('supplier_id', $supplierId)->first()->total_charge_in_invoice
                : '';
            $itemsAndResponses[$item->id][] = $item->requestItems->where('supplier_id', $supplierId)->first() !== null
                ? $item->requestItems->where('supplier_id', $supplierId)->first()->official_fee_in_invoice
                : '';
            $itemsAndResponses[$item->id][] = $item->requestItems->where('supplier_id', $supplierId)->first() !== null
                ? $item->requestItems->where('supplier_id', $supplierId)->first()->locale_cost_in_invoice
                : '';
            $itemsAndResponses[$item->id][] = $item->requestItems->where('supplier_id', $supplierId)->first() !== null
                ? $item->requestItems->where('supplier_id', $supplierId)->first()->service_fee_in_invoice
                : '';
            $itemsAndResponses[$item->id][] = $item->requestItems->where('supplier_id', $supplierId)->first() !== null
                ? $item->requestItems->where('supplier_id', $supplierId)->first()->official_fee_amount
                : '';
            $itemsAndResponses[$item->id][] = $item->requestItems->where('supplier_id', $supplierId)->first() !== null
                ? $item->requestItems->where('supplier_id', $supplierId)->first()->official_fee_currency
                : '';

            $itemsAndResponses[$item->id][] = $item->requestItems->where('supplier_id', $supplierId)->first() !== null
                ? $item->requestItems->where('supplier_id', $supplierId)->first()->local_cost_amount
                : '';
            $itemsAndResponses[$item->id][] = $item->requestItems->where('supplier_id', $supplierId)->first() !== null
                ? $item->requestItems->where('supplier_id', $supplierId)->first()->local_cost_currency
                : '';

        }
        $arrayData = array_merge(
            [$head],
            $itemsAndResponses

        );


        $spreadsheet = new Spreadsheet();
        $spreadsheet->getActiveSheet()
            ->setTitle(self::MAIN_DOWNLOADED_REQUEST_NAME)
            ->fromArray(
                $arrayData,
                NULL
            );

        $spreadsheet->getActiveSheet()->getStyle('A1:I1')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('FBE5D6');

        $spreadsheet->getActiveSheet()->getStyle('J1:Q1')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('C3E5CD');


        foreach (range('A', 'S') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save(storage_path() . self::PATH_TO_REQUEST . self::EXPORT_FILE_NAME);
    }

    private function convertDateValue($value)
    {
        if (is_numeric($value)) {
            return \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject((int)$value)->format('d/m/Y');
        }
        $formats = ['d/m/Y', 'Y-m-d', 'd.m.Y', 'd-m-Y'];
        foreach ($formats as $format) {
            try {
                $parsed = date_parse_from_format($format, $value);
                if ($parsed['error_count'] === 0 && $parsed['warning_count'] === 0) {
                    return Carbon::createFromFormat($format, $value)->format('d/m/Y');
                }
            } catch (\Exception $e) {}
        }
        return $value;
    }

}
