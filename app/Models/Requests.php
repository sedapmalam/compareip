<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;


class Requests extends Model
{
    const STATUS_SAVE = 'saved';
    const STATUS_NEW = 'new';

    protected $table = 'requests';
    protected $fillable = ['user_id', 'status', 'note', 'received_at', 'reference'];

    protected $dates = ['received_at', 'created_at'];

    public function items()
    {
        return $this->hasMany('App\Models\RequestItems', 'request_id');
    }

    public function responses()
    {
        return $this->hasMany('App\Models\Responses', 'request_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function suppliers()
    {
        return $this->belongsToMany('App\User', 'suppliers_requests', 'request_id', 'supplier_id');
    }


    static public function new($userId, $status, $reference)
    {

        return self::create([
            'status' => $status,
            'user_id' => $userId,
            'received_at' => Carbon::now(),
            'reference' => $reference
        ]);

    }

    public function allRequestAnswered()
    {
        return 0 == $this->where('user_id', $this->user->id)
                ->where('status', self::STATUS_NEW)
                ->count();

    }

    public function isSaved()
    {
        return $this->status == self::STATUS_SAVE;
    }

    public function countResponses()
    {

        $count = 0;

        foreach ($this->suppliers as $supplier) {

            $count += $supplier
                    ->response
                    ->where('request_id', $this->id)
                    ->where('status', Responses::STATUS_SEND)
                    ->count()
                +
                $supplier
                    ->supplierRequest
                    ->where('request_id', $this->id)
                    ->where('status', SuppliersRequests::STATUS_DECLINED)
                    ->count();
        }

        return $count;
    }
}
