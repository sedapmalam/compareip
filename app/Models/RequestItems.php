<?php

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class RequestItems extends Model
{
    const LICENSE_OF_RIGHT_TRUE = 1;
    const LICENSE_OF_RIGHT_FALSE = 0;

    const LICENSE_OF_RIGHT_TRUE_VIEW = 'Yes';
    const LICENSE_OF_RIGHT_FALSE_VIEW = 'No';

    const STATUS_PATENT = 'Patent';
    const STATUS_DESIGN = 'Design';
    const STATUS_UTILITY_MODEL = 'Utility Model';
    const STATUS_TRADEMARK = 'Trademark';

    const SIZE_LARGE = 'Large';
    const SIZE_SMALL = 'Small';
    const SIZE_MICRO = 'Micro';

    const STATUS_GRANTED = 'granted';
    const STATUS_APP_SELECT = 'app-select';
    const STATUS_GRANTED_VIEW = 'Granted';
    const STATUS_APP_SELECT_VIEW = 'Application';


    const COLUMNS_IN_UPLOADED_FORM = 11;
    const COLUMNS_IN_UPLOADED_START = 1;


    const COLUMN_NAME_IP_NUMBER = 'IP Number';
    const COLUMN_NAME_ITEM_NUMBER_CODE = 'Item #';
    const COLUMN_NAME_COUNTRY = 'Country';
    const COLUMN_NAME_CASE_TYPE = 'Case Type';
    const COLUMN_NAME_STATUS = 'Status';
    const COLUMN_NAME_ENTITY_SIZE = 'Entity Size';
    const COLUMN_NAME_NUMBER_OF_CLAIMS = '# Claims, Designs, Classes';
    const COLUMN_NAME_NEXT_RENEWAL_DATE = 'Renewal Due&nbsp;Date';
    const COLUMN_NAME_NEXT_ANNUITY_YEAR = 'Annuity Year';
    const COLUMN_NAME_INVOICE_CURRENCY = 'Invoice Currency';
    const COLUMN_NAME_ESTIMATED_OR_INVOICED_TOTAL = 'Cost';

    const FORM_OPTION_SAVE = 'Save';
    const FORM_OPTION_SEND_REQUEST = 'Send Request';

    const VIEW_STATUS_SANDED = true;


    const CUSTOM_FIELD = 'Custom Field';

    protected $table = 'request_items';

    public function request()
    {
        return $this->belongsTo('App\Models\Requests');
    }

    public function requestItems()
    {
        return $this->hasMany('App\Models\ResponseItems', 'request_item_id');
    }

    protected $fillable = [
        'request_id',
        'ip_number',
        'item_number_code',
        'country',
        'case_type',
        'status',
        'entity_size',
        'number_of_claims',
        'next_renewal_date',
        'next_annuity_year',
        'invoice_currency',
        'estimated_or_invoiced_total',

    ];

    static public function updateItems($items, $requestId)
    {
       
        foreach ($items as $index => $value) {

            self::updateOrCreate(
                ['id' => $value['id']],
                ['request_id' => $requestId,
                    'ip_number' => $value['ip'],
                    'item_number_code' => $value['item_number_code'],
                    'country' => $value['country'],
                    'case_type' => $value['type'],
                    'status' => $value['status'],
                    'entity_size' => $value['entity_size'],
                    'number_of_claims' => $value['number'],
                    'next_annuity_year' => $value['next_annuity_year'],
                    'next_renewal_date' => $value['next_renewal_date'],
                    'invoice_currency' => $value['invoice_currency'],
                    'estimated_or_invoiced_total' => $value['estimated_or_invoiced_total']
                    // 'user_submit_request_reminder' => 0,
                    // 'user_submit_request_count' => 0,
                    // 'user_select_supplier_reminder' => 0,
                    // 'user_select_supplier_count' => 0

                ]);


        }
    }

    static public function createItems($items, $requestId)
    {

        foreach ($items as $index => $value) {

            self::create(
                ['request_id' => $requestId,
                    'ip_number' => $value['ip'],
                    'item_number_code' => $value['item_number_code'],
                    'country' => $value['country'],
                    'case_type' => $value['type'],
                    'status' => $value['status'],
                    'entity_size' => $value['entity_size'],
                    'number_of_claims' => $value['number'],
                    'next_annuity_year' => $value['next_annuity_year'],
                    'next_renewal_date' => $value['next_renewal_date'],
                    'invoice_currency' => $value['invoice_currency'],
                    'estimated_or_invoiced_total' => $value['estimated_or_invoiced_total'],
                    'user_submit_request_reminder' => 0,
                    'user_submit_request_count' => 0,
                    'user_select_supplier_reminder' => 0,
                    'user_select_supplier_count' => 0
                ]);


        }
    }

    public function getViewNameOfLicenseOfRight()
    {

        if ($this->license_of_right == self::LICENSE_OF_RIGHT_TRUE) {
            return self::LICENSE_OF_RIGHT_TRUE_VIEW;
        }

        if ($this->license_of_right == self::LICENSE_OF_RIGHT_FALSE) {
            return self::LICENSE_OF_RIGHT_FALSE_VIEW;
        }

    }

    static public function removeDeletedItems($items, $requestId)
    {
        $itemsExist = self::where('request_id', $requestId)->get();

        $itemsIds = [];

        foreach ($items as $index => $value) {
            $itemsIds[] = $value['id'];
        }

        foreach ($itemsExist as $item) {
            if (!in_array($item->id, $itemsIds)) {
                $item->delete();
            }
        }

    }

    static public function sumEstimated($items)
    {

        $sum = 0;

        foreach ($items as $item) {
            $sum += $item->estimated_or_invoiced_total;
        }

        return number_format($sum, 2, '.', '');
    }

    public function statusViewName(){
        return $this->status == self::STATUS_APP_SELECT
            ? self::STATUS_APP_SELECT_VIEW
            : self::STATUS_GRANTED_VIEW;
    }

}
