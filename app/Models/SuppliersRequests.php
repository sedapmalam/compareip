<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 14.03.18
 * Time: 16:14
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuppliersRequests extends Model
{

    const STATUS_NEW = 'new';
    const STATUS_DECLINED = 'declined';

    protected $table = 'suppliers_requests';
    protected $fillable = ['supplier_id', 'request_id', 'status'];

    public function supplier()
    {
        return $this->belongsTo('App\User');
    }

    public function request()
    {
        return $this->belongsTo('App\Models\Requests');
    }

    public function isDeclined()
    {
        return $this->status == self::STATUS_DECLINED;
    }

    static public function createConnections($suppliers, $requestId)
    {
        foreach ($suppliers as $supplierId) {
            self::create([
                'supplier_id' => $supplierId,
                'request_id' => $requestId
            ]);
        }
    }

    static public function getRequestForSuppliers($supplierId)
    {
        return self::where('supplier_id', $supplierId)
            ->where('status', '<>', self::STATUS_DECLINED)
            ->orderBy('id', 'DESC')
            ->get();
    }

    /**
     * Gets all supplier requests.
     *
     * @author Alexey Kuzmenko <alexey.kuzmenko@greenice.net>
     *
     * @param $supplierId
     *
     * @return mixed
     */
    static public function getAllRequests($supplierId)
    {
        return self::where('supplier_id', $supplierId)
            ->orderBy('id', 'DESC')
            ->get();
    }

    public function decline()
    {
        $this->update(
            [
                'status' => self::STATUS_DECLINED
            ]
        );
    }

}
