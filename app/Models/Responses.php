<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Responses extends Model
{

    protected $table = 'responses';
    protected $fillable = [
        'request_id',
        'supplier_id',
        'note',
        'status',
        'responded'
    ];

    const STATUS_STORED = 'save';
    const STATUS_SEND = 'send';
    const STATUS_VIEWED = 'viewed';
    const ACTION_EXIT = 'exit';
    const VIEW_STATUS_SANDED = true;

    protected $dates = ['responded'];

    public function responseItems()
    {
        return $this->hasMany('App\Models\ResponseItems', 'response_id');
    }

    public function supplier()
    {
        return $this->belongsTo('App\User', 'supplier_id');
    }

    public function request()
    {
        return $this->belongsTo('App\Models\Requests', 'supplier_id');
    }


    static public function createOrUpdate($userId, $requestId, $note, $action)
    {
        return self::updateOrCreate(

            ['supplier_id' => $userId,
                'request_id' => $requestId
            ],

            [
                'note' => $note,
                'status' => $action
            ]
        );
    }


    static  public function isSandStatic($requestId, $supplierId)
    {
        return self::where('request_id', $requestId)->where('supplier_id', $supplierId)->where('status', self::STATUS_SEND)->count() !== 0;
    }

    public function setStatusViewed()
    {

        $this->update(
            [
                'status' => self::STATUS_VIEWED
            ]
        );
    }
}
