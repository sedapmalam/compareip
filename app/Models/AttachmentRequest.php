<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttachmentRequest extends Model
{
    protected $table = 'attachments-requests';

    protected $fillable = [
        'supplier_id',
        'request_id',
        'attachment_id'
    ];

    public function supplier()
    {
        return $this->belongsTo('App\User');
    }

    public function request()
    {
        return $this->belongsTo('App\Models\Requests');
    }

    public function attachment()
    {
        return $this->belongsTo('App\Models\Attachment');
    }

    static public function exist($supplierId, $requestId)
    {
        return self::where('supplier_id', $supplierId)->where('request_id', $requestId)->count() != 0;
    }

}
