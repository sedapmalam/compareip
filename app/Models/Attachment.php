<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    const PATH_TO_FILE = '/attachments/';

    protected $table = 'attachments';
    protected $fillable = [
        'name'
    ];
}
