<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profiles extends Model
{

    const DEFAULT_IMAGE_NAME = 'default.jpg';
    const PATH_TO_IMAGE = 'uploads/avatars/';

    protected $table = 'profiles';

    protected $fillable = [

        'user_id',
        'company',
        'position',
        'phone',
        'number_of_registered_ip',
        'patents',
        'models',
        'industrial_designs',
        'trademarks',
        'site',
        'business_name',
        'contact_name',
        'description'

    ];

    public function createOrUpdateProfile($data, $userId)
    {
        $this->updateOrCreate(
            ['user_id' => $userId],
            ['company' => $data['organisation'],
                'position' => $data['position'],
                'phone' => $data['phone'],
                'number_of_registered_ip' => $data['number_of_registered_ip'],
                'patents' => $data['patents'],
                'models' => $data['models'],
                'industrial_designs' => $data['industrial_designs'],
                'trademarks' => $data['trademarks']]
        );
    }

    public function createOrUpdateSupplierProfile($data, $userId)
    {
        $this->updateOrCreate(
            ['user_id' => $userId],
            [
                'business_name' => $data['business_name'],
                'site' => $data['site'],
                'contact_name' => $data['contact_name'],
                'description' => $data['description'],
            ]
        );
    }

    public function createEmpty($userId)
    {
        $this->create([
            'user_id' => $userId
        ]);
    }


}
