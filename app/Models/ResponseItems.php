<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class ResponseItems extends Model
{

    const OFFICIAL_FEE_AMOUNT  = 'Official Fee Amount';
    const OFFICIAL_FEE_IN_INVOICE_CURRENCY = 'Official Fee in Invoice Currency';
    const OFFICIAL_FEE_CURRENCY = 'Official Fee Currency';
    const LOCAL_COSTS_IN_INVOICE_CURRENCY = 'Local Costs in Invoice Currency';
    const SERVICE_FEE_IN_INVOICE_CURRENCY = 'Service Fee in Invoice Currency';
    const TOTAL_CHARGE_IN_INVOICE_CURRENCY = 'Cost in Invoice Currency';

    const LOCAL_COST_AMOUNT = 'Local Cost Amount';
    const LOCAL_COST_CURRENCY = 'Local Cost Currency';

    const COLUMNS_IN_UPLOADED_FORM = 16;
    const COLUMNS_IN_UPLOADED_START = 10;

    protected $table = 'response_items';

    protected $fillable = [
        'supplier_id',
        'request_id',
        'request_item_id',
        'response_id',
        'official_fee_currency',
        'official_fee_in_invoice',
        'official_fee_amount',
        'locale_cost_in_invoice',
        'service_fee_in_invoice',
        'total_charge_in_invoice',

        'local_cost_amount',
        'local_cost_currency',
    ];

    public function newCollection(array $models = [])
    {
        return collect($models);
    }

    public function supplier()
    {
        return $this->belongsTo('App\User', 'supplier_id');
    }

    public function response()
    {
        return $this->belongsTo('App\Models\Responses', 'response_id');
    }

    static public function createOrUpdate($responses, $requestId, $supplierId, $responseId)
    {
        if($responses != null){
        foreach ($responses as $index => $value) {

            self::updateOrCreate(
                ['supplier_id' => $supplierId,
                    'request_id' => $requestId,
                    'request_item_id' => $value['itemId'],
                    'response_id' => $responseId],

                ['official_fee_currency' => $value['official_fee_currency'],
                    'official_fee_in_invoice' => $value['official_fee_in_invoice'],
                    'official_fee_amount' => $value['official_fee_amount'],
                    'locale_cost_in_invoice' => $value['locale_cost_in_invoice'],
                    'service_fee_in_invoice' => $value['service_fee_in_invoice'],
                    'total_charge_in_invoice' => $value['total_charge_in_invoice'],
                    'local_cost_amount' => $value['local_cost_amount'],
                    'local_cost_currency' => $value['local_cost_currency'],
                ]
            );

        }
    }
    }

    static public function getTotal($itemId, $supplierId)
    {
        $response = self
            ::where('request_item_id', $itemId)
            ->where('supplier_id', $supplierId)
            ->first();

        return isset($response->total_charge_in_invoice)
            ? number_format($response->total_charge_in_invoice, 2, '.', '')
            : 0;
    }

    static public function getSumOfTotal($requestId, $supplierId)
    {
        $totalSum = 0;

        $responses = self
            ::where('request_id', $requestId)
            ->where('supplier_id', $supplierId)
            ->get();

        foreach ($responses as $responseItem){
            $totalSum += $responseItem->total_charge_in_invoice;
        }

        return number_format($totalSum, 2, '.', '');
    }


}
