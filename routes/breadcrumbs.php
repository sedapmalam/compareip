<?php

// Main
Breadcrumbs::register('main', function ($breadcrumbs) {
    $breadcrumbs->push('Main', route('dashboard'));
});

//// Main > Create User
Breadcrumbs::register('create-user', function ($breadcrumbs) {
    $breadcrumbs->parent('main');
    $breadcrumbs->push('Create User', route('admin.users.newUser'));
});

//// Main > Update User
Breadcrumbs::register('update-user', function ($breadcrumbs) {
    $breadcrumbs->parent('main');
    $breadcrumbs->push('Update User', route('admin.users.updateUser'));
});

//// Main > Update Pass
Breadcrumbs::register('update-pass', function ($breadcrumbs, $userId) {
    $breadcrumbs->parent('main');
    $breadcrumbs->push('Update Password', route('admin.users.password', $userId));
});

//// Main > Requests
Breadcrumbs::register('request', function ($breadcrumbs) {
    $breadcrumbs->parent('main');
    $breadcrumbs->push('Requests', route('request.index'));
});

//// Main > Response
Breadcrumbs::register('response', function ($breadcrumbs) {
    $breadcrumbs->parent('main');
    $breadcrumbs->push('Activities', route('response.index'));
});

//// Admin - Main > Response
Breadcrumbs::register('response-admin', function ($breadcrumbs, $userId) {
    $breadcrumbs->parent('main');
    $breadcrumbs->push('Activities', route('admin.users.open.supplier', $userId));
});
//// Admin - Main > Requests
Breadcrumbs::register('request-admin', function ($breadcrumbs , $userId) {
    $breadcrumbs->parent('main');
    $breadcrumbs->push('Requests', route('admin.users.open.user', $userId));
});


//// Admin - Main > Requests > Items
Breadcrumbs::register('request-item', function ($breadcrumbs, $request) {
    $breadcrumbs->parent('request-admin', $request->user->id);
    $breadcrumbs->push('Items', route('request.items', $request->id ));
});

//// Admin - Main > Response > Items
Breadcrumbs::register('response-item', function ($breadcrumbs, $request, $supplierId) {
    $breadcrumbs->parent('response-admin', $supplierId);
    $breadcrumbs->push('Items', route('request.items', $request->id ));
});


//// Main > Requests > Items
Breadcrumbs::register('request-item-basic', function ($breadcrumbs) {
    $breadcrumbs->parent('request');
    $breadcrumbs->push('Items');
});

//// Main > Response > Items
Breadcrumbs::register('response-item-basic', function ($breadcrumbs) {
    $breadcrumbs->parent('response');
    $breadcrumbs->push('Items');
});

//// Main > Response > Items
Breadcrumbs::register('profile', function ($breadcrumbs) {
    $breadcrumbs->parent('main');
    $breadcrumbs->push('Profile');
});

//// Main > Requests > Create
Breadcrumbs::register('request-create', function ($breadcrumbs) {
    $breadcrumbs->parent('request');
    $breadcrumbs->push('Create');
});

//// Main > Requests > Forward
Breadcrumbs::register('request-forward', function ($breadcrumbs) {
    $breadcrumbs->parent('request');
    $breadcrumbs->push('Forward');
});

//// Main > Requests > Items > Responses
Breadcrumbs::register('request-item-response', function ($breadcrumbs, $request) {
    $breadcrumbs->parent('request');
    $breadcrumbs->push('Items', route('request.items', ['requestId' => $request->id]));
    $breadcrumbs->push('Responses');
});

//// Admin - Main > Requests > Items > Responses
Breadcrumbs::register('request-item-response-admin', function ($breadcrumbs, $request) {
    $breadcrumbs->parent('request-admin', $request->user->id);
    $breadcrumbs->push('Items', route('request.items', ['requestId' => $request->id]));
    $breadcrumbs->push('Responses');
});


//// Main > Requests > Items > Resend
Breadcrumbs::register('requests-request-resend', function ($breadcrumbs, $request) {
    $breadcrumbs->parent('request');
    $breadcrumbs->push('Items', route('request.items', ['requestId' => $request->id]));
    $breadcrumbs->push('Resend');
});


//// Admin - Main > Requests > Items > Resend
Breadcrumbs::register('requests-request-resend-admin', function ($breadcrumbs, $request) {
    $breadcrumbs->parent('request-admin', $request->user->id);
    $breadcrumbs->push('Items', route('request.items', ['requestId' => $request->id]));
    $breadcrumbs->push('Resend');
});
