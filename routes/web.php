<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// landing routes

Route::get('/', 'LandingController@main')->name('home');

Route::get('/contact', 'LandingController@contact')->name('contact');

Route::get('/faq', 'LandingController@faqs')->name('faq');

Route::get('/terms-of-service', 'LandingController@terms')->name('terms');

Route::get('/privacy-policy', 'LandingController@policy')->name('policy');

Route::get('/success', 'LandingController@success')->name('success');

Route::get('/disabled', 'LandingController@disabled')->name('disabled');

Route::post('/send', 'LandingController@sendEmailFromLanding')->name('lendingEmail');

//admin routes

Auth::routes();

Route::get('/dashboard', 'AdminController@dashboard')
    ->name('dashboard')
    ->middleware('auth');


Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {

    Route::get('agree', 'AdminController@agree')->name('agree');
    Route::put('agree', 'AdminController@setAgree')->name('set.agree');

    Route::get('user/password/{userId}', 'AdminController@changePasswordUser')->name('admin.users.password');
    Route::put('user/password', 'AdminController@updatePasswordUser')->name('admin.users.reset-password');

    Route::get('download/{attachmentId?}/{requestId?}', 'AdminController@download')->name('download');

    Route::get('pattern/download', 'ExcelController@downloadPattern')->name('download.pattern');
    Route::post('form/upload', 'ExcelController@uploadForm')->name('form.upload');
    Route::get('response/download/{requestId?}', 'ExcelController@downloadResponse')->name('response.download');

    Route::get('response/{requestId?}/supplierId/{supplierId?}', 'ResponseController@getRequest')->name('response.request');

});

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'user-supplier']], function () {

    // Users or Supplier functions

    Route::get('profile', 'ProfileController@index')->name('profile.index');
    Route::put('profile/update', 'ProfileController@update')->name('profile.update');

});

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin-user', 'disabled', 'agree']], function () {
    
    // Users functions

    Route::get('requests', 'RequestController@index')->name('request.index');
    Route::get('requests/new', 'RequestController@new')->name('request.new');
    Route::get('requests/{requestId}', 'RequestController@getItems')->name('request.items');
    Route::get('requests/forward/{requestId}', 'RequestController@forward')->name('request.forward');
    Route::get('requests/items/{itemId}', 'RequestController@getItem')->name('request.item');
    Route::post('requests/create', 'RequestController@create')->name('request.create');
    Route::get('request/edit/{requestId}', 'RequestController@edit')->name('request.edit')->middleware('isNewRequest');
    Route::get('supplier/{profileId}', 'AdminController@getSupplierInfo')->name('supplier.info');
   


});


Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin-supplier', 'disabled', 'agree']], function () {

    // Supplier functions

    Route::get('responses', 'ResponseController@index')->name('response.index');
    Route::put('response/decline', 'ResponseController@decline')->name('response.decline');
    Route::post('requests/send', 'ResponseController@send')->name('request.send');
    Route::post('requests/attachments', 'ResponseController@attachments')->name('request.attachments');
    Route::post('response/upload/requestId/{requestId}/supplierId/{supplierId}', 'ExcelController@uploadResponse')->name('response.upload');
    Route::get('request/download/{requestId?}/supplierId/{supplierId?}', 'ExcelController@downloadRequest')->name('request.download');
    Route::delete('attachment/remove', 'ResponseController@removeAttachment')->name('attachment.remove');
});


Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin', 'disabled', 'agree']], function () {

    // Admin functions

    Route::get('users', 'AdminController@users')->name('admin.users.index');
    
  

    Route::put('user/approve', 'AdminController@approveUser')->name('admin.users.approve');

    Route::delete('user/remove', 'AdminController@removeUsers')->name('admin.users.remove');

    Route::get('user/view/user/{userId}', 'RequestController@index')->name('admin.users.open.user');

    Route::get('user/view/supplier/{userId}', 'ResponseController@supplier')->name('admin.users.open.supplier');

    Route::get('user/edit/{userId}', 'AdminController@editUser')->name('admin.users.edit');
    Route::put('user/edit', 'AdminController@updateUser')->name('admin.users.updateUser');

    Route::get('user/new', 'AdminController@newUser')->name('admin.users.newUser');
    Route::post('user/new', 'AdminController@createUser')->name('admin.users.createUser');

    Route::put('user/disable', 'AdminController@disableUser')->name('admin.users.disable');
    Route::put('user/enable', 'AdminController@enableUser')->name('admin.users.enable');


});

Route::any('user/response/update', 'admin\ResponseController@updateResponseDownload');


Route::any('adminer', '\Miroc\LaravelAdminer\AdminerController@index');

Route::any('livezilla', function () {

    require base_path('livezilla/index.php');

})->name('livezilla');

