$(function () {


    var $container = $('#request'),
        itemTemplate = $('#item-template').html(),
        $noteAttachment = $('.note-attachment'),
        $note = $('#note'),
        $sandAttachments = $('#sand-attachments'),
        $dropzone = $('.dropzone'),
        $storeOutForm = $('.store-out-form'),
        $sandOutForm = $('.sand-out-form'),
        $exitOutForm = $('.exit-out-form'),
        $sandFromForm = $('.sand-from-form'),
        $storeFromForm = $('.store-from-form'),
        $exitFromForm = $('.exit-from-form'),
        $chooseAll = $('#choose-all'),
        $saveRequest = $('.save-request'),
        $downloadPatternContainer = $('.download-pattern-container'),
        $uploadPatternContainer = $('.upload-pattern-container'),
        $supplierCard = $('.supplier-card'),
        $declineRequest = $('.decline_request'),
        $reference = $('#reference'),
        $referenceForm = $('#reference-form'),
        $referenceContainer = $('.reference-container'),
        $wordCounter = $('.word-counter'),
        $nobeforeunload = $('.nobeforeunload');


    window.setItemNumberCodes = function () {

        var startNumber = 1;

        $('.item_number_code').each(function () {

            $(this).val(startNumber);
            startNumber++;
        })

    }

    window.manageRemoveButton = function () {

        $('.remove-container').first().children().each(function () {
            $(this).show();
        })

        if ($('.remove-container').length == 1) {
            $('.remove-container').first().children().hide();
        }
    }

    window.download = false;
    window.setItemNumberCodes();
    window.manageRemoveButton();

    $('input, select, textarea').on('change', function () {

        $(this).removeClass('error')

    })

    $supplierCard.click(function (e) {

        $checkbox = jQuery(this).find('[type=checkbox]');

        if ($(e.target).is('a')) {
            return
        }

        if ($checkbox.prop('checked') == true) {
            $checkbox.prop('checked', false);
        } else {
            $checkbox.prop('checked', true);
        }

    })

    $('.add-item').on('click', function (e) {
        e.preventDefault();

        var template = itemTemplate.replace(/\{index\}/g, (new Date()).getTime());

        $container.append(template);

        window.manageRemoveButton();

        window.setItemNumberCodes();

        $('.datepicker').datepicker(
            {
                format: 'dd/mm/yyyy',
            }
        );

    })


    $('body').on('click', '.remove-item', function () {

        $(this)
            .closest('.request-item')
            .remove();

        window.manageRemoveButton();

        window.setItemNumberCodes();


    })

    $('.choose-suppliers').click(function () {

        $('.table-container').fadeOut(500);
        $('.add-item').fadeOut(500);
        $downloadPatternContainer.fadeOut(500);
        $uploadPatternContainer.fadeOut(500);
        $saveRequest.fadeOut(500);
        $referenceContainer.fadeOut(500);
        $(this).fadeOut(500, function () {
            $('.suppliers-container').fadeIn(500, function () {
                setHeightOfBlocks();
            });
            $('.supplier-complete-container').fadeIn(500);

        })
    });


    $('.back-to-items').click(function () {
        $('.suppliers-container').fadeOut(500);
        $('.supplier-complete-container').fadeOut(500, function () {
            $('.table-container').fadeIn(500);
            $('.add-item').fadeIn(500);
            $('.choose-suppliers').fadeIn(500);
            $referenceContainer.fadeIn(500);
            $saveRequest.fadeIn(500);
            $downloadPatternContainer.fadeIn(500);
            $uploadPatternContainer.fadeIn(500);
        });
    })

    $('.send-request').click(function () {

        var pickedSupplier = 0;

        $('.supplier').each(function () {
            if ($(this).prop('checked') == true) {
                pickedSupplier++;
            }
        })

        if (pickedSupplier === 0) {

            showAlertNoSupplier();

        } else {
            $(window).off("beforeunload");
            $('#request-items-form').submit();

        }
    })

    if ($container.find('.request-item').length === 0) {
        $('.add-item').click();
    }

    function showAlertNoSupplier() {

        swal({
            title: 'Error',
            text: 'You need to choose at least one supplier',
            type: 'error',
            showCancelButton: false,
        })
    }

    $noteAttachment.keyup(function () {

        var maxWordCount = 4000;
        var wordLeft = 0;

        $note.val($(this).val())

        wordLeft = maxWordCount - $(this).val().length;

        if (wordLeft < 0) {
            showAlertWordLimit();
            $wordCounter.text(0);
            $(this).val($(this).val().substring(0, 4000));
            $note.val($(this).val());
            return
        }

        $wordCounter.text(wordLeft);
    })

    $sandAttachments.click(function () {
        $dropzone.submit();
    })

    $storeOutForm.click(function () {

        $storeFromForm.click();

    })

    $exitOutForm.click(function () {

        $exitFromForm.click();

    })

    $sandOutForm.click(function () {

        swal({
            title: 'Send response',
            text: 'You sure, you want to send response?',
            type: 'warning',
            showCancelButton: true,
        }).then(function (sure) {
            if (sure) {
                $sandFromForm.click();
            }
        });

    })

    $chooseAll.change(function () {
        if ($(this).prop('checked') == true) {
            $('.supplier').each(function () {
                $(this).prop('checked', 'checked')
            })
        } else {
            $('.supplier').each(function () {
                $(this).removeAttr('checked');
            })
        }
    })


    $saveRequest.click(function () {
        $(window).off("beforeunload");
        $(this).click();
    })

    $nobeforeunload.click(function () {
        $(window).off("beforeunload");
        $(this).click();
    })


    $supplierCard.click(function () {
        $(this).toggleClass('add-checked-border', 0);
    })

    function setHeightOfBlocks() {

        var maxHeight = 0;

        $supplierCard.each(function () {

            if ($(this).height() > maxHeight) {
                maxHeight = $(this).height();
            }

        })

        $supplierCard.each(function () {

            $(this).height(maxHeight);

        })

    }

    $declineRequest.click(function () {
        var button = $(this);

        swal({
            title: 'Decline',
            text: 'You sure, you want to decline request?',
            type: 'warning',
            showCancelButton: true,
        }).then(function (Decline) {
            if (Decline) {
                button.closest('form').submit();
            }
        });
    })

    function showAlertWordLimit() {
        swal({
            title: 'Words limit',
            text: 'Maximum number of characters is 4000',
            type: 'warning',
            showCancelButton: false,
        });
    }

    $('.download-button').click(function () {
        window.download = true;
    })


    $reference.keyup(function () {

        $referenceForm.val($(this).val());

    })

    $('.datepicker').datepicker(
        {
            format: 'dd/mm/yyyy',
        }
    );

    $reference.keyup();
    $noteAttachment.keyup();
})

$(window).on("beforeunload", function (e) {

    var $container = $('#request');
    window.download = false;

    if ($container.length !== 0 && !window.download) {

        var confirmationMessage = 'Please select Save so that any data entered is not lost.'
            + 'Please select Save so that any data entered is not lost.';


        (e || window.event).returnValue = confirmationMessage; //Gecko + IE
        return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.

    }

});
