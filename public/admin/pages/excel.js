$(function () {

    if($('.form-input').length !== 0){

        var $form = $('.upload-excel'),
            uploadUrl = $('#upload-url').data('upload-url'),
            requestItem = $('.request-item'),
            $container = $('#request');

        $('.file-inputs').bootstrapFileInput();

        $('.form-input').change(function () {
            sendAjax();
            $('.form-input').val('');
        });

        function sendAjax() {

            $('#status').fadeIn();
            $('#preloader').addClass('opacity-preloader');
            $('#preloader').fadeIn('slow');

            $.ajax({

                type: "POST",
                url: uploadUrl,
                data: new FormData($form[0]),
                dataType: 'json',
                contentType: false,
                processData: false,
                statusCode: {
                    200: function (resp) {
                        rebuild(resp);
                    },
                    500: function () {
                        showAlertSomeError();
                    }
                },

            }).always(function() {

                $('#status').fadeOut();
                $('#preloader').delay(350).fadeOut('slow', function () {
                    $('#preloader').removeClass('opacity-preloader')
                });

                $('.datepicker').datepicker(
                    {
                        format: 'dd/mm/yyyy',
                    }
                );

            });
        }

        function rebuild(rows) {

            $('.request-item').remove();
            $container.append(rows.responseText);

            if(!rows.responseText){
                $('.add-item').click();
            }

            window.manageRemoveButton();
            window.setItemNumberCodes();

            $('.datepicker').datepicker(
                {
                    format: 'dd/mm/yyyy',
                }
            );

        }

        function showAlertSomeError() {
            swal({
                title: 'Error',
                text: 'Format must be Excel',
                type: 'error',
                showCancelButton: false,
            })
        }

    }


})
