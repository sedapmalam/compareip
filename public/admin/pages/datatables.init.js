/*
 Template Name: Upcube - Bootstrap 4 Admin Dashboard
 Author: Themesdesign
 Website: www.themesdesign.in
 File: Datatable js
 */

$(document).ready(function() {


    function initTable($tableSelector, buttonContainer, options) {

        var settings = {
            lengthChange: false,
            ordering: false,
            autoWidth : false,
            info : false,
            drawCallback: function(settings) {
                var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
                pagination.toggle(this.api().page.info().pages > 1);
            },
            oLanguage: {
                "sSearch": "Find: "
            },
            buttons: [
                {
                    text: 'X',
                    className: 'red',
                    action: function ( e, dt, node, config ) {
                        dt.search('').draw();
                    }
                }
            ]
        };

        var table = $tableSelector.DataTable($.extend(settings, options));

        table.buttons().container()
            .appendTo(buttonContainer);
    }

    initTable($('.datatable-buttons'), '.datatable-buttons_wrapper')
    initTable($('.datatable-request-buttons'), '.datatable-buttons-request_wrapper')
    initTable($('.datatable-buttons-summary'), '.datatable-buttons-summary_wrapper')
    initTable($('.datatable-buttons-response-supplier'), '.datatable-buttons-response-supplier_wrapper')
    initTable($('.datatable-buttons-response'), '#DataTables_Table_0_filter', {
        bPaginate: false
    });
    initTable($('.datatable-buttons-attachments'), '.datatable-buttons-attachments_wrapper')

    $('#adminUsersTable').DataTable({
        columnDefs: [ {
            targets: [ 0 ],
            orderData: [ 0, 1 ]
        }, {
            targets: [ 1 ],
            orderData: [ 1, 0 ]
        }, {
            targets: [ 4 ],
            orderData: [ 4, 0 ]
        } ],
        aoColumnDefs: [{
            bSortable: false,
            aTargets: [-1, -4, -5, -6]
        }],
        aaSorting: [[1, 'desc']]
    });
    $('#adminUsersTabletwo').DataTable({
        columnDefs: [ {
            targets: [ 0 ],
            orderData: [ 0, 1 ]
        }, {
            targets: [ 1 ],
            orderData: [ 1, 0 ]
        }, {
            targets: [ 4 ],
            orderData: [ 4, 0 ]
        } ],
        aoColumnDefs: [{
            bSortable: false,
            aTargets: [-1, -4, -5, -6]
        }],
        aaSorting: [[1, 'desc']]
    });
    $('#adminUsersTablethree').DataTable({
        columnDefs: [ {
            targets: [ 0 ],
            orderData: [ 0, 1 ]
        }, {
            targets: [ 1 ],
            orderData: [ 1, 0 ]
        }, {
            targets: [ 4 ],
            orderData: [ 4, 0 ]
        } ],
        aoColumnDefs: [{
            bSortable: false,
            aTargets: [-1, -4, -5, -6]
        }],
        aaSorting: [[1, 'desc']]
    });
    $('#adminUsersTablefour').DataTable({
        columnDefs: [ {
            targets: [ 0 ],
            orderData: [ 0, 1 ]
        }, {
            targets: [ 1 ],
            orderData: [ 1, 0 ]
        }, {
            targets: [ 4 ],
            orderData: [ 4, 0 ]
        } ],
        aoColumnDefs: [{
            bSortable: false,
            aTargets: [-1, -4, -5, -6]
        }],
        aaSorting: [[1, 'desc']]
    });

} );



