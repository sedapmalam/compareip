$(function () {

    var $profileUser = $('#profile-user');
    var $supplierMainFields = $('#supplier-main-fields');
    var $userMainFields = $('#user-main-fields');
    var $mainFields = $('.main-fields');

    var $createButton = $('.create-button-container');
    var $currentRole = $('#current-role');
    var userRole = $('#userRole').val();
    var supplierRole = $('#supplierRole').val();
    var adminRole = $('#adminRole').val();
    var $profileContainer = $('.profile-container');

    if ($('.profile-container-inputs').length == 0 && ($currentRole.val() == '' || $currentRole.val() == userRole)) {

        $createButton.before($profileUser.html());
    }

    if ($currentRole.val() == '' || $currentRole.val() == userRole) {

        $mainFields.append($userMainFields.html());
    }

    if ( $currentRole.val() == supplierRole) {

        $mainFields.append($supplierMainFields.html());
    }

    if ( $currentRole.val() == adminRole) {

        $mainFields.append($userMainFields.html());
    }

    $('#role').on('change', function () {


        if ($(this).val() == userRole) {

            removeProfileInputs();

            $createButton.before($profileUser.html());

            removeMainFields();

            $mainFields.append($userMainFields.html());
        }

        if ($(this).val() == $('#supplierRole').val()) {

            removeProfileInputs();
            removeMainFields();

            $mainFields.append($supplierMainFields.html());

        }

        if ($(this).val() == $('#adminRole').val()) {

            removeProfileInputs();
            removeMainFields();

            $mainFields.append($userMainFields.html());
        }
    })

    function removeProfileInputs() {

        var $profileContainer = $('.profile-container-inputs');
        $profileContainer.remove();
    }

    function removeMainFields() {
        $mainFields.children().each(function () {
            $(this).remove();
        })
    }
})
