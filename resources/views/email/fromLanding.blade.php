
<div>

    <h2>New message from {{ config('app.name') }}</h2>

    <br>

    <p>Sender - {{ $name }}, Email - {{ $email }}</p>

    <br>
    <p>Message - {{ $messageFromLanding }}</p>

    @include('email._logo')

</div>
