
<div>

    <p style="font-weight: normal;">Dear {{@$user->first_name}} {{@$user->last_name}}</p>
    <br>
    <p>Please login to view and download your responses from Suppliers.</p>
    <br>
    <p>Kind regards</p>
    <p>CompareIP Support</p>
    @include('email._logo')
</div>
