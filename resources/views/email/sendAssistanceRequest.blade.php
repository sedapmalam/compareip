
<div>
    


    <p style="font-weight: normal;">Dear {{@$user->first_name}} {{@$user->last_name}}</p>
    <br>
    <p>Now that you have downloaded your report, and any additional documents provided by Suppliers with their personal message to you, please let us know if you need any further assistance from us.</p>
    <br>
    <p>If you have found our service useful, please recommend us to others who may not know of our platform.</p>
    <br>
    <p>Kind regards</p>
    <p>Peter Rouse</p>
    @include('email._logo')
</div>

