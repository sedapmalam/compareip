
<div>



    <h2>Dear {{ $user->first_name }}</h2>

    <br>
<p>You are now authorized to make use of the Compare IP platform.</p>
    <p>Your username and password are set out below – your password should be changed when you first login:</p>

    <br>


    <p>Username: {{ $user->email }}</p>
    <p>Password: {{ $password }}</p>

    <br>

    <p>You will also be taken to the Terms of Service to which you must agree in order to access your dashboard from where you will be able to upload renewal data; select suppliers; send pricing requests; track progress; as well as review and download responses.</p>
    <br>

    <p>If you have any questions, please use the live chat service on our website or email support@Compare IP.com and we will do all we can to help.</p>
    <br>

    <p>Kind regards</p>
    <p>Peter Rouse</p>
    @include('email._logo')
</div>
