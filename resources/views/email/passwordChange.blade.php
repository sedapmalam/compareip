<div>

    @if($user->isUser())
        <h2>Dear {{ $user->first_name }}</h2>
    @else
        <h2>Dear {{ $user->profile->business_name }}</h2>
    @endif

        <br>

        <p>Your password has been changed by the administrator</p>

        <p>This is your new password - {{ $password }}</p>
        <p>Follow link to login {{ route('dashboard') }}</p>

        <p>Kind regards</p>
        <p>CompareIP Support</p>

            @include('email._logo')

</div>
