
<div>

    <h2>Dear {{ $user->profile->business_name }}</h2>

    <br>

    <p>A quote request has been sent to you for response – please visit your dashboard {{ $requestUrl }} to view and respond.</p>

    <p>Those who respond soonest impress most.</p>

    <p>Kind regards</p>
    <p>CompareIP Support</p>

    @include('email._logo')

</div>
