
<div>

    <h2>Dear {{ $user->first_name }}</h2>

    <br>

    <p>Your quote request sent to {{ $supplier->profile->business_name }}  has been answered – please visit your dashboard {{ $linkToRequest }} to view and/or download the response.</p>

    <p>Kind regards</p>
    <p>CompareIP Support</p>

    @include('email._logo')

</div>
