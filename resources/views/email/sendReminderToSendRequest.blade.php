
<div>

    <p style="font-weight: normal;">Dear {{@$user->first_name}} {{@$user->last_name}}</p>
    <br>
    <p>You have now selected the Suppliers you want to provide indicative pricing.  However, Suppliers will not receive your data until you have selected 'Send Request'.  </p>
    <p>Please login <a href="http://www.compareip.com">here</a> and go to Requests.  Then click on the arrow in the right hand column and on the next page check you have the right Suppliers selected and then click on 'Send Request'.</p>
    <br>
    <p>If you need help,then please send a message to support@compareip.com</p>
    <br>
    <p>Kind regards</p>
    <p>CompareIP Support</p>
    @include('email._logo')
</div>
