
<div>

<p style="font-weight: normal;">Dear {{@$user->first_name}} {{@$user->last_name}}</p>

    <br>
    <p>Now that you have uploaded the data that Suppliers will need to provide you with indicative pricing you, the next steps are to select Suppliers and then send your request.</p>
    <p>Please login <a href="http://www.compareip.com">here</a> and go to Requests.  Then click on the arrow icon and on the next page select 'Choose suppliers'. On the next page deselect any Suppliers you don't want included, then select 'Send Request'.</p>
    <br>
    <p>If you need help,please send a message to support@compareip.com</p>
    <br>
    <p>Kind regards</p>
    <p>CompareIP Support</p>
    @include('email._logo')
</div>

