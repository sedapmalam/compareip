


<div>



    <h4>Dear {{ $user->first_name }}</h4>
    <br>
    <p>Thanks for registering to use CompareIP. To confirm your authorization to use our platform and access to your private dashboard, we need some basic information from you that can be shared with suppliers. While your identity will not be disclosed, please provide the number of each of the following iP rights you hold as most suppliers have service fees that vary according to portfolio size:</p>
    <br>
    <p>Patents:</p>
    <p>Utility Models*:</p>
    <p>Industrial Designs:</p>
    <p>Trademarks:</p>
    <br>
    <p>*including petty patents and other types</p>
    <br>
    <p>If we can be of assistance, please use the live chat service on our website or email support@compareIP.com and we will do all we can to help.</p>
    <br>
    <p>Kind regards</p>
    <p>Peter Rouse</p>
    @include('email._logo')
</div>



