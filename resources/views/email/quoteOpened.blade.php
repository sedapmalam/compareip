
<div>

    <h2>Dear {{ $user->first_name }}</h2>

    <br>

    <p>Your quote {{ $reference }} sent on {{ $now }} has been viewed by the IP owner who requested it.</p>

    <p>Follow link to login {{ route('dashboard') }}</p>

    <p>Kind regards</p>
    <p>CompareIP Support</p>

    @include('email._logo')

</div>
