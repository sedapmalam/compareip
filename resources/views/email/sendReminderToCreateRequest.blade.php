
<div>
    
    <p style="font-weight: normal;">Dear {{@$user['first_name']}} </p>
    <br>
    <p>Your CompareIP.com account has been fully enabled and you are now free to upload data to create a Request to be sent to Suppliers for them to provide you with indicative pricing.</p>
    <br>
    <p>If you need help at any stage, please send a message to support@compareip.com</p>
    <br>
    <p>Kind regards</p>
    <p>CompareIP Support</p>
    @include('email._logo')
</div>






