
<div>

    <h2>Dear {{ $user->first_name }}</h2>

    <br>

    <p>Your quote request sent to {{ $supplier->profile->business_name }} on {{ $dateDeclined }} has been declined.</p>

    <p>We are sorry that this supplier has chosen not to respond.  If you have not yet done so, you can send requests to other suppliers from your {{ route('dashboard') }} (you will need to create a new request).</p>

    <br>

    <p>Kind regards</p>
    <p>CompareIP Support</p>

    @include('email._logo')

</div>
