<h2>Dear {{ $admin->first_name }}</h2>

<p>New account has been created for user:</p>
<dl>
    <dt><strong>First Name:</strong></dt>
    <dd>{{ $user->first_name }}</dd>
    <dt><strong>Last Name:</strong></dt>
    <dd>{{ $user->first_name }}</dd>
    <dt><strong>Organisation:</strong></dt>
    <dd>{{ $user->profile->company }}</dd>
    <dt><strong>Email:</strong></dt>
    <dd>{{ $user->email }}</dd>
</dl>

<p>Kind regards</p>
<p>CompareIP Support</p>

@include('email._logo')
