
<div>


    <h2>Dear {{ $user->profile->business_name }}</h2>

    <br>

    <p>You are now authorized to make use of the CompareIP platform as a Supplier.</p>

    <p>Your username and password are set out below – your password should be changed when you first login:</p>

    <br>
    <br>

    <p>Username: {{ $user->email }}</p>
    <p>Password: {{ $password }}</p>

    <br>
    <br>

    <p>When you have logged in you will see the Terms of Service to which you must agree in order to have access to your private dashboard from where you will be able to view and download quote requests; upload quote responses; and view your history of requests received and answered.</p>
    <br>

    <p>If you have any questions, please use the live chat service on our website or email support@CompareIP.com and we will do all we can to help.</p>

    <br>

    <p>Kind regards</p>
    <p>Peter Rouse</p>
    @include('email._logo')
    <p>Phone: +44 (0) 1803 860184</p>


</div>
