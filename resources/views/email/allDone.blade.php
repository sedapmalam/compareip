
<div>

    <h2>Dear {{ $user->first_name }}</h2>

    <br>

    <p>Your quote requests have now all been answered – please visit your dashboard {{ route('dashboard') }} to view and/or download the responses.  </p>

    <p>Kind regards</p>
    <p>CompareIP Support</p>

    @include('email._logo')

</div>
