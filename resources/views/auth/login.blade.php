@extends('layouts.auth')

@section('content')

    <div class="card">
        <div class="card-body">

            <h3 class="text-center mt-0 m-b-15">
                <a href="{{ route('home') }}" class="logo logo-admin"><img src="{{ asset('admin/images/logo.png') }}" height="30" alt="logo"></a>
            </h3>

            <h4 class="text-muted text-center font-18"><b>Sign In</b></h4>

            <div class="p-3">
                <form class="form-horizontal m-t-20" action="{{ route('login') }}" method="POST">

                    {{ csrf_field() }}

                    <div class="form-group row" >
                        <div class="col-12 text-align {{ $errors->has('email') ? ' has-error' : '' }}">
                            <input name="email" class="form-control" type="email" required="" placeholder="Email" value="{{ old('email') }}">

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif

                        </div>

                    </div>

                    <div class="form-group row">
                        <div class="col-12 text-align {{ $errors->has('password') ? ' has-error' : '' }}">
                            <input name="password" class="form-control" type="password" required="" placeholder="Password">

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif

                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <div class="custom-control custom-checkbox">
                                <input  type="checkbox" class="custom-control-input" id="customCheck1" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label class="custom-control-label" for="customCheck1">Remember me</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group text-center row m-t-20">
                        <div class="col-12">
                            <button class="btn btn-info btn-block waves-effect waves-light" type="submit">Log In</button>
                        </div>
                    </div>

                    <div class="form-group m-t-10 mb-0 row">
                        <div class="col-sm-7 m-t-20">
                            <a href="{{ route('password.request') }}" class="text-muted"><i class="mdi mdi-lock    "></i> Forgot your password?</a>
                        </div>

                    </div>
                </form>
            </div>

        </div>
    </div>

@endsection
