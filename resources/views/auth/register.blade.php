@extends('layouts.auth')

@section('content')

    <div class="card">
        <div class="card-body">

            <h3 class="text-center mt-0 m-b-15">
                <a href="{{ route('home') }}" class="logo logo-admin"><img src="{{ asset('admin/images/logo.png') }}"
                                                                           height="30" alt="logo"></a>
            </h3>

            <h4 class="text-muted text-center font-18"><b>Register</b></h4>

            <div class="p-3">
                <form class="form-horizontal m-t-20" method="POST" action="{{ route('register') }}">

                    {{ csrf_field() }}

                    <div class="form-group row">
                        <div class="col-12">
                            <input id="first_name" type="text" class="form-control" name="first_name"
                                   value="{{ isset($first_name) ? $first_name : old('first_name') }}" required autofocus
                                   placeholder="First Name">

                            @if ($errors->has('first_name'))
                                <span class="help-block">
    <strong>{{ $errors->first('first_name') }}</strong>
    </span>
                            @endif

                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <input id="last_name" type="text" class="form-control" name="last_name"
                                   value="{{ isset($last_name) ? $last_name : old('last_name') }}" required
                                   placeholder="Second Name">

                            @if ($errors->has('last_name'))
                                <span class="help-block">
    <strong>{{ $errors->first('last_name') }}</strong>
    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <input id="organisation" type="text" class="form-control" name="organisation"
                                   value="{{ isset($organisation) ? $organisation : old('organisation') }}" required
                                   placeholder="Organisation">

                            @if ($errors->has('organisation'))
                                <span class="help-block">
    <strong>{{ $errors->first('organisation') }}</strong>
    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <input id="email" type="email" class="form-control" name="email"
                                   value="{{ isset($email) ? $email : old('email') }}" required placeholder="Email">

                            @if ($errors->has('email'))
                                <span class="help-block">
    <strong>{{ $errors->first('email') }}</strong>
    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <input id="position" type="text" class="form-control" name="position"
                                   value="{{ old('position') }}" placeholder="Position">

                            @if ($errors->has('position'))
                                <span class="help-block">
    <strong>{{ $errors->first('position') }}</strong>
    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}"
                                   placeholder="Phone">

                            @if ($errors->has('phone'))
                                <span class="help-block">
    <strong>{{ $errors->first('phone') }}</strong>
    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <input id="number_of_registered_ip" type="text" class="form-control"
                                   name="number_of_registered_ip" value="{{ old('number_of_registered_ip') }}"
                                   placeholder="Number of registered IP rights">

                            @if ($errors->has('number_of_registered_ip'))
                                <span class="help-block">
    <strong>{{ $errors->first('number_of_registered_ip') }}</strong>
    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <input id="patents" type="text" class="form-control" name="patents"
                                   value="{{ old('patents') }}" placeholder="Patents">

                            @if ($errors->has('patents'))
                                <span class="help-block">
    <strong>{{ $errors->first('patents') }}</strong>
    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <input id="models" type="text" class="form-control" name="models"
                                   value="{{ old('models') }}" placeholder="Utility Models (and similar)">

                            @if ($errors->has('models'))
                                <span class="help-block">
    <strong>{{ $errors->first('models') }}</strong>
    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <input id="industrial_designs" type="text" class="form-control" name="industrial_designs"
                                   value="{{ old('industrial_designs') }}" placeholder="Industrial Designs">

                            @if ($errors->has('industrial_designs'))
                                <span class="help-block">
    <strong>{{ $errors->first('industrial_designs') }}</strong>
    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <input id="trademarks" type="text" class="form-control" name="trademarks"
                                   value="{{ old('trademarks') }}" placeholder="Trademarks">

                            @if ($errors->has('trademarks'))
                                <span class="help-block">
    <strong>{{ $errors->first('trademarks') }}</strong>
    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <input id="password" type="password" class="form-control" name="password" required
                                   placeholder="Password">

                            @if ($errors->has('password'))
                                <span class="help-block">
    <strong>{{ $errors->first('password') }}</strong>
    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <input id="password-confirm" type="password" class="form-control"
                                   placeholder="Password Confirmation" name="password_confirmation" required>
                        </div>
                    </div>
                   
                    <div class="form-group row">
                    <div class="col-md-6 offset-md-4">
                    <div class="g-recaptcha" data-sitekey="6LcDp7YUAAAAAHU4mOJHba3GFg-FX8Pvi-qe2TTs"></div>
                        @if ($errors->has('g-recaptcha-response'))
                            <span class="invalid-feedback" style="display: block;">
                                <strong class="g-recaptcha" >{{ $errors->first('g-recaptcha-response') }}</strong>
                            </span>
                        @endif
                    </div>
                    </div>

                  
                   
                    <div class="form-group text-center row m-t-20">
                        <div class="col-12">
                            <button class="btn btn-info btn-block waves-effect waves-light" type="submit">Register
                            </button>
                        </div>
                    </div>
                    
                   

                </form>

                <div class="form-group m-t-10 mb-0 row">
                    <div class="col-12 m-t-20 text-center">
                        <a href="{{ route('login') }}" class="text-muted">Already have account?</a>
                    </div>
                </div>
            </div>

        </div>



@endsection
