@extends('layouts.auth')

@section('content')

    <div class="accountbg"></div>
    <div class="wrapper-page">

        <div class="card">
            <div class="card-body">

                <h3 class="text-center mt-0 m-b-15">
                    <a href="{{ route('home') }}" class="logo logo-admin"><img src="{{ asset('admin/images/logo.png') }}" height="30" alt="logo"></a>
                </h3>

                <h4 class="text-muted text-center font-18"><b>Reset Password</b></h4>

                <div class="p-3">

                    <form class="form-horizontal m-t-20" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="alert alert-info alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            Enter your <b>Email</b> and instructions will be sent to you!
                        </div>


                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                            <div class="col-xs-12">
                                <input id="email" type="email" class="form-control" name="email" placeholder="Email"
                                       value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group text-center row m-t-20">
                            <div class="col-12">
                                <button class="btn btn-info btn-block waves-effect waves-light" type="submit">Send Email</button>
                            </div>
                        </div>

                    </form>

                </div>

            </div>
        </div>
    </div>

@endsection
