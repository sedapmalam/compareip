@extends('layouts.admin')

@section('content')

    {{ Breadcrumbs::render('update-user') }}

    <div class="card wrapper-page">
        <div class="card-body">

            <input type="hidden" value="{{ \App\User::ROLE_USER }}" id="userRole">

            <input type="hidden" value="{{ \App\User::ROLE_SUPPLIER }}" id="supplierRole">

            <input type="hidden" value="{{ \App\User::ROLE_ADMIN }}" id="adminRole">

            <input type="hidden" value="{{ old('role') !== null ? old('role') : $user->role }}" id="current-role">

            <h3 class="text-center mt-0 m-b-15">
                <img src="{{ asset('admin/images/logo.png') }}"
                     height="30" alt="logo">
            </h3>

            <h4 class="text-muted text-center font-18"><b>Update User</b></h4>

            <div class="p-3">

                {{ Form::open(['method' => 'PUT', 'route' => 'admin.users.updateUser', 'enctype' => 'multipart/form-data']) }}
                {{ Form::token() }}
                {{ Form::hidden('userId', $user->id) }}


                <div class="main-fields">

                </div>

                <div class="form-group row">
                    <div class="col-12">

                        <div class="row">

                            <div class="col-3">
                                <label for="email" class="form-inline">  Email </label>
                            </div>
                            <div class="col-9">
                                <input id="email" type="email" class="form-control" name="email"
                                       value="{{ old('email') !== null ? old('email') : $user->email }}" required
                                       >

                                @if ($errors->has('email'))
                                    <span class="help-block">
    <strong>{{ $errors->first('email') }}</strong>
    </span>
                                @endif
                            </div>
                        </div>



                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-12">

                        <div class="row">

                            <div class="col-3">
                                <label for="role" class="form-inline"> Role </label>
                            </div>
                            <div class="col-9">
                                <select id="role" class="form-control" name="role">

                                    <option value="{{ \App\User::ROLE_USER }}"


                                            {{ $role = old('role') !== null ? old('role') : $user->role }}

                                    @if($role == \App\User::ROLE_USER)

                                        {{ 'selected' }}

                                            @endif

                                    >User
                                    </option>

                                    <option value="{{ \App\User::ROLE_SUPPLIER }}"

                                    @if($role == \App\User::ROLE_SUPPLIER)

                                        {{ 'selected' }}

                                            @endif

                                    >Supplier
                                    </option>

                                    <option value="{{\App\User::ROLE_ADMIN}}"

                                    @if($role == \App\User::ROLE_ADMIN)

                                        {{ 'selected' }}

                                            @endif

                                    >Admin
                                    </option>

                                </select>

                                @if ($errors->has('role'))
                                    <span class="help-block">
    <strong>{{ $errors->first('role') }}</strong>
    </span>
                                @endif
                            </div>
                        </div>



                    </div>
                </div>

                <script type="text/html" id="user-main-fields">

                    <div class="form-group row">
                        <div class="col-12">

                            <div class="row">

                                <div class="col-3">
                                    <label for="first_name" class="form-inline"> First Name </label>
                                </div>
                                <div class="col-9">
                                    <input id="first_name" type="text" class="form-control" name="first_name"
                                           value="{{ old('first_name') !== null ? old('first_name') : $user->first_name }}"
                                           required autofocus
                                           placeholder="First Name">

                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
            <strong>{{ $errors->first('first_name') }}</strong>
    </span>
                                    @endif

                                </div>
                            </div>



                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">

                            <div class="row">

                                <div class="col-3">
                                    <label for="last_name" class="form-inline">  Second Name </label>
                                </div>
                                <div class="col-9">
                                    <input id="last_name" type="text" class="form-control" name="last_name"
                                           value="{{  old('last_name') !== null ? old('last_name') : $user->last_name }}"
                                           required
                                    >

                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
    <strong>{{ $errors->first('last_name') }}</strong>
    </span>
                                    @endif
                                </div>
                            </div>


                        </div>
                    </div>

                </script>

                <script type="text/html" id="supplier-main-fields">


                    <div class="form-group row  profile-container">
                        <div class="col-12">


                            <div class="form-group row">
                                <div class="col-12">

                                    <div class="row">

                                        <div class="col-3">
                                            <label for="last_name" class="form-inline"> Business Name </label>
                                        </div>
                                        <div class="col-9">

                                            <input id="business_name" type="text" class="form-control"
                                                   name="business_name"
                                                   value="{{  old('business_name') !== null ? old('business_name') : $user->profile->business_name  }}"
                                                   required>

                                            @if ($errors->has('business_name'))
                                                <span class="help-block">
    <strong>{{ $errors->first('business_name') }}</strong>
    </span>
                                            @endif

                                        </div>
                                    </div>


                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">

                                    <div class="row">

                                        <div class="col-3">
                                            <label for="last_name" class="form-inline"> Contact Name </label>
                                        </div>
                                        <div class="col-9">

                                            <input id="contact_name" type="text" class="form-control" name="contact_name"
                                                   value="{{  old('contact_name') !== null ? old('contact_name') : $user->profile->contact_name }}"
                                                   required>

                                            @if ($errors->has('contact_name'))
                                                <span class="help-block">
    <strong>{{ $errors->first('contact_name') }}</strong>
    </span>
                                            @endif

                                        </div>
                                    </div>



                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">

                                    <div class="row">

                                        <div class="col-3">
                                            <label for="last_name" class="form-inline"> Link to Site </label>
                                        </div>
                                        <div class="col-9">

                                            <input id="site" type="text" class="form-control" name="site"
                                                   value="{{  old('site') !== null ? old('site') : $user->profile->site }}"
                                                   required>

                                            @if ($errors->has('site'))
                                                <span class="help-block">
    <strong>{{ $errors->first('site') }}</strong>
    </span>
                                            @endif

                                        </div>
                                    </div>


                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-12">

                                    <div class="row">

                                        <div class="col-3">
                                            <label for="last_name" class="form-inline"> Description </label>
                                        </div>
                                        <div class="col-9">
                                        <textarea name="description"
                                                  class="form-control

                                                @if ($errors->has('description'))
                                                  {{ 'error' }}
                                                  @endif
                                                          "
                                        >{{ old('description')  !== null ? old('site') : $user->profile->description}}</textarea>

                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </script>

                <div class="form-group row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-3">
                                <label for="logo" class="form-inline">Logo</label>
                            </div>
                            <div class="col-9">
                                <input id="logo" type="file" class="form-control" name="logo">
                                @if ($errors->has('logo'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('logo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-12">

                        <div class="row">

                            <div class="col-3">
                                <label for="agree" class="form-inline">Agree</label>
                            </div>
                            <div class="col-9">
                                <input type="hidden" name="agree" value="0"/>

                                <input id="agree" type="checkbox" class="form-control aline-checkbox" name="agree"

                                       @if( empty(old('agree')) && $user->agree == \App\User::AGREE_STATUS_TRUE)

                                       checked="checked"

                                       @endif

                                       @if(old('agree') == \App\User::AGREE_STATUS_TRUE && $user->agree == \App\User::AGREE_STATUS_TRUE)

                                       checked="checked"

                                       @endif

                                       @if(old('agree') == \App\User::AGREE_STATUS_TRUE && $user->agree != \App\User::AGREE_STATUS_TRUE)

                                       checked="checked"

                                       @endif

                                       value="{{\App\User::AGREE_STATUS_TRUE }}">


                                @if ($errors->has('agree'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('agree') }}</strong>
                                        </span>
                                @endif

                            </div>
                        </div>


                    </div>
                </div>

                @if($user->isUser())


                    @if($errors->has('organisation')||$errors->has('number_of_registered_ip'))

                        @include('.admin.sections.update-user-profile', ['user' => $user])

                    @endif

                @endif


                <script type="text/html" id="profile-user">

                      @include('.admin.sections.update-user-profile', ['user' => $user])

                </script>

                {{ Form::submit('Update', ['class' => 'btn btn-info create-button-container']) }}
                {{ Form::close() }}

            </div>

        </div>


@endsection
