@extends('layouts.admin')

@section('content')

    {{ Breadcrumbs::render('create-user') }}

    <div class="card wrapper-page">
        <div class="card-body">

            <input type="hidden" value="{{ \App\User::ROLE_USER }}" id="userRole">

            <input type="hidden" value="{{ \App\User::ROLE_SUPPLIER }}" id="supplierRole">

            <input type="hidden" value="{{ \App\User::ROLE_ADMIN }}" id="adminRole">

            <input type="hidden" value="{{ old('role') }}" id="current-role">

            <h3 class="text-center mt-0 m-b-15">
                <img src="{{ asset('admin/images/logo.png') }}"
                     height="30" alt="logo">
            </h3>

            <h4 class="text-muted text-center font-18"><b>Create User</b></h4>

            <div class="p-3">
                <form class="form-horizontal m-t-20 create-user-form" method="POST"
                      action="{{ route('admin.users.createUser') }}">

                    {{ csrf_field() }}


                    <div class="main-fields">

                    </div>


                    <div class="form-group row">
                        <div class="col-12">

                            <div class="row">

                                <div class="col-3">
                                    <label for="email" class="form-inline"> Email </label>
                                </div>
                                <div class="col-9">
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
    <strong>{{ $errors->first('email') }}</strong>
    </span>
                                    @endif

                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">

                            <div class="row">

                                <div class="col-3">
                                    <label for="role" class="form-inline"> Role </label>
                                </div>
                                <div class="col-9">
                                    <select id="role" class="form-control" name="role">


                                        <option value="{{ \App\User::ROLE_USER }}"

                                                selected

                                        @if(old('role') == \App\User::ROLE_USER)

                                            {{ 'selected' }}

                                                @endif

                                        >User
                                        </option>

                                        <option value="{{ \App\User::ROLE_SUPPLIER }}"

                                        @if(old('role') == \App\User::ROLE_SUPPLIER)

                                            {{ 'selected' }}

                                                @endif

                                        >Supplier
                                        </option>

                                        <option value="{{\App\User::ROLE_ADMIN}}"

                                        @if(old('role') == \App\User::ROLE_ADMIN)

                                            {{ 'selected' }}

                                                @endif

                                        >Admin
                                        </option>

                                    </select>

                                    @if ($errors->has('role'))
                                        <span class="help-block">
    <strong>{{ $errors->first('role') }}</strong>
    </span>
                                    @endif

                                </div>
                            </div>


                        </div>
                    </div>

                    <script type="text/html" id="user-main-fields">


                        <div class="form-group row">
                            <div class="col-12">

                                <div class="row">

                                    <div class="col-3">
                                        <label for="first_name" class="form-inline"> First Name </label>
                                    </div>
                                    <div class="col-9">
                                        <input id="first_name" type="text" class="form-control" name="first_name"
                                               value="{{ old('first_name') }}" required autofocus
                                        >

                                        @if ($errors->has('first_name'))
                                            <span class="help-block">
    <strong>{{ $errors->first('first_name') }}</strong>
    </span>
                                        @endif

                                    </div>
                                </div>


                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12">

                                <div class="row">

                                    <div class="col-3">
                                        <label for="last_name" class="form-inline"> Second Name </label>
                                    </div>
                                    <div class="col-9">
                                        <input id="last_name" type="text" class="form-control" name="last_name"
                                               value="{{old('last_name') }}" required
                                        >


                                        @if ($errors->has('last_name'))
                                            <span class="help-block">
    <strong>{{ $errors->first('last_name') }}</strong>
    </span>
                                        @endif

                                    </div>
                                </div>


                            </div>
                        </div>

                    </script>

                    <script type="text/html" id="supplier-main-fields">


                        <div class="form-group row  profile-container-inputs">
                            <div class="col-12">


                                <div class="form-group row">
                                    <div class="col-12">

                                        <div class="row">

                                            <div class="col-3">
                                                <label for="last_name" class="form-inline"> Business Name </label>
                                            </div>
                                            <div class="col-9">

                                                <input id="business_name" type="text" class="form-control"
                                                       name="business_name"
                                                       value="{{  old('business_name') }}"
                                                       required>

                                                @if ($errors->has('business_name'))
                                                    <span class="help-block">
    <strong>{{ $errors->first('business_name') }}</strong>
    </span>
                                                @endif

                                            </div>
                                        </div>


                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-12">

                                        <div class="row">

                                            <div class="col-3">
                                                <label for="last_name" class="form-inline"> Contact Name </label>
                                            </div>
                                            <div class="col-9">

                                                <input id="contact_name" type="text" class="form-control"
                                                       name="contact_name"
                                                       value="{{  old('contact_name') }}"
                                                       required>

                                                @if ($errors->has('contact_name'))
                                                    <span class="help-block">
    <strong>{{ $errors->first('contact_name') }}</strong>
    </span>
                                                @endif

                                            </div>
                                        </div>


                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-12">

                                        <div class="row">

                                            <div class="col-3">
                                                <label for="last_name" class="form-inline"> Link to Site </label>
                                            </div>
                                            <div class="col-9">

                                                <input id="site" type="text" class="form-control" name="site"
                                                       value="{{  old('site') }}"
                                                       required>

                                                @if ($errors->has('site'))
                                                    <span class="help-block">
    <strong>{{ $errors->first('site') }}</strong>
    </span>
                                                @endif

                                            </div>
                                        </div>


                                    </div>
                                </div>



                                <div class="form-group row">
                                    <div class="col-12">

                                        <div class="row">

                                            <div class="col-3">
                                                <label for="last_name" class="form-inline"> Description </label>
                                            </div>
                                            <div class="col-9">
                                        <textarea name="description"
                                                  class="form-control

                                                @if ($errors->has('description'))
                                                  {{ 'error' }}
                                                  @endif
                                                          "
                                        >{{ old('description') }}</textarea>

                                            </div>
                                        </div>


                                    </div>
                                </div>

                            </div>
                        </div>
                    </script>

                    <script type="text/html" id="profile-user">

                        <div class="form-group row  profile-container-inputs">
                            <div class="col-12">

                                <div class="row">

                                    <div class="col-3">
                                        <label for="position" class="form-inline"> Position </label>
                                    </div>
                                    <div class="col-9">
                                        <input id="position" type="text" class="form-control" name="position"
                                               value="{{ old('position') }}">


                                        @if ($errors->has('position'))
                                            <span class="help-block">
    <strong>{{ $errors->first('position') }}</strong>
    </span>
                                        @endif

                                    </div>
                                </div>


                            </div>
                        </div>

                        <div class="form-group row  profile-container-inputs">
                            <div class="col-12">

                                <div class="row">

                                    <div class="col-3">
                                        <label for="organisation" class="form-inline"> Organisation </label>
                                    </div>
                                    <div class="col-9">
                                        <input id="organisation" type="text" class="form-control" name="organisation"
                                               value="{{ old('organisation') }}"
                                        >


                                        @if ($errors->has('organisation'))
                                            <span class="help-block">
    <strong>{{ $errors->first('organisation') }}</strong>
    </span>
                                        @endif
                                    </div>
                                </div>


                            </div>
                        </div>

                        <div class="form-group row  profile-container-inputs">
                            <div class="col-12">

                                <div class="row">

                                    <div class="col-3">
                                        <label for="phone" class="form-inline"> Phone </label>
                                    </div>
                                    <div class="col-9">
                                        <input id="phone" type="text" class="form-control" name="phone"
                                               value="{{ old('phone') }}"
                                        >

                                        @if ($errors->has('phone'))
                                            <span class="help-block">
    <strong>{{ $errors->first('phone') }}</strong>
    </span>
                                        @endif

                                    </div>
                                </div>


                            </div>
                        </div>

                        <div class="form-group row  profile-container-inputs">
                            <div class="col-12">

                                <div class="row">

                                    <div class="col-3">
                                        <label for="number_of_registered_ip" class="form-inline"> Number of registered
                                            IP rights </label>
                                    </div>
                                    <div class="col-9">
                                        <input id="number_of_registered_ip" type="text" class="form-control"
                                               name="number_of_registered_ip"
                                               value="{{ old('number_of_registered_ip') }}"
                                        >

                                        @if ($errors->has('number_of_registered_ip'))
                                            <span class="help-block">
    <strong>{{ $errors->first('number_of_registered_ip') }}</strong>
    </span>
                                        @endif

                                    </div>
                                </div>


                            </div>
                        </div>

                        <div class="form-group row  profile-container-inputs">
                            <div class="col-12">

                                <div class="row">

                                    <div class="col-3">
                                        <label for="patents" class="form-inline"> Patents </label>
                                    </div>
                                    <div class="col-9">
                                        <input id="patents" type="text" class="form-control" name="patents"
                                               value="{{ old('patents') }}">
                                        @if ($errors->has('models'))
                                            <span class="help-block">
    <strong>{{ $errors->first('models') }}</strong>
    </span>
                                        @endif

                                    </div>


                                </div>


                            </div>
                        </div>

                        <div class="form-group row  profile-container-inputs">
                            <div class="col-12">

                                <div class="row">

                                    <div class="col-3">
                                        <label for="models" class="form-inline"> Utility Models (and similar) </label>
                                    </div>
                                    <div class="col-9">
                                        <input id="models" type="text" class="form-control" name="models"
                                               value="{{ old('models') }}">

                                        @if ($errors->has('models'))
                                            <span class="help-block">
    <strong>{{ $errors->first('models') }}</strong>
    </span>
                                        @endif
                                    </div>
                                </div>


                            </div>
                        </div>

                        <div class="form-group row  profile-container-inputs">
                            <div class="col-12">

                                <div class="row">

                                    <div class="col-3">
                                        <label for="industrial_designs" class="form-inline">Industrial Designs </label>
                                    </div>
                                    <div class="col-9">
                                        <input id="industrial_designs" type="text" class="form-control"
                                               name="industrial_designs"
                                               value="{{ old('industrial_designs') }}">
                                        @if ($errors->has('industrial_designs'))
                                            <span class="help-block">
    <strong>{{ $errors->first('industrial_designs') }}</strong>
    </span>
                                        @endif
                                    </div>
                                </div>


                            </div>
                        </div>

                        <div class="form-group row  profile-container-inputs">
                            <div class="col-12">

                                <div class="row">

                                    <div class="col-3">
                                        <label for="trademarks" class="form-inline">Trademarks </label>
                                    </div>
                                    <div class="col-9">
                                        <input id="trademarks" type="text" class="form-control" name="trademarks"
                                               value="{{ old('trademarks') }}">


                                        @if ($errors->has('trademarks'))
                                            <span class="help-block">
    <strong>{{ $errors->first('trademarks') }}</strong>
    </span>
                                        @endif

                                    </div>
                                </div>


                            </div>
                        </div>

                    </script>

                    <div class="form-group row">
                        <div class="col-12">

                            <div class="row">

                                <div class="col-3">
                                    <label for="agree" class="form-inline">Agree</label>
                                </div>
                                <div class="col-9">
                                    <input type="hidden" name="agree" value="0"/>

                                    <input id="agree" type="checkbox" class="form-control aline-checkbox" name="agree"

                                           @if(old('agree') == \App\User::AGREE_STATUS_TRUE)

                                           checked="checked"

                                           @endif

                                           value="{{\App\User::AGREE_STATUS_TRUE }}">


                                    @if ($errors->has('agree'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('agree') }}</strong>
                                        </span>
                                    @endif

                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="form-group text-center row m-t-20 create-button-container">
                        <div class="col-12">
                            <button class="btn btn-info btn-block waves-effect waves-light" type="submit">Create User
                            </button>
                        </div>
                    </div>

                </form>

            </div>

        </div>


@endsection
