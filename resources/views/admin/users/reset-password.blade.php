@extends('layouts.admin')

@section('content')

    {{ Breadcrumbs::render('update-pass', $userId) }}

    <div class="wrapper-page">

        <div class="card">
            <div class="card-body">

                <h3 class="text-center mt-0 m-b-15">
                    <img src="{{ asset('admin/images/logo.png') }}"
                         height="30" alt="logo">
                </h3>

                <div class="p-3">

                    {{ Form::open(['method' => 'PUT', 'route' => 'admin.users.reset-password']) }}
                    {{ Form::token() }}

                    <input type="hidden" value="{{ $userId }}" name="userId">

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                        <div class="col-md-12">
                            <input id="password" type="password" class="form-control" name="password" required placeholder="Password">

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <div class="col-md-12">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Password Confirm">

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    {{ Form::submit('Reset Password', ['class' => 'btn btn-info btn-block ']) }}
                    {{ Form::close() }}

                </div>

            </div>
        </div>
    </div>

@endsection
