@extends('layouts.admin')

@section('content')

    {{ Breadcrumbs::render('main') }}

    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">

                    <h4 class="mt-0 header-title">Users</h4>
                    <div class="panel with-nav-tabs panel-default">
                        <div class="panel-heading">
                     <ul class="nav nav-tabs bordered nav-justified" id="tabs">
                  <li class="col-3 text-center">
  <a style="color:black;" data-toggle="tab" href="#t1" aria-expanded="true">Approved(User)</a>
                </li>
                <li class="col-3 text-center">
  <a style="color:black;" data-toggle="tab" href="#t2" aria-expanded="true">Not approved(User)</a>
                </li>
               <li class="col-3 text-center">
  <a style="color:black;" data-toggle="tab" href="#t3" aria-expanded="true">Approved(Supplier)</a>
                </li>
                <li class="col-3 text-center">
  <a style="color:black;" data-toggle="tab" href="#t4" aria-expanded="true">Not approved(Supplier)</a>
                </li>
            </ul>   
<!--  <div class="datatable-buttons_wrapper"></div>-->

                <div class="tab-content">  
                 <div id="t1" class="tab-pane in active">  
                 <table id="adminUsersTable" class="table table-bordered">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Status</th>
                            <th scope="col">Date of Registration</th>
                            <th scope="col">First Name / Business Name</th>
                            <th scope="col">Last Name / Contact Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Type</th>
                            <th scope="col">State</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users['user_approved'] as $user)
                            <tr>
                                <td> {{ $user->getApprovedStatus()}} </td>
                                <td data-order="{{ $user->getDateOfRegistration() }}">
                                    {{ $user->getDateOfRegistration('M jS, Y \a\t g:ia') }}
                                </td>
                                <td>

                                    @if($user->isUser() || $user->isAdmin())

                                        {{ $user->first_name }}

                                    @endif

                                    @if($user->isSupplier())

                                        {{ $user->profile->business_name }}

                                    @endif
                                </td>
                                <td>

                                    @if($user->isUser() || $user->isAdmin())

                                        {{ $user->last_name }}

                                    @endif

                                    @if($user->isSupplier())

                                        {{ $user->profile->contact_name }}

                                    @endif

                                </td>
                                <td> {{ $user->email }}</td>
                                <td> {{ $user->getRoleName() }}</td>

                                <td>
                                    @if($user->isDisabled())
                                        Disabled
                                    @else
                                        Enabled
                                    @endif

                                </td>

                                <td class="forms-container">
                                    @if($user->isNotApproved())

                                        {{ Form::open(['method' => 'PUT', 'route' => 'admin.users.approve']) }}
                                        {{ Form::token() }}
                                        {{ Form::hidden('userId',  $user->id) }}
                                        {{ Form::button('<i class="fa fa-check"></i>',
                                        [
                                        'type' => 'submit',
                                        'class' => 'btn btn-success btn-lg',
                                        'data-toggle'=>"tooltip",
                                        'data-placement'=>"top",
                                        'title'=>"Approve"
                                        ])}}
                                        {{ Form::close() }}

                                    @endif



                                    @if($user->isUser())
                                        <a href="{{ route('admin.users.open.user', ['userId' => $user->id]) }}">
                                            <button type="button" class="btn btn-primary btn-lg" data-toggle="tooltip"
                                                    data-placement="top" title="Show User requests">
                                                <i class="fa fa-address-book"></i>
                                            </button>
                                        </a>
                                    @endif


                                    @if($user->isSupplier())
                                        <a href="{{ route('admin.users.open.supplier', ['userId' => $user->id]) }}">
                                            <button type="button" class="btn btn-primary btn-lg" data-toggle="tooltip"
                                                    data-placement="top" title="Show Supplier responses">
                                                <i class="fa fa-address-book"></i>
                                            </button>
                                        </a>
                                    @endif


                                    <a href="{{ route('admin.users.edit', ['userId' => $user->id]) }}">
                                        <button type="button" class="btn btn-warning btn-lg" data-toggle="tooltip"
                                                data-placement="top" title="Edit profile">
                                            <i class="	fa fa-pencil"></i>
                                        </button>
                                    </a>

                                    <a href="{{ route('admin.users.password', ['userId' => $user->id]) }}"
                                       data-toggle="tooltip" data-placement="top" title="Change Password">
                                        <button type="button" class="btn btn-primary btn-lg">
                                            <i class="fa fa-asterisk"></i>
                                        </button>
                                    </a>

                                    {{ Form::open(['method' => 'DELETE', 'route' => 'admin.users.remove']) }}
                                    {{ Form::token() }}
                                    {{ Form::hidden('userId',  $user->id) }}
                                    {{ Form::button('<i class="fa fa-trash"></i>',
                                    [
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-lg',
                                        'data-toggle'=>"tooltip",
                                        'data-placement'=>"top",
                                        'title'=>"Delete"
                                    ]
                                    )}}

                                    {{ Form::close() }}

                                    @if($user->isDisabled())

                                        {{ Form::open(['method' => 'PUT', 'route' => 'admin.users.enable']) }}
                                        {{ Form::token() }}
                                        {{ Form::hidden('userId',  $user->id) }}
                                        {{ Form::submit('Enable',
                                        ['class' => 'btn btn-success btn-lg']) }}
                                        {{ Form::close() }}

                                    @else

                                        {{ Form::open(['method' => 'PUT', 'route' => 'admin.users.disable']) }}
                                        {{ Form::token() }}
                                        {{ Form::hidden('userId',  $user->id) }}
                                        {{ Form::submit('Disable', ['class' => 'btn btn-danger btn-lg']) }}
                                        {{ Form::close() }}

                                    @endif

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                       
                        
                    </table>
                      </div>
                 <div id="t2" class="tab-pane fade">  
                <table id="adminUsersTabletwo" class="table table-bordered">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Status</th>
                            <th scope="col">Date of Registration</th>
                            <th scope="col">First Name / Business Name</th>
                            <th scope="col">Last Name / Contact Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Type</th>
                            <th scope="col">State</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users['user_notapproved'] as $user)
                            <tr>
                                <td> {{ $user->getApprovedStatus()}} </td>
                                <td data-order="{{ $user->getDateOfRegistration() }}">
                                    {{ $user->getDateOfRegistration('M jS, Y \a\t g:ia') }}
                                </td>
                                <td>

                                    @if($user->isUser() || $user->isAdmin())

                                        {{ $user->first_name }}

                                    @endif

                                    @if($user->isSupplier())

                                        {{ $user->profile->business_name }}

                                    @endif
                                </td>
                                <td>

                                    @if($user->isUser() || $user->isAdmin())

                                        {{ $user->last_name }}

                                    @endif

                                    @if($user->isSupplier())

                                        {{ $user->profile->contact_name }}

                                    @endif

                                </td>
                                <td> {{ $user->email }}</td>
                                <td> {{ $user->getRoleName() }}</td>

                                <td>
                                    @if($user->isDisabled())
                                        Disabled
                                    @else
                                        Enabled
                                    @endif

                                </td>

                                <td class="forms-container">
                                    @if($user->isNotApproved())

                                        {{ Form::open(['method' => 'PUT', 'route' => 'admin.users.approve']) }}
                                        {{ Form::token() }}
                                        {{ Form::hidden('userId',  $user->id) }}
                                        {{ Form::button('<i class="fa fa-check"></i>',
                                        [
                                        'type' => 'submit',
                                        'class' => 'btn btn-success btn-lg',
                                        'data-toggle'=>"tooltip",
                                        'data-placement'=>"top",
                                        'title'=>"Approve"
                                        ])}}
                                        {{ Form::close() }}

                                    @endif



                                    @if($user->isUser())
                                        <a href="{{ route('admin.users.open.user', ['userId' => $user->id]) }}">
                                            <button type="button" class="btn btn-primary btn-lg" data-toggle="tooltip"
                                                    data-placement="top" title="Show User requests">
                                                <i class="fa fa-address-book"></i>
                                            </button>
                                        </a>
                                    @endif


                                    @if($user->isSupplier())
                                        <a href="{{ route('admin.users.open.supplier', ['userId' => $user->id]) }}">
                                            <button type="button" class="btn btn-primary btn-lg" data-toggle="tooltip"
                                                    data-placement="top" title="Show Supplier responses">
                                                <i class="fa fa-address-book"></i>
                                            </button>
                                        </a>
                                    @endif


                                    <a href="{{ route('admin.users.edit', ['userId' => $user->id]) }}">
                                        <button type="button" class="btn btn-warning btn-lg" data-toggle="tooltip"
                                                data-placement="top" title="Edit profile">
                                            <i class="	fa fa-pencil"></i>
                                        </button>
                                    </a>

                                    <a href="{{ route('admin.users.password', ['userId' => $user->id]) }}"
                                       data-toggle="tooltip" data-placement="top" title="Change Password">
                                        <button type="button" class="btn btn-primary btn-lg">
                                            <i class="fa fa-asterisk"></i>
                                        </button>
                                    </a>

                                    {{ Form::open(['method' => 'DELETE', 'route' => 'admin.users.remove']) }}
                                    {{ Form::token() }}
                                    {{ Form::hidden('userId',  $user->id) }}
                                    {{ Form::button('<i class="fa fa-trash"></i>',
                                    [
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-lg',
                                        'data-toggle'=>"tooltip",
                                        'data-placement'=>"top",
                                        'title'=>"Delete"
                                    ]
                                    )}}

                                    {{ Form::close() }}

                                    @if($user->isDisabled())

                                        {{ Form::open(['method' => 'PUT', 'route' => 'admin.users.enable']) }}
                                        {{ Form::token() }}
                                        {{ Form::hidden('userId',  $user->id) }}
                                        {{ Form::submit('Enable',
                                        ['class' => 'btn btn-success btn-lg']) }}
                                        {{ Form::close() }}

                                    @else

                                        {{ Form::open(['method' => 'PUT', 'route' => 'admin.users.disable']) }}
                                        {{ Form::token() }}
                                        {{ Form::hidden('userId',  $user->id) }}
                                        {{ Form::submit('Disable', ['class' => 'btn btn-danger btn-lg']) }}
                                        {{ Form::close() }}

                                    @endif

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                       
                        
                    </table>
                     </div>
                 <div id="t3" class="tab-pane fade">  
                <table id="adminUsersTablethree" class="table table-bordered">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Status</th>
                            <th scope="col">Date of Registration</th>
                            <th scope="col">First Name / Business Name</th>
                            <th scope="col">Last Name / Contact Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Type</th>
                            <th scope="col">State</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users['supplier_approved'] as $user)
                            <tr>
                                <td> {{ $user->getApprovedStatus()}} </td>
                                <td data-order="{{ $user->getDateOfRegistration() }}">
                                    {{ $user->getDateOfRegistration('M jS, Y \a\t g:ia') }}
                                </td>
                                <td>

                                    @if($user->isUser() || $user->isAdmin())

                                        {{ $user->first_name }}

                                    @endif

                                    @if($user->isSupplier())

                                        {{ $user->profile->business_name }}

                                    @endif
                                </td>
                                <td>

                                    @if($user->isUser() || $user->isAdmin())

                                        {{ $user->last_name }}

                                    @endif

                                    @if($user->isSupplier())

                                        {{ $user->profile->contact_name }}

                                    @endif

                                </td>
                                <td> {{ $user->email }}</td>
                                <td> {{ $user->getRoleName() }}</td>

                                <td>
                                    @if($user->isDisabled())
                                        Disabled
                                    @else
                                        Enabled
                                    @endif

                                </td>

                                <td class="forms-container">
                                    @if($user->isNotApproved())

                                        {{ Form::open(['method' => 'PUT', 'route' => 'admin.users.approve']) }}
                                        {{ Form::token() }}
                                        {{ Form::hidden('userId',  $user->id) }}
                                        {{ Form::button('<i class="fa fa-check"></i>',
                                        [
                                        'type' => 'submit',
                                        'class' => 'btn btn-success btn-lg',
                                        'data-toggle'=>"tooltip",
                                        'data-placement'=>"top",
                                        'title'=>"Approve"
                                        ])}}
                                        {{ Form::close() }}

                                    @endif



                                    @if($user->isUser())
                                        <a href="{{ route('admin.users.open.user', ['userId' => $user->id]) }}">
                                            <button type="button" class="btn btn-primary btn-lg" data-toggle="tooltip"
                                                    data-placement="top" title="Show User requests">
                                                <i class="fa fa-address-book"></i>
                                            </button>
                                        </a>
                                    @endif


                                    @if($user->isSupplier())
                                        <a href="{{ route('admin.users.open.supplier', ['userId' => $user->id]) }}">
                                            <button type="button" class="btn btn-primary btn-lg" data-toggle="tooltip"
                                                    data-placement="top" title="Show Supplier responses">
                                                <i class="fa fa-address-book"></i>
                                            </button>
                                        </a>
                                    @endif


                                    <a href="{{ route('admin.users.edit', ['userId' => $user->id]) }}">
                                        <button type="button" class="btn btn-warning btn-lg" data-toggle="tooltip"
                                                data-placement="top" title="Edit profile">
                                            <i class="	fa fa-pencil"></i>
                                        </button>
                                    </a>

                                    <a href="{{ route('admin.users.password', ['userId' => $user->id]) }}"
                                       data-toggle="tooltip" data-placement="top" title="Change Password">
                                        <button type="button" class="btn btn-primary btn-lg">
                                            <i class="fa fa-asterisk"></i>
                                        </button>
                                    </a>

                                    {{ Form::open(['method' => 'DELETE', 'route' => 'admin.users.remove']) }}
                                    {{ Form::token() }}
                                    {{ Form::hidden('userId',  $user->id) }}
                                    {{ Form::button('<i class="fa fa-trash"></i>',
                                    [
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-lg',
                                        'data-toggle'=>"tooltip",
                                        'data-placement'=>"top",
                                        'title'=>"Delete"
                                    ]
                                    )}}

                                    {{ Form::close() }}

                                    @if($user->isDisabled())

                                        {{ Form::open(['method' => 'PUT', 'route' => 'admin.users.enable']) }}
                                        {{ Form::token() }}
                                        {{ Form::hidden('userId',  $user->id) }}
                                        {{ Form::submit('Enable',
                                        ['class' => 'btn btn-success btn-lg']) }}
                                        {{ Form::close() }}

                                    @else

                                        {{ Form::open(['method' => 'PUT', 'route' => 'admin.users.disable']) }}
                                        {{ Form::token() }}
                                        {{ Form::hidden('userId',  $user->id) }}
                                        {{ Form::submit('Disable', ['class' => 'btn btn-danger btn-lg']) }}
                                        {{ Form::close() }}

                                    @endif

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                       
                        
                    </table>
                     </div>
                 <div id="t4" class="tab-pane fade">  
                <table id="adminUsersTablefour" class="table table-bordered">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Status</th>
                            <th scope="col">Date of Registration</th>
                            <th scope="col">First Name / Business Name</th>
                            <th scope="col">Last Name / Contact Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Type</th>
                            <th scope="col">State</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users['supplier_notapproved'] as $user)
                            <tr>
                                <td> {{ $user->getApprovedStatus()}} </td>
                                <td data-order="{{ $user->getDateOfRegistration() }}">
                                    {{ $user->getDateOfRegistration('M jS, Y \a\t g:ia') }}
                                </td>
                                <td>

                                    @if($user->isUser() || $user->isAdmin())

                                        {{ $user->first_name }}

                                    @endif

                                    @if($user->isSupplier())

                                        {{ $user->profile->business_name }}

                                    @endif
                                </td>
                                <td>

                                    @if($user->isUser() || $user->isAdmin())

                                        {{ $user->last_name }}

                                    @endif

                                    @if($user->isSupplier())

                                        {{ $user->profile->contact_name }}

                                    @endif

                                </td>
                                <td> {{ $user->email }}</td>
                                <td> {{ $user->getRoleName() }}</td>

                                <td>
                                    @if($user->isDisabled())
                                        Disabled
                                    @else
                                        Enabled
                                    @endif

                                </td>

                                <td class="forms-container">
                                    @if($user->isNotApproved())

                                        {{ Form::open(['method' => 'PUT', 'route' => 'admin.users.approve']) }}
                                        {{ Form::token() }}
                                        {{ Form::hidden('userId',  $user->id) }}
                                        {{ Form::button('<i class="fa fa-check"></i>',
                                        [
                                        'type' => 'submit',
                                        'class' => 'btn btn-success btn-lg',
                                        'data-toggle'=>"tooltip",
                                        'data-placement'=>"top",
                                        'title'=>"Approve"
                                        ])}}
                                        {{ Form::close() }}

                                    @endif



                                    @if($user->isUser())
                                        <a href="{{ route('admin.users.open.user', ['userId' => $user->id]) }}">
                                            <button type="button" class="btn btn-primary btn-lg" data-toggle="tooltip"
                                                    data-placement="top" title="Show User requests">
                                                <i class="fa fa-address-book"></i>
                                            </button>
                                        </a>
                                    @endif


                                    @if($user->isSupplier())
                                        <a href="{{ route('admin.users.open.supplier', ['userId' => $user->id]) }}">
                                            <button type="button" class="btn btn-primary btn-lg" data-toggle="tooltip"
                                                    data-placement="top" title="Show Supplier responses">
                                                <i class="fa fa-address-book"></i>
                                            </button>
                                        </a>
                                    @endif


                                    <a href="{{ route('admin.users.edit', ['userId' => $user->id]) }}">
                                        <button type="button" class="btn btn-warning btn-lg" data-toggle="tooltip"
                                                data-placement="top" title="Edit profile">
                                            <i class="	fa fa-pencil"></i>
                                        </button>
                                    </a>

                                    <a href="{{ route('admin.users.password', ['userId' => $user->id]) }}"
                                       data-toggle="tooltip" data-placement="top" title="Change Password">
                                        <button type="button" class="btn btn-primary btn-lg">
                                            <i class="fa fa-asterisk"></i>
                                        </button>
                                    </a>

                                    {{ Form::open(['method' => 'DELETE', 'route' => 'admin.users.remove']) }}
                                    {{ Form::token() }}
                                    {{ Form::hidden('userId',  $user->id) }}
                                    {{ Form::button('<i class="fa fa-trash"></i>',
                                    [
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-lg',
                                        'data-toggle'=>"tooltip",
                                        'data-placement'=>"top",
                                        'title'=>"Delete"
                                    ]
                                    )}}

                                    {{ Form::close() }}

                                    @if($user->isDisabled())

                                        {{ Form::open(['method' => 'PUT', 'route' => 'admin.users.enable']) }}
                                        {{ Form::token() }}
                                        {{ Form::hidden('userId',  $user->id) }}
                                        {{ Form::submit('Enable',
                                        ['class' => 'btn btn-success btn-lg']) }}
                                        {{ Form::close() }}

                                    @else

                                        {{ Form::open(['method' => 'PUT', 'route' => 'admin.users.disable']) }}
                                        {{ Form::token() }}
                                        {{ Form::hidden('userId',  $user->id) }}
                                        {{ Form::submit('Disable', ['class' => 'btn btn-danger btn-lg']) }}
                                        {{ Form::close() }}

                                    @endif

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                       
                        
                    </table>
                     </div>
                    </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                    <a href="{{ route('admin.users.newUser') }}">
                        <input type="button" class="btn btn-success btn-lg" value="Create User">
                    </a>
                </div>
            </div>
        </div>
    </div>


@endsection
