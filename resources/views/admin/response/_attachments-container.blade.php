<div class="col-12">
    <div class="card m-t-30">
        <div class="card-body">

            <label>Add Attachments</label>
            <div class="m-b-30">
                <form action="{{ route('request.attachments') }}" enctype="multipart/form-data" method="POST"
                      class="dropzone">

                    {{ csrf_field() }}

                    <div class="dz-message" data-dz-message><span>Drop files here to upload, or click to browse</span></div>

                    <input type="hidden" value="{{ $request->id }}" name="requestId">

                    <input type="hidden" class="note" name="note" id="note">

                    <div class="fallback">
                        <input name="file" type="file" multiple="multiple">
                    </div>

                </form>
            </div>

            <div class="text-center m-t-15">
                <button type="button" class="btn btn-primary waves-effect waves-light hide" id="sand-attachments">Send
                    Files
                </button>
            </div>
        </div>
    </div>
</div>
