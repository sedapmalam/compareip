<?php
$prefix = 'items.' . $index . '.';
?>

<td class="character-15">

    <input type="hidden" value="{{ $index }}" name="items[{{ $index }}][itemId]">

    @if(isset($response->id))

        <input type="hidden" value="{{ $response->id }}" name="responseId">

        <input type="hidden" value="{{ $response->id }}" name="items[{{ $index }}][responseId]">

    @endif

    <input name="items[{{ $index }}][total_charge_in_invoice]"
           value="{{
                   isset($response->total_charge_in_invoice)
                   ? $response->total_charge_in_invoice
                   :old($prefix . 'total_charge_in_invoice')
                   }}"
           type="text"

           @if(isset($response->response->status)  && $response->response->status == \App\Models\Responses::STATUS_SEND)
           {{
           'readonly'
           }}
           @endif

           class="form-control green

                    @if ($errors->has($prefix . 'total_charge_in_invoice'))
           {{ 'error' }}
           @endif
                   "

           @if ($errors->has($prefix . 'total_charge_in_invoice'))
           data-toggle="tooltip"
           data-placement="top"
           title="{{ $errors->first($prefix . 'total_charge_in_invoice') }}"
            @endif
    >
</td>

<td class="character-15">
    <input name="items[{{ $index }}][official_fee_in_invoice]"
           value="{{
                   isset($response->official_fee_in_invoice)
                   ? $response->official_fee_in_invoice
                   :old($prefix . 'official_fee_in_invoice')
                   }}"

           @if(isset($response->response->status)  && $response->response->status == \App\Models\Responses::STATUS_SEND)
           {{
           'readonly'
           }}
           @endif

           class="form-control green

                    @if ($errors->has($prefix . 'official_fee_in_invoice'))
           {{ 'error' }}
           @endif
                   "
           type="text"

           @if ($errors->has($prefix . 'official_fee_in_invoice'))
           data-toggle="tooltip"
           data-placement="top"
           title="{{ $errors->first($prefix . 'official_fee_in_invoice') }}"
            @endif
    >

</td>

<td class="character-15">
    <input name="items[{{ $index }}][locale_cost_in_invoice]"
           value="{{
                   isset($response->locale_cost_in_invoice)
                   ? $response->locale_cost_in_invoice
                   :old($prefix . 'locale_cost_in_invoice')
                   }}"
           type="text"

           @if(isset($response->response->status)  && $response->response->status == \App\Models\Responses::STATUS_SEND)
           {{
           'readonly'
           }}
           @endif

           class="form-control green

                    @if ($errors->has($prefix . 'locale_cost_in_invoice'))
           {{ 'error' }}
           @endif
                   "

           @if ($errors->has($prefix . 'locale_cost_in_invoice'))
           data-toggle="tooltip"
           data-placement="top"
           title="{{ $errors->first($prefix . 'locale_cost_in_invoice') }}"
            @endif
    >

</td>

<td class="character-15">
    <input name="items[{{ $index }}][service_fee_in_invoice]"
           value="{{
                   isset($response->service_fee_in_invoice)
                   ? $response->service_fee_in_invoice
                   :old($prefix . 'service_fee_in_invoice')
                   }}"
           type="text"

           @if(isset($response->response->status)  && $response->response->status == \App\Models\Responses::STATUS_SEND)
           {{
           'readonly'
           }}
           @endif

           class="form-control green

                    @if ($errors->has($prefix . 'service_fee_in_invoice'))
           {{ 'error' }}
           @endif
                   "

           @if ($errors->has($prefix . 'service_fee_in_invoice'))
           data-toggle="tooltip"
           data-placement="top"
           title="{{ $errors->first($prefix . 'service_fee_in_invoice') }}"
            @endif
    >

</td>

<td class="character-15">
    <input name="items[{{ $index }}][official_fee_amount]"
           value="{{
                   isset($response->official_fee_amount)
                   ? $response->official_fee_amount
                   :old($prefix . 'official_fee_amount')
                   }}"
           type="text"

           @if(isset($response->response->status)  && $response->response->status == \App\Models\Responses::STATUS_SEND)
           {{
           'readonly'
           }}
           @endif

           class="form-control green

                    @if ($errors->has($prefix . 'official_fee_amount'))
           {{ 'error' }}
           @endif
                   "

           @if ($errors->has($prefix . 'official_fee_amount'))
           data-toggle="tooltip"
           data-placement="top"
           title="{{ $errors->first($prefix . 'official_fee_amount') }}"
            @endif
    >
</td>

<td class="character-15">

    <input name="items[{{ $index }}][official_fee_currency]"
           value="{{
                   isset($response->official_fee_currency)
                   ? $response->official_fee_currency
                   : old($prefix . 'official_fee_currency')
                   }}"

           @if(isset($response->response->status)  && $response->response->status == \App\Models\Responses::STATUS_SEND)
           {{
           'readonly'
           }}
           @endif

           class="form-control green

                    @if ($errors->has($prefix . 'official_fee_currency'))
           {{ 'error' }}
           @endif
                   "
           type="text"

           @if ($errors->has($prefix . 'official_fee_currency'))
           data-toggle="tooltip"
           data-placement="top"
           title="{{ $errors->first($prefix . 'official_fee_currency') }}"
            @endif
    >

</td>

<td class="character-15">
    <input name="items[{{ $index }}][local_cost_amount]"
           value="{{
                   isset($response->local_cost_amount)
                   ? $response->local_cost_amount
                   :old($prefix . 'local_cost_amount')
                   }}"

           @if(isset($response->response->status)  && $response->response->status == \App\Models\Responses::STATUS_SEND)
           {{
           'readonly'
           }}
           @endif

           class="form-control green

                    @if ($errors->has($prefix . 'local_cost_amount'))
           {{ 'error' }}
           @endif
                   "
           type="text"

           @if ($errors->has($prefix . 'local_cost_amount'))
           data-toggle="tooltip"
           data-placement="top"
           title="{{ $errors->first($prefix . 'local_cost_amount') }}"
            @endif
    >
</td>
<td class="character-15">
    <input name="items[{{ $index }}][local_cost_currency]"
           value="{{
                   isset($response->local_cost_currency)
                   ? $response->local_cost_currency
                   :old($prefix . 'local_cost_currency')
                   }}"
           type="text"

           @if(isset($response->response->status)  && $response->response->status == \App\Models\Responses::STATUS_SEND)
           {{
           'readonly'
           }}
           @endif

           class="form-control green

                    @if ($errors->has($prefix . 'local_cost_currency'))
           {{ 'error' }}
           @endif
                   "

           @if ($errors->has($prefix . 'local_cost_currency'))
           data-toggle="tooltip"
           data-placement="top"
           title="{{ $errors->first($prefix . 'local_cost_currency') }}"
            @endif
    >

</td>
