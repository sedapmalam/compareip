@extends('layouts.admin')

@section('content')

    @if(\Illuminate\Support\Facades\Auth::user()->isSupplier())

        @if(!$errors->isEmpty())
            <div class="card m-b-30">
                <div class="card-body">
                    <div class="alert alert-danger" role="alert">
                        Please check the highlighted  fields
                    </div>
                </div>
            </div>
        @endif


        <div class="float-right m-r-15 download-pattern-container">

            <a href="{{ route('request.download', ['requestId' => $request->id, 'supplierId' => $supplierId]) }}">
                <button type="button" class="btn btn-primary btn-lg download-button m-r-5"
                        onclick="$(window).off('beforeunload'); ">
                    Download
                </button>
            </a>

        </div>

        <div class="float-right m-r-15 upload-pattern-container">

            @if($response === null || $response->status == \App\Models\Responses::STATUS_STORED)
                <form method="POST"
                      action="{{ route('response.upload', ['requestId' => $request->id, 'supplierId' => $supplierId]) }}"
                      enctype="multipart/form-data" class="upload-excel"
                      id="upload-excel">

                    <input type="hidden" id="upload-url"
                           data-upload-url="{{ route('response.upload', ['requestId' => $request->id, 'supplierId' => $supplierId]) }}">

                    <input type="file" class="form-input hide" name="form" id="form-input"
                           title="Upload"
                    >

                    <input type="button" class=" btn-primary upload-form m-r-5" name="form"
                           onclick="document.getElementById('form-input').click();"
                           value="Upload"
                    >

                </form>
            @endif
        </div>

        @if(\Illuminate\Support\Facades\Auth::user()->isSupplier() &&  (!$response || $response->status != \App\Models\Responses::STATUS_SEND))

            <div class="float-right m-r-15">

                <input type="button" name="action" onclick="$(window).off('beforeunload'); "
                       value="Exit"
                       class="btn btn-danger btn-lg exit-out-form m-r-5">

            </div>

            <div class="float-right m-r-15">
                <input type="button" name="action" onclick="$(window).off('beforeunload'); "
                       value="{{ ucfirst(\App\Models\Responses::STATUS_SEND) }}"
                       class="btn btn-success btn-lg sand-out-form m-r-5">
            </div>

            <div class="float-right m-r-15">
                <input type="button" name="action" onclick="$(window).off('beforeunload'); "
                       value="{{ ucfirst(\App\Models\Responses::STATUS_STORED) }}"
                       class="btn btn-success btn-lg store-out-form m-r-5">
            </div>



        @endif
    @endif

    @if(\Illuminate\Support\Facades\Auth::user()->isAdmin())
        {{ Breadcrumbs::render('response-item', $request, $supplierId) }}
    @elseif(\Illuminate\Support\Facades\Auth::user()->isUser())
        {{ Breadcrumbs::render('request-item-response', $request) }}
    @else
        {{ Breadcrumbs::render('response-item-basic') }}
    @endif

    <div class="row">

        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">

                    <h4 class="mt-0 header-title">Items</h4>

                    <form method="POST" action="{{ route('request.send') }}">

                        {{ csrf_field() }}

                        <input type="hidden" value="{{ $request->id }}" name="requestId">
                        <div class="table-container">

                            <table class="table table-bordered datatable-buttons-response">

                                <thead class="thead-dark">
                                <tr>

                                    <th>{{ \App\Models\RequestItems::COLUMN_NAME_ITEM_NUMBER_CODE }}</th>

                                    @if(\Illuminate\Support\Facades\Auth::user()->isUser())
                                        <th>{{ \App\Models\RequestItems::COLUMN_NAME_IP_NUMBER }}</th>
                                    @endif

                                    <th>{{ \App\Models\RequestItems::COLUMN_NAME_NEXT_RENEWAL_DATE }}</th>
                                    <th>{{ \App\Models\RequestItems::COLUMN_NAME_COUNTRY }}</th>
                                    <th>{{ \App\Models\RequestItems::COLUMN_NAME_STATUS }}</th>
                                    <th>{{ \App\Models\RequestItems::COLUMN_NAME_CASE_TYPE }}</th>
                                    <th>{{ \App\Models\RequestItems::COLUMN_NAME_ENTITY_SIZE }}</th>
                                    <th>{{ \App\Models\RequestItems::COLUMN_NAME_NEXT_ANNUITY_YEAR }}</th>
                                    <th class="space">{{ \App\Models\RequestItems::COLUMN_NAME_NUMBER_OF_CLAIMS }}</th>
                                    <th width="40">{{ \App\Models\RequestItems::COLUMN_NAME_INVOICE_CURRENCY }}</th>

                                    @if(\Illuminate\Support\Facades\Auth::user()->isUser())
                                        <th>{{ \App\Models\RequestItems::COLUMN_NAME_ESTIMATED_OR_INVOICED_TOTAL }}</th>
                                    @endif

                                    <th class="green-th">{{ \App\Models\ResponseItems::TOTAL_CHARGE_IN_INVOICE_CURRENCY }}</th>
                                    <th class="green-th">{{ \App\Models\ResponseItems::OFFICIAL_FEE_IN_INVOICE_CURRENCY }}</th>
                                    <th class="green-th">{{ \App\Models\ResponseItems::LOCAL_COSTS_IN_INVOICE_CURRENCY }}</th>
                                    <th class="green-th">{{ \App\Models\ResponseItems::SERVICE_FEE_IN_INVOICE_CURRENCY }}</th>
                                    <th class="green-th">{{ \App\Models\ResponseItems::OFFICIAL_FEE_AMOUNT }}</th>
                                    <th class="green-th">{{ \App\Models\ResponseItems::OFFICIAL_FEE_CURRENCY }}</th>
                                    <th class="green-th">{{ \App\Models\ResponseItems::LOCAL_COST_AMOUNT }}</th>
                                    <th class="green-th">{{ \App\Models\ResponseItems::LOCAL_COST_CURRENCY }}</th>

                                </tr>
                                </thead>

                                <tbody

                                        @if(\Illuminate\Support\Facades\Auth::user()->isSupplier())
                                        id="request"
                                        @endif
                                >


                                @include('.admin.response._request-response-table', ['items' => $items, 'response' => $response])

                                </tbody>
                            </table>
                        </div>

                        @if((\Illuminate\Support\Facades\Auth::user()->isSupplier()
                        &&  (!$response || $response->status != \App\Models\Responses::STATUS_SEND)))

                            <div class="col-12">
                                <div class="card m-t-30">
                                    <div class="card-body">


                                        <div class="col-12 m-b-30 no-padding">

                                            <label>Create Message</label>
                                            <textarea name="note"

                                                      class="form-control note-attachment m-b-30

                    @if ($errors->has('note'))
                                                      {{ 'error' }}
                                                      @endif
                                                              ">@if($response){{ $response->note }}@endif</textarea>

                                            @if ($errors->has('note'))
                                                <span class="help-block">
                            <strong>{{ $errors->first('note') }}</strong>
                        </span>
                                            @endif

                                            <p class="text-muted m-b-30 font-14"><span class="word-counter"></span>
                                                characters left</p>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endif

                        <input type="submit" name="action"
                               value="{{ \App\Models\Responses::STATUS_SEND }}"
                               class="btn btn-success btn-lg sand-from-form hide">

                        <input type="submit" name="action"
                               value="{{ \App\Models\Responses::STATUS_STORED }}"
                               class="btn btn-success btn-lg store-from-form hide">

                        <input type="submit" name="action"
                               value="{{ \App\Models\Responses::ACTION_EXIT }}"
                               class="btn btn-danger btn-lg exit-from-form hide">

                    </form>


                    @if(\Illuminate\Support\Facades\Auth::user()->isSupplier() &&  (!$response || $response->status != \App\Models\Responses::STATUS_SEND))

                        @include('admin.response._attachments-container')

                        <div class="col-12 m-t-30">
                            <input type="button" name="action" onclick="$(window).off('beforeunload'); "
                                   value="{{ ucfirst(\App\Models\Responses::STATUS_SEND) }}"
                                   class="btn btn-success btn-lg sand-out-form">

                            <input type="button" name="action" onclick="$(window).off('beforeunload'); "
                                   value="{{ ucfirst(\App\Models\Responses::STATUS_STORED) }}"
                                   class="btn btn-success btn-lg store-out-form">

                            <input type="button" name="action" onclick="$(window).off('beforeunload'); "
                                   value="Exit"
                                   class="btn btn-danger btn-lg exit-out-form">

                        </div>

                    @endif


                    @if(!$attachmentsRequests->count()==0)

                        <div class="col-12">
                            <div class="card m-t-30">
                                <div class="card-body attachments-container-wrapper">

                                    <label>Attachments</label>
                                    <div class="datatable-buttons_wrapper datatable-buttons-attachments_wrapper"></div>

                                    <table class="table datatable-buttons-attachments table">

                                        <thead class="thead-dark">
                                        <tr>
                                            <th>File Name</th>
                                            <th>Download</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($attachmentsRequests as $attachmentsRequest)

                                            <tr>
                                                <td> {{ $attachmentsRequest->attachment->name }}</td>

                                                <td class="manage forms-container">

                                                    @if(\Illuminate\Support\Facades\Auth::user()->isSupplier() &&  (!$response || $response->status != \App\Models\Responses::STATUS_SEND))

                                                        {{ Form::open(['method' => 'DELETE', 'route' => 'attachment.remove']) }}
                                                        {{ Form::token() }}
                                                        {{ Form::hidden('attachmentId',  $attachmentsRequest->attachment->id) }}
                                                        {{ Form::button('<i class="fa fa-trash"></i>',

                                                        [
                                                            'type' => 'submit',
                                                            'class' => 'btn btn-danger btn-lg nobeforeunload',
                                                            'data-toggle'=>"tooltip",
                                                            'data-placement'=>"top",
                                                            'title'=>"Delete attachment"
                                                        ]

                                                        )}}

                                                        {{ Form::close() }}

                                                    @endif

                                                    <a href="{{ route('download', ['attachmentId' => $attachmentsRequest->attachment->id]) }}">
                                                        <button type="button"
                                                                class="btn btn-success btn-lg nobeforeunload">
                                                            <i class="fa fa-download"></i>
                                                        </button>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>


                                </div>
                            </div>

                        </div>

                    @endif

                    @if(!empty($response->note))
                        <div class="col-12">
                            <div class="card m-t-30">
                                <div class="card-body">
                                    <label>Message Preview</label>

                                    <textarea readonly
                                              class="form-control min-height">{{ $response->note }}</textarea>
                                </div>
                            </div>
                        </div>
                    @endif

                </div>
            </div>

        </div>
    </div>

@endsection
