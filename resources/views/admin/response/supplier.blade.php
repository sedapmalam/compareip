@extends('layouts.admin')

@section('content')

    {{ Breadcrumbs::render('response') }}

    @if(session('status') && session('status') == \App\Models\RequestItems::VIEW_STATUS_SANDED)
        <div class="card m-b-30">
            <div class="card-body">
                <div class="alert alert-success" role="alert">
                    Your Response has been sent to the user!
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">

                    <h4 class="mt-0 header-title">Supplier Responses</h4>

                    <div class="datatable-buttons_wrapper"></div>

                    <table  class="table table-bordered datatable-buttons">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col" width="100">User Name</th>
                            <th scope="col" width="280">Portfolio</th>
                            <th scope="col">Response date</th>
                            <th scope="col">Declined</th>
                            <th scope="col" width="210">Manage</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($suppliersRequests as $suppliersRequest)

                            @if($suppliersRequest->request->user->isActive())

                                <tr>
                                    <td>
                                        {{ $suppliersRequest->request->user->first_name }}
                                        {{ $suppliersRequest->request->user->last_name }}
                                    </td>
                                    <td>
                                        <ul class="no-bullets">
                                            <li>
                                                Number of registered IP rights :
                                                @if($suppliersRequest->request->user->profile->number_of_registered_ip)
                                                    {{ $suppliersRequest->request->user->profile->number_of_registered_ip }}
                                                @else
                                                    0
                                                @endif
                                            </li>
                                            <li>
                                                Patents :
                                                @if($suppliersRequest->request->user->profile->patents)
                                                    {{ $suppliersRequest->request->user->profile->patents }}
                                                @else
                                                    0
                                                @endif
                                            </li>
                                            <li>
                                                Utility Models (and similar) :
                                                @if($suppliersRequest->request->user->profile->models)
                                                    {{ $suppliersRequest->request->user->profile->models }}
                                                @else
                                                    0
                                                @endif
                                            </li>
                                            <li>
                                                Industrial Designs :
                                                @if($suppliersRequest->request->user->profile->industrial_designs)
                                                    {{ $suppliersRequest->request->user->profile->industrial_designs }}
                                                @else
                                                    0
                                                @endif
                                            </li>
                                            <li>
                                                Trademarks :
                                                @if($suppliersRequest->request->user->profile->trademarks)
                                                    {{ $suppliersRequest->request->user->profile->trademarks }}
                                                @else
                                                    0
                                                @endif
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        @if($suppliersRequest->status == App\Models\SuppliersRequests::STATUS_DECLINED)
                                            {{ $suppliersRequest->updated_at->format('d/m/Y') }}
                                        @else
                                            @foreach($suppliersRequest->request->responses->where('supplier_id', $supplierId) as $response)
                                                @if($response->responded)
                                                    {{ $response->responded->format('d/m/Y') }}
                                                @endif
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if($suppliersRequest->status == App\Models\SuppliersRequests::STATUS_DECLINED)
                                            <span class="text-danger"><strong>Yes</strong></span>
                                        @else
                                            <span class="text-success"><strong>No</strong></span>
                                        @endif
                                    </td>

                                    <td class="forms-container manage">
                                        <a href="{{ route('response.request', ['requestId' => $suppliersRequest->request->id, 'supplierId' => $supplierId]) }}">
                                            <button type="button" class="btn btn-success btn-lg" data-toggle="tooltip" data-placement="top"
                                                    @foreach($suppliersRequest->request->responses->where('supplier_id', $supplierId) as $response)
                                                    @if($response->status && $response->status == \App\Models\Responses::STATUS_SEND)
                                                    title="Show response"
                                                    @else
                                                    title="Show request"
                                                    @endif
                                                    @endforeach>
                                                View
                                            </button>
                                        </a>

                                        @if(!\App\Models\Responses::isSandStatic($suppliersRequest->request->id, $suppliersRequest->supplier->id))
                                            @if(!Auth::user()->isAdmin())
                                                {{ Form::open(['method' => 'PUT', 'route' => 'response.decline']) }}
                                                {{ Form::token() }}
                                                {{ Form::hidden('requestSupplierId', $suppliersRequest->id) }}
                                                {{ Form::button('Decline', ['class' => 'btn btn-danger btn-lg decline_request']) }}
                                                {{ Form::close() }}
                                            @endif
                                        @endif

                                    </td>
                                </tr>

                            @endif
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
