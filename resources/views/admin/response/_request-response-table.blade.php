@foreach($items as $item)

    <tr class="request-item">

        <td class="character-5">{{ $item->item_number_code }} </td>

        @if(\Illuminate\Support\Facades\Auth::user()->isUser())
            <td>{{ $item->ip_number }}</td>
        @endif

        <td class="character-10">{{ $item->next_renewal_date}}</td>
        <td class="character-30">{{ $item->country }}</td>
        <td class="character-12">{{ $item->statusViewName() }}</td>
        <td class="character-20">{{ $item->case_type }}</td>
        <td class="character-10">{{ $item->entity_size }}</td>
        <td class="character-4">{{ $item->next_annuity_year }}</td>
        <td class="character-10">{{ $item->number_of_claims }}</td>
        <td class="character-4">{{ $item->invoice_currency }}</td>

        @if(\Illuminate\Support\Facades\Auth::user()->isUser())
            <td>{{ is_numeric($item->estimated_or_invoiced_total) ? number_format($item->estimated_or_invoiced_total, 2, '.', '') : $item->estimated_or_invoiced_total }}</td>
        @endif

        @if(!\Illuminate\Support\Facades\Auth::user()->isSupplier())

            @foreach($item->requestItems->where('supplier_id', $supplierId) as $responseItem)

                @if($responseItem->request_item_id == $item->id)

                    <td class="character-15 green">{{ $responseItem->total_charge_in_invoice }}</td>
                    <td class="character-15 green">{{ $responseItem->official_fee_in_invoice }}</td>
                    <td class="character-15 green">{{ $responseItem->locale_cost_in_invoice }}</td>
                    <td class="character-15 green">{{ $responseItem->service_fee_in_invoice }}</td>
                    <td class="character-15 green">{{ $responseItem->official_fee_amount }}</td>
                    <td class="character-15 green">{{ $responseItem->official_fee_currency }}</td>
                    <td class="character-15 green">{{ $responseItem->local_cost_amount }}</td>
                    <td class="character-15 green">{{ $responseItem->local_cost_currency }}</td>

                    @break

                @endif

            @endforeach

        @else

            @if($item->requestItems->where('supplier_id', $supplierId)->count() !== 0)

                @foreach($item->requestItems->where('supplier_id', $supplierId) as $responseItem)

                    @if($responseItem->request_item_id == $item->id)

                        @include('.admin.response._responses', ['index' => $item->id, 'response' => $responseItem])

                        @break

                    @endif

                @endforeach

            @elseif(!$item->requestItems->where('supplier_id', $supplierId)->first() || $item->requestItems->count() == 0)

                @include('.admin.response._responses', ['index' => $item->id])

            @endif

        @endif

    </tr>

@endforeach
