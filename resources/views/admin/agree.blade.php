@extends('layouts.admin')

@section('content')


    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                    @include('partials.tc')
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                    {{ Form::open(['method' => 'PUT', 'route' => 'set.agree']) }}
                        {{ Form::token() }}
                        {{ Form::hidden('userId', $userId) }}
                        {{ Form::submit('Agree', ['class' => 'btn btn-success btn-lg']) }}
                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>



@endsection
