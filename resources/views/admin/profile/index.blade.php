@extends('layouts.admin')

@section('content')

    {{ Breadcrumbs::render('profile') }}

    <div class="card wrapper-page">
        <div class="card-body">

            <h3 class="text-center mt-0 m-b-15">
                <img src="{{ asset('admin/images/logo.png') }}"
                     height="30" alt="logo">
            </h3>

            <h4 class="text-muted text-center font-18"><b>Profile</b></h4>

            <div class="p-3">

                {{ Form::open(['method' => 'PUT', 'route' => 'profile.update', 'enctype' => 'multipart/form-data']) }}
                {{ Form::token() }}

                <input type="hidden" value="{{ $user->id }}" name="userId">

                @if(\Illuminate\Support\Facades\Auth::user()->isUser())

                <div class="form-group row">
                    <div class="col-12">

                        <div class="row">

                            <div class="col-3">
                                <label for="first_name" class="form-inline"> First Name </label>
                            </div>
                            <div class="col-9">
                                <input id="first_name" type="text" class="form-control" name="first_name"
                                       value="{{ old('first_name') !== null ? old('first_name') : $user->first_name }}"
                                       required autofocus>

                                @if ($errors->has('first_name'))
                                    <span class="help-block">
            <strong>{{ $errors->first('first_name') }}</strong>
    </span>
                                @endif
                            </div>
                        </div>



                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-12">

                        <div class="row">

                            <div class="col-3">
                                <label for="last_name" class="form-inline"> Second Name </label>
                            </div>
                            <div class="col-9">

                                <input id="last_name" type="text" class="form-control" name="last_name"
                                       value="{{  old('last_name') !== null ? old('last_name') : $user->last_name }}"
                                       required>

                                @if ($errors->has('last_name'))
                                    <span class="help-block">
    <strong>{{ $errors->first('last_name') }}</strong>
    </span>
                                @endif

                            </div>
                        </div>



                    </div>
                </div>

                @endif

                @if(\Illuminate\Support\Facades\Auth::user()->isSupplier())

                    <div class="form-group row">
                        <div class="col-12">

                            <div class="row">

                                <div class="col-3">
                                    <label for="last_name" class="form-inline"> Business Name </label>
                                </div>
                                <div class="col-9">

                                    <input id="business_name" type="text" class="form-control" name="business_name"
                                               value="{{  old('business_name') !== null ? old('business_name') : $user->profile->business_name }}"
                                           required>

                                    @if ($errors->has('business_name'))
                                        <span class="help-block">
    <strong>{{ $errors->first('business_name') }}</strong>
    </span>
                                    @endif

                                </div>
                            </div>



                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">

                            <div class="row">

                                <div class="col-3">
                                    <label for="last_name" class="form-inline"> Contact Name </label>
                                </div>
                                <div class="col-9">

                                    <input id="contact_name" type="text" class="form-control" name="contact_name"
                                           value="{{  old('contact_name') !== null ? old('contact_name') : $user->profile->contact_name }}"
                                           required>

                                    @if ($errors->has('contact_name'))
                                        <span class="help-block">
    <strong>{{ $errors->first('contact_name') }}</strong>
    </span>
                                    @endif

                                </div>
                            </div>



                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">

                            <div class="row">

                                <div class="col-3">
                                    <label for="last_name" class="form-inline"> Link to Site </label>
                                </div>
                                <div class="col-9">

                                    <input id="site" type="text" class="form-control" name="site"
                                           value="{{  old('site') !== null ? old('site') : $user->profile->site }}"
                                           required>

                                    @if ($errors->has('site'))
                                        <span class="help-block">
    <strong>{{ $errors->first('site') }}</strong>
    </span>
                                    @endif

                                </div>
                            </div>



                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-12">

                            <div class="row">

                                <div class="col-3">
                                    <label for="last_name" class="form-inline"> Description </label>
                                </div>
                                <div class="col-9">
                                        <textarea name="description"
                                                  class="form-control

                                                @if ($errors->has('description'))
                                                  {{ 'error' }}
                                                  @endif
                                                          "
                                        >{{ old('description')  !== null ? old('site') : $user->profile->description}}</textarea>

                                </div>
                            </div>


                        </div>
                    </div>

                @endif


                <div class="form-group row">
                    <div class="col-12">

                        <div class="row">

                            <div class="col-3">
                                <label for="email" class="form-inline"> Email </label>
                            </div>
                            <div class="col-9">
                                <input id="email" type="email" class="form-control" name="email"
                                       value="{{ old('email') !== null ? old('email') : $user->email }}" required
                                       ">

                                @if ($errors->has('email'))
                                    <span class="help-block">
    <strong>{{ $errors->first('email') }}</strong>
    </span>
                                @endif
                            </div>
                        </div>



                    </div>
                </div>

                @if(\Illuminate\Support\Facades\Auth::user()->isUser())

                    @include('.admin.sections.update-user-profile', ['user' => $user])

                @endif

                <div class="form-group row">


                    <div class="col-md-12 ">

                        <div class="row">

                            <div class="col-3">

                                <label for="avatar" class="form-inline"> Logo </label>

                            </div>
                            <div class="col-9">

                                <input id="avatar" type="file" class="form-control" name="avatar" title="Search avatar img"
                                >
                                @if ($errors->has('avatar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('avatar') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>



                    </div>
                </div>

                {{ Form::submit('Update', ['class' => 'btn btn-info ']) }}

                {{ Form::close() }}

            </div>

        </div>


@endsection
