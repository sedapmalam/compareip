<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>CompareIP</title>
    <meta content="Admin Dashboard" name="description" />
    <meta content="ThemeDesign" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="{{ asset('/img/icons/favicon.ico')}}" type="image/x-icon">

</head>
<body>

<style>
    body {
        margin: 0;
        height: 100vh !important;
    }
    .container{
        margin-top: 10px;
        padding-left: 5px;
        min-height: 30px;
    }

    .container a{
        font-family: "Calibri Light";
        text-decoration: none;
        color: grey;
    }
</style>

<div class="container">
    <a href="{{ route('dashboard') }}">Back to CompareIP Dashboard</a>
</div>

<iframe src="{{ $site }}" style="border: 0; width: 100%; height: 100%">Your browser doesn't support iFrames.</iframe>


</body>
</html>
