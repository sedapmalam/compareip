@extends('layouts.admin')

@section('content')


    <div class="float-right m-r-15">

        <a href="{{ route('response.download', ['requestId' => $request->id]) }}">
            <button type="button" class="btn btn-success btn-lg">
                Export
            </button>
        </a>

    </div>

    @if(\Illuminate\Support\Facades\Auth::user()->isAdmin())
        {{ Breadcrumbs::render('request-item', $request) }}
    @else
        {{ Breadcrumbs::render('request-item-basic') }}
    @endif


    <div class="row">

        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">

                    <h4 class="mz-1 header-title">Items</h4>
                    <br>
                    <div class="datatable-buttons-request_wrapper datatable-buttons_wrapper"></div>

                    <table class="table table-bordered datatable-request-buttons">



                        <thead class="thead-dark">
                        <tr>

                            <th>{{ \App\Models\RequestItems::COLUMN_NAME_ITEM_NUMBER_CODE }}</th>
                            <th>{{ \App\Models\RequestItems::COLUMN_NAME_IP_NUMBER }}</th>
                            <th>{{ \App\Models\RequestItems::COLUMN_NAME_NEXT_RENEWAL_DATE }}</th>
                            <th>{{ \App\Models\RequestItems::COLUMN_NAME_COUNTRY }}</th>
                            <th>{{ \App\Models\RequestItems::COLUMN_NAME_STATUS }}</th>
                            <th>{{ \App\Models\RequestItems::COLUMN_NAME_CASE_TYPE }}</th>
                            <th>{{ \App\Models\RequestItems::COLUMN_NAME_ENTITY_SIZE }}</th>
                            <th>{{ \App\Models\RequestItems::COLUMN_NAME_NEXT_ANNUITY_YEAR }}</th>
                            <th width="80"
                                class="space">{{ \App\Models\RequestItems::COLUMN_NAME_NUMBER_OF_CLAIMS }}</th>

                            <th>{{ \App\Models\RequestItems::COLUMN_NAME_INVOICE_CURRENCY }}</th>
                            <th>{{ \App\Models\RequestItems::COLUMN_NAME_ESTIMATED_OR_INVOICED_TOTAL }}</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            <tr>
                                <td>{{ $item->item_number_code }} </td>
                                <td>{{ $item->ip_number }}</td>
                                <td>{{ $item->next_renewal_date  }}</td>
                                <td>{{ $item->country }}</td>
                                <td>{{ $item->statusViewName() }}</td>
                                <td>{{ $item->case_type }}</td>
                                <td>{{ $item->entity_size }}</td>
                                <td>{{ $item->next_annuity_year }}</td>
                                <td>{{ $item->number_of_claims }}</td>
                                <td>{{ $item->invoice_currency }}</td>
                                <td>{{ is_numeric($item->estimated_or_invoiced_total) ? number_format($item->estimated_or_invoiced_total, 2, '.', '') : $item->estimated_or_invoiced_total }}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">

                    <h4 class="mz-1 header-title">Summary</h4>
                    <br>
                    <div class="datatable-buttons_wrapper datatable-buttons-summary_wrapper"></div>
                    <table class="table  table-bordered datatable-buttons-summary">

                        <thead class="thead-dark">
                        <tr>
                            <th width="50">{{ \App\Models\RequestItems::COLUMN_NAME_ITEM_NUMBER_CODE }}</th>
                            <th width="50">{{ \App\Models\RequestItems::COLUMN_NAME_ESTIMATED_OR_INVOICED_TOTAL  }}</th>
                            @foreach($suppliers as $supplier)
                                <th> {{ $supplier->profile->business_name }}</th>
                            @endforeach

                        </tr>

                        </thead>

                        <tbody>

                        @foreach($items as $item)
                            <tr>

                                <td>{{ $item->item_number_code }}</td>

                                <td>{{ is_numeric($item->estimated_or_invoiced_total) ? number_format($item->estimated_or_invoiced_total, 2, '.', '') : $item->estimated_or_invoiced_total  }}</td>

                                @foreach($suppliers as $supplier)

                                    <td>{{ \App\Models\ResponseItems::getTotal($item->id, $supplier->id)}}</td>

                                @endforeach

                            </tr>

                        @endforeach

                        <tr>
                            <td>Total</td>
                            <td>{{ \App\Models\RequestItems::sumEstimated($items) }}</td>

                            @foreach($suppliers as $supplier)

                                <td>{{ \App\Models\ResponseItems::getSumOfTotal($item->request->id, $supplier->id) }}</td>

                            @endforeach

                        </tr>

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">

                    <h4 class="mz-1 header-title">Supplier Responses</h4>
                    <br>
                    <div class="datatable-buttons_wrapper datatable-buttons-response-supplier_wrapper"></div>
                        <table class="table  table-bordered datatable-buttons-response-supplier">

                        <thead class="thead-dark">
                        <tr>
                            <th width="150">Supplier</th>
                            <th >Message</th>
                            <th width="250">Files</th>
                            <th width="100">View Response</th>
                        </tr>

                        </thead>

                        <tbody>

                        @foreach($suppliers as $supplier)

                            @if($supplier->isResponded($request->id) || $supplier->isDeclined($request->id))
                                <tr>
                                    <td>{{ isset($supplier->profile->business_name) ? $supplier->profile->business_name : '' }}</td>

                                    <td>


                                        @if($supplier->isDeclined($request->id))

                                            <span class="help-block">{{ ucfirst(\App\Models\SuppliersRequests::STATUS_DECLINED) }}</span>

                                        @else
                                            @foreach($supplier->response->where('request_id', $item->request_id) as $response)
                                                {!!nl2br(str_replace(" ", " &nbsp;", $response->note))!!}
                                            @endforeach
                                        @endif


                                    </td>

                                    <td>
                                        <ul>
                                            @foreach($supplier->supplierAttachment->where('request_id', $item->request_id) as $attachments)
                                                            
                                                <li>

                                                    <a  href="{{ route('download', ['attachmentId' => @$attachments->attachment->id,'requestId' => @$item->request_id]) }}">
                                                        {{ $attachments->attachment->name }}
                                                    </a>

                                                </li>

                                            @endforeach
                                        </ul>
                                    </td>

                                    <td width="50" class="single-icon-td">

                                        @if(!$supplier->isDeclined($request->id))
                                          
                                            <a href="{{ route('response.request', ['requestId' => $item->request_id, 'supplierId' => $supplier]) }}"
                                            >
                                                <button type="button" class="btn btn-primary btn-lg" 
                                                        data-toggle="tooltip"
                                                        data-placement="top" title="Show response">
                                                    <i class="fa fa-eye"></i>
                                                </button>
                                            </a>
                                        @endif

                                    </td>

                                    {{--<td class="single-icon-td">--}}

                                    {{--@if(\App\Models\AttachmentRequest::exist($supplier->id, $item->request_id) || $supplier->hasNote($item->request_id))--}}
                                    {{--<a href="{{ route('request.resend', ['requestId' => $item->request_id, 'supplierId' => $supplier]) }}"--}}
                                    {{-->--}}
                                    {{--<button type="button" class="btn btn-primary btn-lg"--}}
                                    {{--data-toggle="tooltip"--}}
                                    {{--data-placement="top" title="Resend request">--}}
                                    {{--<i class="fa fa-refresh"></i>--}}
                                    {{--</button>--}}
                                    {{--</a>--}}
                                    {{--@endif--}}
                                    {{--</td>--}}

                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>



@endsection
