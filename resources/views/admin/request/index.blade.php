@extends('layouts.admin')

@section('content')

    {{ Breadcrumbs::render('request') }}
 <style>
.datos{
  table-layout: fixed;
  height : 100px;
}
.modal-header{
   cursor: move;
}
.modal-body
{
    heightL
}
.close-icon {
  cursor: pointer;
}
#draggable1{
    display:none;
    width:200px;
    height:250px;
}
#draggable2{
    display:none;
    width:200px;
    height:200px;
}
.draggable3{
    display:none;
    width:400px;
    height:310px;
}
.p{
    text-align:center;
}
.card-blockquote {
    padding: 0 22px;
    margin: 25px 0;
    border-left: medium none;
    text-align: center;
}
.overlay {
  position: absolute;
  text-align: center;
  top:30%;
  left:40%;
  height: 80%;
  width: 100%;
  opacity: 1.0;
  z-index: 9999;
  color: white;
  background-color: grey;
}

</style>
    @if(session('status') && session('status') == \App\Models\RequestItems::VIEW_STATUS_SANDED)
        <div class="card m-b-30">
            <div class="card-body">
                <div class="alert alert-success" role="alert">
                    Your Request has been sent to the selected Suppliers!
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-12">
        <div class="card card-outline-danger text-center draggable3 card w-25 overlay">
<h5 class="card-header primary-color white-text">User Guide<span class="pull-right clickable close-icon" data-effect="fadeOut"><i class="fa fa-times"></i></span></h5> 

  <div class="card-block" >
    <blockquote class="card-blockquote">
      <p class="msg">
     
    </p>
    </blockquote>
  </div>
</div>
            <div class="card m-b-30">
                <div class="card-body">

                    <h4 class="mt-0 header-title">Requests</h4>
                    <div class="datatable-buttons_wrapper"></div>

                    <table class="table table-bordered datatable-buttons">
                        <thead class="thead-dark">

                        <tr>
                            <th>Request Number</th>
                            <th>Reference Name</th>
                            <th width="100">Request Created</th>
                            <th width="100">Request Sent</th>
                            <th width="100">Number of suppliers sent request</th>
                            <th width="100">Number of responses received</th>
                            <th width="120">Manage Request</th>
                        </tr>
                  
                        </thead>
                        <tbody>
                    
                       <?php $class = ''; ?>
                        @foreach($requests as $key => $request)
                        <?php $class[$request->id] = $request->status; ?>
                            <tr>
                                <td width="40"> {{ $key+1 }} </td>
                                <td> {{ $request->reference }} </td>
                                <td> {{ $request->created_at->format('d/m/Y') }}</td>
                                <td>

                                    @if($request->status == \App\Models\Requests::STATUS_NEW)
                                                                                
                                        {{ $request->updated_at->format('d/m/Y') }}
                                    
                                    @endif 
                                </td>
                                <td> {{ $request->suppliers->count()}}</td>
                                <td> {{ $request->countResponses()}}</td>
                              
                                <td class="single-icon-td">
                                    <a href="{{ route('request.items', ['requestId' => $request->id]) }}">
                                        <button 
                                                type="button"
                                                class="btn btn-success btn-lg"
                                                data-toggle="tooltip"
                                                data-placement="top"
                                                title="View Request and Supplier Responses">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                    </a>
            
                                    <a href="#">
                                        <button
                                                type="button"
                                                class="btn note btn-warning btn-lg"
                                                data-val = <?php echo $request->status; ?>
                                                data-toggle="tooltip"
                                                data-placement="top"
                                                title="User Guide">
                                            <i class="fa fa-question"></i>
                                        </button>
                                    </a>
                                    @if(!Auth::user()->isAdmin() && $request->isSaved())

                                        <a href="{{ route('request.edit', ['requestId' => $request->id]) }}">
                                            <button type="button" class="btn btn-warning btn-lg" data-toggle="tooltip"
                                                    data-placement="top" title="Edit request">
                                                <i class="	fa fa-pencil"></i>
                                            </button>
                                        </a>

                                        @endif

                                        @if(Auth::user()->isUser())

                                        <a href="{{ route('request.forward', ['requestId' => $request->id]) }}">
                                            <button
                                                    type="button" 
                                                    class="btn pop btn-warning btn-lg"
                                                    data-toggle="tooltip"
                                                    data-placement="top"
                                                    <?php if($request->status == \App\Models\Requests::STATUS_NEW) {
                                                    ?>  
                                                    title="Choose additional Suppliers and re-send Request">
                                                       
                                                    <?php } else { ?>
                                                        title="Confirm Suppliers and Send Request">
                                                    <?php } ?>
                                                <i class="fa fa-paper-plane"></i>
                                            </button>
                                        </a>
                                        @endif

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                  
                    @if(!Auth::user()->isAdmin())
                        <div>
                            <a href="{{ route('request.new') }}" class="padding-l-15">
                                <input type="button" value="Create Request" class="btn btn-success btn-lg">
                            </a>
                        </div>

                    @endif
                 
            </div>
            </div>
        </div>
    </div>

<div class="card card-outline-danger text-center" id="draggable1" class="card w-25" >
<span class="pull-right clickable close-icon" data-effect="fadeOut"><i class="fa fa-times"></i></span>
<h5 class="card-header primary-color white-text">User Guide</h5>  
<div class="card-block" >
    <blockquote class="card-blockquote">
      <p>Confirm Suppliers and Send Request</p>
    </blockquote>
  </div>
</div>
<div class="card card-outline-danger text-center" id="draggable2" class="card w-25">
<span class="pull-right clickable close-icon" data-effect="fadeOut"><i class="fa fa-times"></i></span>
<h5 class="card-header primary-color white-text">User Guide</h5>   
<div class="card-block" >
    <blockquote class="card-blockquote">
      <p>Choose additional Suppliers and re-send Request</p>
    </blockquote>
  </div>
</div>

       
<script>
$('#draggable1').draggable();
$('#draggable2').draggable();
$('.draggable3').draggable();
</script>
<script>
$('.note').hover(function()
    {  
        var test = $(this).attr("data-val");
        if(test == 'new')
        {  
            $('.msg').html("If you deselected Suppliers when you sent your original Request you can add them now and send your Request again. Click on the arrow icon and on the next page select 'Choose suppliers'. On the next page select the Suppliers you previously deselected and deselect the others, then select 'Send Request'.");
        }else
        {  
            $('.msg').html("Once you have uploaded your data, to send your Request click on the arrow icon and on the next page select 'Choose suppliers'. On the next page deselect any Suppliers you don't want included, then select 'Send Request'."); 
        }
        $('.draggable3').show();
    });
</script>
<script>
$('.close-icon').on('click',function() {
  $(this).closest('.card').fadeOut();
})
</script>

@endsection
