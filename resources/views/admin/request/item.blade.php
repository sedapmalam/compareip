@extends('layouts.admin')

@section('content')

    @if(\Illuminate\Support\Facades\Auth::user()->isAdmin())
        {{ Breadcrumbs::render('request-item-response-admin', $item->request) }}
    @else
        {{ Breadcrumbs::render('request-item-response', $item->request) }}
    @endif


    <div class="row">

        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">

                    <h4 class="mt-0 header-title">Items</h4>
                    <div class="datatable-buttons_wrapper"></div>
                    <table class="table table-bordered datatable-buttons">

                        <thead class="thead-dark">
                        <tr>
                            <th>IP</th>
                            <th>Item #</th>
                            <th>Country</th>
                            <th>Type</th>
                            <th>Status</th>
                            <th>Size</th>
                            {{--<th>Invoice amount</th>--}}
                            <th># Claims/Designs/Classes</th>
                            <th>Renewal Date</th>
                            <th>Annuity Year</th>
                            <th>Currency</th>
                            <th>Estimated Total</th>
                            <th></th>

                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td>{{ $item->ip_number }}</td>
                            <td>{{ $item->item_number_code }} </td>
                            <td>{{ $item->country }}</td>
                            <td>{{ $item->case_type }}</td>
                            <td>{{ $item->status }}</td>
                            <td>{{ $item->entity_size }}</td>
                            {{--<td>{{ $item->invoice_amount }}</td>--}}
                            <td>{{ $item->number_of_claims }}</td>
                            <td>{{ $item->next_renewal_date }}</td>
                            <td>{{ $item->next_annuity_year }}</td>
                            <td>{{ $item->invoice_currency }}</td>

                            <td>{{ $item->estimated_or_invoiced_total }}</td>
                            <td>
                                @if($item->additional_field_name_2 || $item->additional_field_value_2)
                                    {{ $item->additional_field_name_2}} / {{$item->additional_field_value_2 }}
                                @endif
                            </td>

                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>

        </div>
    </div>


    <div class="row">

        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">

                    <h4 class="mt-0 header-title">Responses</h4>

                    <table class="table table-bordered datatable-buttons response-grid">
                        <thead class="thead-dark">
                        <tr>

                            @foreach($responses as $response)
                                <th> {{ $response->supplier->profile->business_name }}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        <tr>

                            @foreach($responses as $response)

                                @if($responses->first()->id == $response->id)

                                    <td>
                                        <small>Official Fee in Local Currency</small>
                                        <p>{{ $response->official_fee_currency }}</p></td>

                                @else

                                    <td> {{ $response->official_fee_currency }}</td>

                                @endif

                            @endforeach
                        </tr>
                        <tr>
                            @foreach($responses as $response)

                                @if($responses->first()->id == $response->id)

                                    <td>
                                        <small>Official Fee in Invoice Currency</small>
                                        <p>{{ $response->official_fee_in_invoice }}</p></td>

                                @else

                                    <td>{{ $response->official_fee_in_invoice }}</td>

                                @endif

                            @endforeach
                        </tr>
                        <tr>

                            @foreach($responses as $response)


                                @if($responses->first()->id == $response->id)

                                    <td>
                                        <small>Local Costs in Local Currency</small>
                                        <p>{{ $response->official_fee_amount }}</p></td>

                                @else

                                    <td>  {{ $response->official_fee_amount }}</td>

                                @endif


                            @endforeach
                        </tr>
                        <tr>

                            @foreach($responses as $response)

                                @if($responses->first()->id == $response->id)

                                    <td>
                                        <small>Local Costs in Invoice Currency</small>
                                        <p>{{ $response->locale_cost_in_invoice }}</p></td>

                                @else

                                    <td>  {{ $response->locale_cost_in_invoice }}</td>

                                @endif


                            @endforeach
                        </tr>
                        <tr>

                            @foreach($responses as $response)

                                @if($responses->first()->id == $response->id)

                                    <td>
                                        <small>Service Fee in Invoice Currency</small>
                                        <p>{{ $response->service_fee_in_invoice }}</p></td>

                                @else

                                    <td>  {{ $response->service_fee_in_invoice }}</td>

                                @endif


                            @endforeach
                        </tr>
                        <tr>

                            @foreach($responses as $response)

                                @if($responses->first()->id == $response->id)

                                    <td>
                                        <small>Cost in Invoice Currency</small>
                                        <p>{{ $response->total_charge_in_invoice }}</p></td>

                                @else

                                    <td>  {{ $response->total_charge_in_invoice }}</td>

                                @endif

                            @endforeach
                        </tr>
                        <tr>

                            @foreach($responses as $response)


                                @if($responses->first()->id == $response->id)

                                    <td>
                                        <small>FX Currency Conversion Date</small>
                                        <p>{{ $response->local_cost_amount->format('d/m/Y') }}</p></td>

                                @else

                                    <td>  {{ $response->local_cost_amount->format('d/m/Y') }}</td>

                                @endif

                            @endforeach
                        </tr>
                        <tr>

                            @foreach($responses as $response)


                                @if($responses->first()->id == $response->id)

                                    <td>
                                        <small>FX Markup on Interbank Rate</small>
                                        <p>{{ $response->local_cost_currency }}</p></td>

                                @else

                                    <td>  {{ $response->local_cost_currency }}</td>

                                @endif

                            @endforeach
                        </tr>
                        <tr>

                            @foreach($responses as $response)


                                @if($responses->first()->id == $response->id)

                                    <td>
                                        <small>Comments</small>
                                        <p>{{ $response->comments }}</p></td>

                                @else

                                    <td>  {{ $response->comments }}</td>

                                @endif

                            @endforeach

                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>

        </div>
    </div>

@endsection
