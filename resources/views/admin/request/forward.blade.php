@extends('layouts.admin')

@section('content')

    @if(!$errors->isEmpty())
        <div class="card m-b-30">
            <div class="card-body">
                <div class="alert alert-danger" role="alert">
                    Please check the highlighted  fields
                </div>
            </div>
        </div>
    @endif

    <div class="card m-b-30">
        <div class="card-body">
            <div class="float-right m-r-15 download-pattern-container">

                <a href="{{ route('download.pattern') }}">
                    <button type="button" class="btn btn-primary btn-lg download-button m-r-15"
                            onclick="$(window).off('beforeunload'); ">
                        Download Template
                    </button>
                </a>

            </div>

            <div class="float-right m-r-15 upload-pattern-container">

                <form method="POST" action="{{ route('form.upload') }}" enctype="multipart/form-data"
                      class="upload-excel"
                      id="upload-excel">

                    <input type="hidden" id="upload-url" data-upload-url="{{ route('form.upload') }}">

                    <input type="file" class="form-input hide" name="form" id="form-input"
                           title="Upload Form"
                    >

                    <input type="button" class=" btn-primary upload-form m-r-10 m-l-10" name="form"
                           onclick="document.getElementById('form-input').click()"
                           value="Upload Form"
                    >

                </form>

            </div>

            <div class="float-right m-r-10 reference-container">

                <div class="block m-b-10 ">

                    <input class="block  " type="text" placeholder="Reference Name" id="reference"
                           value="{{ old('reference')  !== null ? old('reference')  : $request->reference}}">

                    @if ($errors->has('reference'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('reference') }}</strong>
                                    </span>
                    @endif

                </div>

            </div>

            {{ Breadcrumbs::render('request-forward') }}


            <div class="container-fluid new-request-container">

                <form method="Post" action="{{ route('request.create') }}" id="request-items-form"
                      class="wrapper-create-request">

                    {{ csrf_field() }}

                    @if(isset($requestId))

                        <input type="hidden" name="requestId" value="{{ $requestId }}">

                    @endif

                    <div class="table-container">


                        <table class="table table-bordered">
                            <thead class="thead-dark">
                            <tr>
                                <th>{{ \App\Models\RequestItems::COLUMN_NAME_ITEM_NUMBER_CODE }}</th>
                                <th class="blue-th">{{ \App\Models\RequestItems::COLUMN_NAME_IP_NUMBER }}&nbsp;
                                    <i class="fa fa-info-circle pointer tooltips"

                                       data-placement="top" data-toggle="tooltip"
                                       data-original-title="Not disclosed to suppliers"

                                    ></i>
                                </th>
                                <th>{{ \App\Models\RequestItems::COLUMN_NAME_NEXT_RENEWAL_DATE }}</th>
                                <th>{{ \App\Models\RequestItems::COLUMN_NAME_COUNTRY }}</th>
                                <th>{{ \App\Models\RequestItems::COLUMN_NAME_STATUS }}</th>
                                <th>{{ \App\Models\RequestItems::COLUMN_NAME_CASE_TYPE }}</th>
                                <th>{{ \App\Models\RequestItems::COLUMN_NAME_ENTITY_SIZE }}</th>
                                <th>{{ \App\Models\RequestItems::COLUMN_NAME_NEXT_ANNUITY_YEAR }}</th>
                                <th class="space">{{ \App\Models\RequestItems::COLUMN_NAME_NUMBER_OF_CLAIMS }}</th>


                                <th>{{ \App\Models\RequestItems::COLUMN_NAME_INVOICE_CURRENCY }}</th>
                                <th class="blue-th">{{ \App\Models\RequestItems::COLUMN_NAME_ESTIMATED_OR_INVOICED_TOTAL }}
                                    &nbsp;
                                    <i class="fa fa-info-circle pointer tooltips"

                                       data-placement="top" data-toggle="tooltip"
                                       data-original-title="Not disclosed to suppliers"

                                    ></i>
                                </th>
                                <th>Delete Item</th>

                            </tr>
                            </thead>
                            <tbody id="request">


                            @if(!empty(old('items', [])))
                                @foreach(old('items', []) as $index => $item)
                                    @include('admin.request._item', ['index' => $index])
                                @endforeach
                            @elseif(isset($items))
                                @foreach($items as $item)
                                    @include('admin.request._item-resend', ['index' => $item->id, 'item' => $item])
                                @endforeach
                            @endif

                            </tbody>
                        </table>


                    </div>

                    <div class="suppliers-container">
                        <div class="row">

                            @foreach($suppliers as $supplier)

                                <div class="col-lg-6 col-12 col-sm-12 col-md-12 col-xl-6 col-xs-12 supplier-card-container">
                                    <div class="card m-b-30 supplier-card

                                    @if(in_array($supplier->id, old('suppliers', [])) || old('suppliers') === null))
                            {{ 'add-checked-border' }}
                                    @endif
                                            ">
                                        <div class="card-body">

                                            <input type="checkbox"
                                                   name="suppliers[]"
                                                   value="{{ $supplier->id }}"
                                                   class="supplier hide"
                                                   @if(in_array($supplier->id, old('suppliers', [])) || old('suppliers') === null))
                                                    {{ 'checked' }}
                                                    @endif

                                            />

                                            <div class="media">
                                                <img class="d-flex mr-3 img-thumbnail thumb-lg thumb-orig"
                                                     src="{{ $supplier->getAvatarPath() }}"
                                                     alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <h5 class="mt-0 font-18 mb-1">{{ $supplier->profile->business_name }}</h5>
                                                    <p class="text-muted font-14" style="overflow : hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical;">{{ $supplier->profile->description }}</p>

                                                    @if(isset($supplier->profile->site) && $supplier->profile->site !== '')

                                                        <a title="Read More"
                                                           target="_blank"
                                                           data-placement="top" data-toggle="tooltip"
                                                           class="tooltips"
                                                           href="{{ route('supplier.info', ['profileId' => $supplier->profile->id]) }}"
                                                           data-original-title="Read More">Read More</a>

                                                    @endif

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div> <!-- end col -->

                            @endforeach

                        </div>
                    </div>

                    <div class="add-item-container col-12 m-t-10">
                        <div>
                            <a href="#" class="add-item">Add Item</a>
                        </div>

                    </div>

                    <input type="hidden" id="reference-form" name="reference">

                    <div class="item-complete-container col-12 m-t-10">

                        <button type="button" class="btn btn-success btn-lg choose-suppliers button-float-right">
                            Choose suppliers
                        </button>

                        <input type="submit" name="action"
                               class="btn btn-primary btn-lg m-r-15 button-float-right save-request"
                               value="{{ \App\Models\RequestItems::FORM_OPTION_SAVE }}"/>


                    </div>


                    <div class="supplier-complete-container col-12 m-t-10">

                        <button type="button" class="btn btn-info btn-lg back-to-items">
                            Back
                        </button>

                        <button type="button" class="btn btn-success btn-lg send-request">
                            {{ \App\Models\RequestItems::FORM_OPTION_SEND_REQUEST }}
                        </button>

                    </div>

                </form>


            </div><!-- container -->
        </div>
    </div>
    <script id="item-template" type="text/html">

        @include('admin.request._item', ['index' => '{index}'])

    </script>


@endsection
