@extends('layouts.admin')

@section('content')

    @if(\Illuminate\Support\Facades\Auth::user()->isAdmin())
        {{ Breadcrumbs::render('requests-request-resend-admin', $request) }}
    @else
        {{ Breadcrumbs::render('requests-request-resend', $request) }}
    @endif

    <div class="container-fluid new-request-container">

        <form method="Post" action="{{ route('request.create') }}" id="request-items-form"
              class="wrapper-create-request">

            {{ csrf_field() }}

            <input type="hidden" value="{{ $supplierId }}" name="suppliers[]">

            <div id="request" class="row">


                @if(!empty(old('items', [])))
                    @foreach(old('items', []) as $index => $item)
                        @include('admin.request._item', ['index' => $index])
                    @endforeach
                @else
                    @foreach($items as $item)
                        @include('admin.request._item-resend', ['index' => $item->id, 'item' => $item])
                    @endforeach
                @endif

            </div>


            <div class="add-item-container col-12 m-t-10">

                <a href="#" class="add-item">Add Item Request</a>

            </div>


            <button type="submit" class="btn btn-success btn-lg button-float-right">
                Send Request
            </button>

        </form>


    </div><!-- container -->

    <script id="item-template" type="text/html">

        @include('admin.request._item', ['index' => '{index}'])

    </script>

@endsection
