<?php
$prefix = 'items.' . $index . '.';
?>



<tr class="request-item">

    <input name="items[{{ $index }}][id]" value="{{  $item->id }}" type="hidden">

    <td width="20">

        <input name="items[{{ $index }}][item_number_code]"
               value="{{  old($prefix . 'item_number_code') !== null ? old($prefix . 'item_number_code') : $item->item_number_code }}"

               readonly="readonly"

               class="form-control item_number_code

                    @if ($errors->has($prefix . 'item_number_code'))
               {{ 'error' }}
               @endif
                       "
               type="text"

               @if ($errors->has($prefix . 'item_number_code'))
               data-toggle="tooltip"
               data-placement="top"
               title="{{ $errors->first($prefix . 'item_number_code') }}"
                @endif


        >

    </td>

    <td class="character-20">

        <input name="items[{{ $index }}][ip]"
               value="{{ old($prefix . 'ip') !== null ? old($prefix . 'ip') : $item->ip_number  }}"

               class="form-control blue

                    @if ($errors->has($prefix . 'ip'))
               {{ 'error' }}
               @endif
                       "
               type="text"

               @if ($errors->has($prefix . 'ip'))
               data-toggle="tooltip"
               data-placement="top"
               title="{{ $errors->first($prefix . 'ip') }}"
                @endif

        >

    </td>

    <td class="character-10">


        <input name="items[{{ $index }}][next_renewal_date]"
               value="{{ old($prefix . 'next_renewal_date') !== null ? old($prefix . 'next_renewal_date') : $item->next_renewal_date }}"
               class="form-control datepicker selectable

                    @if ($errors->has($prefix . 'next_renewal_date'))
               {{ 'error' }}
               @endif
                       "
               type="text"

               @if ($errors->has($prefix . 'next_renewal_date'))
               data-toggle="tooltip"
               data-placement="top"
               title="{{ $errors->first($prefix . 'next_renewal_date') }}"
                @endif

        >

    </td>

    <td class="character-30">

        <input name="items[{{ $index }}][country]"
               value="{{ old($prefix . 'country') !== null ? old($prefix . 'country') : $item->country }}"
               type="text"
               class="form-control

                    @if ($errors->has($prefix . 'country'))
               {{ 'error' }}
               @endif
                       "

               @if ($errors->has($prefix . 'country'))
               data-toggle="tooltip"
               data-placement="top"
               title="{{ $errors->first($prefix . 'country') }}"
                @endif
        >

    </td>

    <td class="character-12">

        <select name="items[{{ $index }}][status]"
                class="form-control selectable

                    @if ($errors->has($prefix . 'status'))
                {{ 'error' }}
                @endif
                        "

                @if ($errors->has($prefix . 'status'))
                data-toggle="tooltip"
                data-placement="top"
                title="{{ $errors->first($prefix . 'status') }}"
                @endif

        >

            <option value="" selected="selected"></option>

            <option value="{{ \App\Models\RequestItems::STATUS_GRANTED }}"

            @if(old($prefix . 'status') !== null ? old($prefix . 'status') : $item->status == \App\Models\RequestItems::STATUS_GRANTED)
                {{ 'selected' }}
                    @endif

            >{{ \App\Models\RequestItems::STATUS_GRANTED_VIEW }}
            </option>

            <option value="{{ \App\Models\RequestItems::STATUS_APP_SELECT }}"

            @if(old($prefix . 'status') !== null ? old($prefix . 'status') : $item->status == \App\Models\RequestItems::STATUS_APP_SELECT)
                {{ 'selected' }}
                    @endif
            >{{ \App\Models\RequestItems::STATUS_APP_SELECT_VIEW }}
            </option>

        </select>

    </td>

    <td class="character-15">

        <input name="items[{{ $index }}][type]"
               value="{{ old($prefix . 'type') !== null ? old($prefix . 'country') : $item->case_type }}"
               type="text"
               class="form-control

                    @if ($errors->has($prefix . 'type'))
               {{ 'error' }}
               @endif
                       "

               @if ($errors->has($prefix . 'type'))
               data-toggle="tooltip"
               data-placement="top"
               title="{{ $errors->first($prefix . 'type') }}"
                @endif
        >


    </td>

    <td class="character-10">

        <select name="items[{{ $index }}][entity_size]"
                class="form-control entity_size selectable

                    @if ($errors->has($prefix . 'entity_size'))
                {{ 'error' }}
                @endif
                        "

                @if ($errors->has($prefix . 'entity_size'))
                data-toggle="tooltip"
                data-placement="top"
                title="{{ $errors->first($prefix . 'entity_size') }}"
                @endif

        >

            <option value="" selected="selected"></option>

            <option value="{{ \App\Models\RequestItems::SIZE_LARGE }}"

            @if(old($prefix . 'entity_size') !== null ? old($prefix . 'entity_size') : $item->entity_size == \App\Models\RequestItems::SIZE_LARGE)
                {{ 'selected' }}
                    @endif

            >{{ \App\Models\RequestItems::SIZE_LARGE }}
            </option>

            <option value="{{ \App\Models\RequestItems::SIZE_SMALL }}"

            @if(old($prefix . 'entity_size') !== null ? old($prefix . 'entity_size') : $item->entity_size == \App\Models\RequestItems::SIZE_SMALL)
                {{ 'selected' }}
                    @endif
            >{{ \App\Models\RequestItems::SIZE_SMALL }}
            </option>

            <option value="{{ \App\Models\RequestItems::SIZE_MICRO }}"

            @if(old($prefix . 'entity_size') !== null ? old($prefix . 'entity_size') : $item->entity_size == \App\Models\RequestItems::SIZE_MICRO)
                {{ 'selected' }}
                    @endif
            >{{ \App\Models\RequestItems::SIZE_MICRO }}
            </option>

        </select>

    </td>

    <td class="character-4">

        <input name="items[{{ $index }}][next_annuity_year]"
               value="{{ old($prefix . 'next_annuity_year') !== null ? old($prefix . 'next_annuity_year') : $item->next_annuity_year}}"
               class="form-control

                    @if ($errors->has($prefix . 'next_annuity_year'))
               {{ 'error' }}
               @endif
                       "
               type="text"

               @if ($errors->has($prefix . 'next_annuity_year'))
               data-toggle="tooltip"
               data-placement="top"
               title="{{ $errors->first($prefix . 'next_annuity_year') }}"
                @endif

        >

    </td>

    <td class="character-10">

        <input name="items[{{ $index }}][number]"
               value="{{ old($prefix . 'number') !== null ? old($prefix . 'number') : $item->number_of_claims }}"
               type="text"
               class="form-control

                    @if ($errors->has($prefix . 'number'))
               {{ 'error' }}
               @endif
                       "

               @if ($errors->has($prefix . 'number'))
               data-toggle="tooltip"
               data-placement="top"
               title="{{ $errors->first($prefix . 'number') }}"
                @endif

        >


    </td>


    <td class="character-4">

        <input name="items[{{ $index }}][invoice_currency]"
               value="{{ old($prefix . 'invoice_currency')  !== null ? old($prefix . 'invoice_currency') : $item->invoice_currency }}"
               class="form-control

                    @if ($errors->has($prefix . 'invoice_currency'))
               {{ 'error' }}
               @endif
                       "
               type="text"

               @if ($errors->has($prefix . 'invoice_currency'))
               data-toggle="tooltip"
               data-placement="top"
               title="{{ $errors->first($prefix . 'invoice_currency') }}"
                @endif

        >


    </td>

    <td class="character-10">

        <input name="items[{{ $index }}][estimated_or_invoiced_total]"
               value="{{ old($prefix . 'estimated_or_invoiced_total') !== null ? old($prefix . 'estimated_or_invoiced_total') : $item->estimated_or_invoiced_total}}"
               type="text"
               class="form-control blue

                    @if ($errors->has($prefix . 'estimated_or_invoiced_total'))
               {{ 'error' }}
               @endif
                       "

               @if ($errors->has($prefix . 'estimated_or_invoiced_total'))
               data-toggle="tooltip"
               data-placement="top"
               title="{{ $errors->first($prefix . 'estimated_or_invoiced_total') }}"
                @endif

        >

    </td>

    <td class="character-5" style="text-align: center">

        <div class="remove-container">
            <i class="fa fa-remove remove-item"></i>
        </div>

    </td>


</tr>
