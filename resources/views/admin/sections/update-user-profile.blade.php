<div class="form-group row profile-container-inputs">
    <div class="col-12">

        <div class="row">

            <div class="col-3">
                <label for="organisation" class="form-inline"> Organisation </label>
            </div>
            <div class="col-9">
                <input id="organisation"
                       type="text"
                       class="form-control"
                       name="organisation"
                       value="{{ old('organisation') !== null ? old('organisation') : $user->profile->company }}"
                       required
                >

                @if ($errors->has('organisation'))
                    <span class="help-block">
    <strong>{{ $errors->first('organisation') }}</strong>
    </span>
                @endif
            </div>
        </div>


    </div>
</div>

<div class="form-group row profile-container-inputs">
    <div class="col-12">

        <div class="row">

            <div class="col-3">
                <label for="position" class="form-inline"> Position </label>
            </div>
            <div class="col-9">
                <input id="position" type="text" class="form-control" name="position"
                       value="{{ old('position') !== null ? old('position') : $user->profile->position }}"
                       >

                @if ($errors->has('position'))
                    <span class="help-block">
    <strong>{{ $errors->first('position') }}</strong>
    </span>
                @endif
            </div>
        </div>


    </div>
</div>

<div class="form-group row profile-container-inputs">
    <div class="col-12">

        <div class="row">

            <div class="col-3">
                <label for="phone" class="form-inline"> Phone </label>
            </div>
            <div class="col-9">
                <input id="phone" type="text" class="form-control" name="phone"
                       value="{{ old('phone') !== null ? old('phone') : $user->profile->phone }}"
                       >

                @if ($errors->has('phone'))
                    <span class="help-block">
    <strong>{{ $errors->first('phone') }}</strong>
    </span>
                @endif
            </div>
        </div>


    </div>
</div>

<div class="form-group row profile-container-inputs">
    <div class="col-12">

        <div class="row">

            <div class="col-3">
                <label for="number_of_registered_ip" class="form-inline"> Number of registered IP rights </label>
            </div>
            <div class="col-9">
                <input id="number_of_registered_ip" type="text" class="form-control"
                       name="number_of_registered_ip"
                       value="{{ old('number_of_registered_ip') !== null ? old('number_of_registered_ip') : $user->profile->number_of_registered_ip }}"
                       >

                @if ($errors->has('number_of_registered_ip'))
                    <span class="help-block">
    <strong>{{ $errors->first('number_of_registered_ip') }}</strong>
    </span>
                @endif
            </div>
        </div>


    </div>
</div>

<div class="form-group row profile-container-inputs">
    <div class="col-12">

        <div class="row">

            <div class="col-3">
                <label for="patents" class="form-inline"> Patents </label>
            </div>
            <div class="col-9">
                <input id="patents" type="text" class="form-control" name="patents"
                       value="{{ old('patents') !== null ? old('patents') : $user->profile->patents }}"
                       >

                @if ($errors->has('patents'))
                    <span class="help-block">
    <strong>{{ $errors->first('patents') }}</strong>
    </span>
                @endif
            </div>
        </div>


    </div>
</div>

<div class="form-group row profile-container-inputs">
    <div class="col-12">

        <div class="row">

            <div class="col-3">
                <label for="models" class="form-inline"> Utility Models (and similar) </label>
            </div>
            <div class="col-9">
                <input id="models" type="text" class="form-control" name="models"
                       value="{{ old('models') !== null ? old('models') : $user->profile->models }}"
                       >

                @if ($errors->has('models'))
                    <span class="help-block">
    <strong>{{ $errors->first('models') }}</strong>
    </span>
                @endif
            </div>
        </div>


    </div>
</div>

<div class="form-group row profile-container-inputs">
    <div class="col-12">

        <div class="row">

            <div class="col-3">
                <label for="industrial_designs" class="form-inline"> Industrial Designs </label>
            </div>
            <div class="col-9">
                <input id="industrial_designs" type="text" class="form-control"
                       name="industrial_designs"
                       value="{{ old('industrial_designs') !== null ? old('industrial_designs') : $user->profile->industrial_designs }}"
                       >

                @if ($errors->has('industrial_designs'))
                    <span class="help-block">
    <strong>{{ $errors->first('industrial_designs') }}</strong>
    </span>
                @endif
            </div>
        </div>


    </div>
</div>

<div class="form-group row profile-container-inputs">
    <div class="col-12">

        <div class="row">

            <div class="col-3">
                <label for="trademarks" class="form-inline"> Trademarks </label>
            </div>
            <div class="col-9">
                <input id="trademarks" type="text" class="form-control" name="trademarks"
                       value="{{ old('trademarks') !== null ? old('trademarks') : $user->profile->trademarks }}"
                       >

                @if ($errors->has('trademarks'))
                    <span class="help-block">
    <strong>{{ $errors->first('trademarks') }}</strong>
    </span>
                @endif
            </div>
        </div>


    </div>
</div>
