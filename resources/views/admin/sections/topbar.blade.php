<div class="topbar">

    <nav class="navbar-custom">

        <ul class="list-inline float-right mb-0">


            <li class="list-inline-item dropdown notification-list">

                <div class="user-name-container">

                    @if(\Illuminate\Support\Facades\Auth::user()->isSupplier())
                        <p class="font-16"> {{ \Illuminate\Support\Facades\Auth::user()->profile->business_name }}</p>
                    @else(\Illuminate\Support\Facades\Auth::user()->isSupplier())
                        <p class="font-16"> {{ \Illuminate\Support\Facades\Auth::user()->first_name }}</p>
                    @endif

                </div>

                <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#"
                   role="button"
                   aria-haspopup="false" aria-expanded="false">
                    <img src={{ \Illuminate\Support\Facades\Auth::user()->getAvatarPath() }} alt="user" class="rounded-circle">
                </a>

                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">

                    @if(\Illuminate\Support\Facades\Auth::user()->isUserOrSupplier())
                        <a class="dropdown-item" href="{{ route('profile.index') }}"><i class="mdi mdi-account-circle m-r-5 text-muted"></i>
                            Profile</a>
                    @endif

                        <a class="dropdown-item" href="{{ route('admin.users.password', ['userId' => \Illuminate\Support\Facades\Auth::user()->id]) }}"><i class="mdi mdi-account-outline m-r-5 text-muted"></i>
                            Change Password</a>

                    {{--<a class="dropdown-item" href="#"><span class="badge badge-success pull-right">5</span><i class="mdi mdi-settings m-r-5 text-muted"></i> Settings</a>--}}
                    {{--<a class="dropdown-item" href="#"><i class="mdi mdi-lock-open-outline m-r-5 text-muted"></i> Lock screen</a>--}}
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();"
                    >

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>

                        <i class="mdi mdi-logout m-r-5 text-muted"></i> Logout</a>
                </div>
            </li>

        </ul>

        <ul class="list-inline menu-left mb-0">
            <li class="list-inline-item">
                <button type="button" class="button-menu-mobile open-left waves-effect">
                    <i class="ion-navicon"></i>
                </button>
            </li>
            <li class="hide-phone list-inline-item app-search">
                <h3 class="page-title" style="font-family: 'Calibri Light'">CompareIP</h3>
            </li>
        </ul>

        <div class="clearfix"></div>

    </nav>

</div>
