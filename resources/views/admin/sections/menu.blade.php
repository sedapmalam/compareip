<div class="left side-menu">
    <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
        <i class="ion-close"></i>
    </button>

    <!-- LOGO -->
    <div class="topbar-left">
        <div class="text-center">
            {{--<a href=" {{ route('dashboard') }}" class="logo"><img src={{ asset('/admin/images/logo.png') }} height="24"--}}
                                                                  {{--alt="logo"></a>--}}
        </div>
    </div>

    <div class="sidebar-inner slimscrollleft">

        <div class="user-details">
            <div class="text-center">
                <img src={{  \Illuminate\Support\Facades\Auth::user()->getAvatarPath() }} alt="" class="rounded-circle">
            </div>
            <div class="user-info">

                @if(\Illuminate\Support\Facades\Auth::user()->isSupplier())
                    <h4 class="font-16"> {{ \Illuminate\Support\Facades\Auth::user()->profile->business_name }}</h4>
                @else(\Illuminate\Support\Facades\Auth::user()->isSupplier())
                    <h4 class="font-16"> {{ \Illuminate\Support\Facades\Auth::user()->first_name }}</h4>
                @endif

            </div>
        </div>

        <div id="sidebar-menu">
            <ul>
                <li class="menu-title">Main</li>

                {{--<li>--}}
                {{--<a href="#" class="waves-effect">--}}
                {{--<i class="ti-home"></i>--}}
                {{--<span> Dashboard</span>--}}
                {{--</a>--}}
                {{--</li>--}}

                @if(Auth::user()->isSupplier())
                    <li>
                        <a href="{{ route('response.index') }}" class="waves-effect">
                            <i class="typcn typcn-plus-outline"></i>
                            <span>Requests</span>
                        </a>
                    </li>
                @endif

                @if(Auth::user()->isUser())
                    <li>
                        <a href="{{ route('request.index') }}" class="waves-effect">
                            <i class="typcn typcn-plus-outline"></i>
                            <span>Requests</span>
                        </a>
                    </li>
                @endif

                @if(Auth::user()->isAdmin())
                    <li>
                        <a href="{{ route('admin.users.index') }}" class="waves-effect">
                            <i class="typcn typcn-user-add-outline"></i>
                            <span>Users</span>
                        </a>
                    </li>
                @endif

            </ul>
        </div>
        <div class="clearfix"></div>
    </div> <!-- end sidebarinner -->
</div>

