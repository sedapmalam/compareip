@extends('layouts.landing')

@section('content')

    <!-- Page content area start -->
    <section class="content">
        <!-- template breadcrumb start -->
        <div class="template-section grd-1 breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <h3 class="breadcrumb-title">Contact</h3>
                    </div>
                    <div class="col-xs-6">
                        <div class="breadcrumb-wrap">
                            <ul class="breadcrumb-list">
                                <li><a href="{{ route('home') }}">Home </a></li>
                                <li><a href="{{ route('contact') }}">Contact </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- template contact section start -->
        <div class="template-section contact-section">
            <div class="container">

                <div class="row">
                    <div class="contact-form">
                        <form id="template-contact-form" action=" {{ route('lendingEmail') }}" method="POST"
                              name="Bizpro_message_form">

                            {{ csrf_field() }}

                            <div class="col-md-6">
                                <input type="text" class="form-control" id="Name" name="name" placeholder="your name"
                                       required>

                                @if ($errors->has('name'))
                                    <span class="help-block">
    <strong>{{ $errors->first('name') }}</strong>
    </span>
                                @endif

                                <input type="email" class="form-control" id="Email" name="email"
                                       placeholder="your email" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
    <strong>{{ $errors->first('email') }}</strong>
    </span>
                                @endif

                                <input type="text" class="form-control" id="Subject" name="subject"
                                       placeholder="subject" required>

                                @if ($errors->has('subject'))
                                    <span class="help-block">
    <strong>{{ $errors->first('subject') }}</strong>
    </span>
                                @endif

                                <p class="form-send-message"></p>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" id="Message" name="message"
                                              placeholder="your message"></textarea>
                                </div>
                                <button type="submit" class="button">send message</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- template contact section end -->

    </section>
    <!-- Page content area end -->

@endsection
