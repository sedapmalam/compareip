@extends('layouts.landing')

@section('content')

    <section class="content">
        <!-- breadcrumb start -->
        <div class="template-section grd-1 breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <h3 class="breadcrumb-title">Terms of Service</h3>
                    </div>
                    <div class="col-xs-6">
                        <div class="breadcrumb-wrap">
                            <ul class="breadcrumb-list">
                                <li>
                                    <a href="{{ route('home') }}">Home </a>
                                </li>
                                <li>
                                    <a href="{{ route('terms') }}">Terms of Service </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb end -->
        <!-- team section start -->
        <div class="template-section team-area-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <p>These Terms of Service, including any future modifications (collectively the &quot;Terms
                            of Service&quot; or &quot;Agreement&quot;) govern your use of the <strong>CompareIP</strong>
                            service and is a legal contract between you and us.</p>

                        <p>By registering with the <strong>CompareIP</strong> service as a User you acknowledge that
                            you have read, understood, and agree to be bound by this Agreement. If you do not agree
                            to future amendments to these Terms of Service, you can terminate your subscription and
                            stop using the <strong>CompareIP</strong> service.</p>

                        <h4>Definitions</h4>

                        <dl>
                            <dt>&prime;PACL&prime;</dt>
                            <dd>Patent Annuity Costs Limited, providers of the <strong>CompareIP</strong> service</dd>
                        </dl>
                        <dl>
                            <dt>&prime;CompareIP service&prime;</dt>
                            <dd>Online services provided via www.CompareIP.com</dd>
                        </dl>
                        <dl>
                            <dt>&prime;Suppliers&prime;</dt>
                            <dd>Providers of renewal payment services able to provide Quotes to Clients and Firms</dd>
                        </dl>
                        <dl>
                            <dt>&prime;Clients&prime;</dt>
                            <dd>IP owners who have registered with the <strong>CompareIP</strong> service in order
                                to obtain Quotes from Suppliers</dd>
                        </dl>
                        <dl>
                            <dt>&prime;Users&prime;</dt>
                            <dd>Clients and Suppliers who use the <strong>CompareIP</strong> service</dd>
                        </dl>
                        <dl>
                            <dt>&prime;Quote&prime;</dt>
                            <dd>Indicative pricing provided by a Supplier based on information provided</dd>
                        </dl>

                        <h4>The purpose, principal functions and permitted use of CompareIP</h4>
                        <p>The <strong>CompareIP</strong> service is a secure online venue through which IP owners
                            or their representatives may seek, receive and download quotations for renewal of their
                            registered IP rights.</p>

                        <h4>Principal Functions</h4>
                        <p>Clients upload IP renewals data; select Suppliers and send Quote requests; receive,
                            review and download Quotes received. Suppliers receive Quote requests and either
                            decline or respond to each request with a Quote.</p>

                        <h4>Permitted Use</h4>
                        <p>Rights and privileges of Clients and Suppliers are governed and administered by
                            the <strong>CompareIP</strong> service and by these Terms of Service. Users must
                            self-organise within the scope of the functionality afforded by the
                            <strong>CompareIP</strong> service and these Terms of Service.
                            The <strong>CompareIP</strong> service may only be used for purposes allowed by Law and
                            Users will be accountable to relevant National and International jurisdictions for the
                            content they publish and the actions they take, coordinate, encourage or allow using the
                            <strong>CompareIP</strong> service.</p>

                        <h4>Charges</h4>
                        <p>Clients who are IP owners may, at the discretion of PACL, use the <strong>CompareIP</strong>
                            service free of charge. Suppliers must pay an annual fee determined by PACL. Suppliers much
                            also pay a fee for each Quote request to which they respond on the basis of a schedule of
                            charges set by PACL. If Suppliers do not pay charges due then their use of the
                            <strong>CompareIP</strong> service may be terminated or suspended at the
                            discretion of PACL.</p>

                        <h4>The role and responsibilities of PACL in the provision of the CompareIP service</h4>
                        <p>PACL is responsible for ensuring the security and continuing provision of the
                            <strong>CompareIP</strong> service for the benefit of Users.</p>

                        <h6>PACL has the power to:</h6>
                        <ul class="list-style-type-disc">
                            <li>delete Users</li>
                            <li>control User rights and privileges</li>
                            <li>manage any subscription charges and payments by Users</li>
                            <li>amend and add to the functionality and appearance of the <strong>CompareIP</strong>
                                service</li>
                            <li>allow the creation and use of third party applications</li>
                        </ul>
                        <br>

                        <h6>The roles and responsibilities of Users</h6>
                        <p>Every prospective User must first register with the <strong>CompareIP</strong> service
                            providing a username and password which must be kept secure and confidential
                            to the User.</p>

                        <h4>Roles of Users</h4>

                        <h6>Responsibilities of Users</h6>
                        <p>All Users must self-organise and administer their use of the <strong>CompareIP</strong>
                            service according to these Terms of Service and standards of conduct prescribed by Law
                            and/or relevant professional regulations.</p>
                        <p>All Users are responsible for the content they publish and any infringement of the rights of
                            third parties in relation to such content.</p>
                        <p>All Users are responsible for the security of their usernames and passwords as well as the
                            security their chosen means and place of access to the
                            <strong>CompareIP</strong> service.</p>
                        <p>Clients must keep confidential and not disclose to third parties (including persons outside
                            their organization or, in the case of professional representatives, to anyone other than
                            the client on whose behalf a Quote was obtained) Quotes received from Suppliers or any
                            information contained in such Quotes related to fees and charges of Suppliers.</p>

                        <h6>Abuse of the CompareIP service and sanctions</h6>
                        You will be abusing the <strong>CompareIP</strong> service if you:
                        <ul class="list-style-type-disc">
                            <li>rent, lease, loan, or sell access to the <strong>CompareIP</strong> service;</li>
                            <li>decompile or reverse engineer or attempt to access the source code of the software
                                underlying the <strong>CompareIP</strong> service
                                (the &prime;PACL Technology&prime;);</li>
                            <li>copy, archive, store, reproduce, rearrange, modify, adapt, download, upload, create
                                derivate works from, display, perform, publish, distribute, redistribute or disseminate
                                any PACL Technology;</li>
                            <li>access the <strong>CompareIP</strong> service to build a product using similar ideas,
                                features, functions, interface or graphics of the <strong>CompareIP</strong>
                                service;</li>
                            <li>access (or attempt to access) any service on the <strong>CompareIP</strong> service by
                                any means other than as permitted in these Terms of Service;</li>
                            <li>access the <strong>CompareIP</strong> service to upload anything to or otherwise cause
                                a breach of security to or interfere with the <strong>CompareIP</strong> service or to
                                prevent others from using the <strong>CompareIP</strong> service;</li>
                            <li>solicit login information or access an account belonging to another User without
                                authority;</li>
                            <li>use the <strong>CompareIP</strong> service to do anything unlawful, misleading,
                                malicious, or discriminatory;</li>
                            <li>facilitate or encourage abuse of the <strong>CompareIP</strong> service by others</li>
                        </ul>
                        <br>

                        <h6>Sanctions for abuse of the CompareIP service</h6>
                        PACL reserves the right in its absolute discretion to:
                        <ul class="list-style-type-disc">
                            <li> remove your right to use the <strong>CompareIP</strong> service (refunding
                                any subscription paid pro rata)</li>
                            <li>pass on third party User reports of your conduct to the appropriate authorities</li>
                            <li>initiate proceedings against you seeking injunctive relief and damages in respect of any
                                abuse that has caused or threatens to cause damage to the <strong>CompareIP</strong>
                                service or to PACL.</li>
                        </ul>
                        <br>

                        <h4>Disputes and Indemnity</h4>
                        <h6>Disputes</h6>
                        <p>You will resolve any claim, cause of action or dispute (&quot;claim&quot;) you have
                            with us arising out of or relating to these Terms of Service or PACL exclusively in
                            the Courts of England. English law will govern these Terms of Service, as well as any claim
                            that might arise between you and us, without regard to conflict of law provisions. You
                            agree to submit to the personal jurisdiction of the English Courts for the purpose of
                            litigating all such claims.</p>

                        <h6>Indemnity</h6>
                        <p>If anyone brings a claim against us related to content you have published or actions
                            coordinated using the <strong>CompareIP</strong> service you will indemnify and hold us
                            harmless from and against all damages, losses, and expenses of any kind (including
                            reasonable legal fees and costs) related to such claim.</p>

                        <h4>Payment for services</h4>
                        <p>Clients who are IP owners may, at the discretion of PACL, use the <strong>CompareIP</strong>
                            service free of charge. Suppliers must pay an annual fee determined by PACL. Suppliers much
                            also pay a fee for each Quote request to which they respond on the basis of a schedule of
                            charges set by PACL. Payment must be made to PACL (who will invoice and collect payment for
                            use of <strong>CompareIP</strong>) or such other payment methods as may from time to time
                            be provided for by the <strong>CompareIP</strong> service. If Suppliers do not pay charges
                            due, then their use of the <strong>CompareIP</strong> service may be terminated or
                            suspended at the discretion of PACL.</p>

                        <h4>Exclusion of liability for loss</h4>
                        <p>To the extent permitted by applicable laws, no liability is accepted for any direct,
                            indirect or consequential loss or damage or loss of use, data, business opportunity or
                            profits, whether in an action in contract, negligence or other claim, however resulting
                            from the access to and use of this website and the information and materials contained
                            on it (but for the avoidance of doubt this clause does not exclude liability for death
                            or personal injury).</p>
                        <p>YOUR USE OF THE COMPAREIP SERVICE IS AT YOUR SOLE RESPONSIBILITY AND RISK. THE
                            COMPAREIP SERVICE IS PROVIDED ON AN &quot;AS IS&quot; AND &quot;AS AVAILABLE&quot; BASIS.
                            PACL AND ITS SUCCESSORS, AFFILIATES, CONTRACTORS, EMPLOYEES, SUPPLIERS, LICENSORS, PARTNERS
                            AND AGENTS DISCLAIM ANY WARRANTY THAT THE COMPAREIP SERVICE WILL MEET YOUR REQUIREMENTS OR
                            BE UNINTERRUPTED, TIMELY, SECURE, OR ERROR-FREE, THAT DEFECTS WILL BE CORRECTED, OR THAT
                            THE COMPAREIP SERVICE OR THE SERVER THAT MAKES THE COMPAREIP SERVICE AVAILABLE IS FREE OF
                            VIRUSES OR OTHER HARMFUL COMPONENTS. ANY MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH
                            THE USE OF THE COMPAREIP SERVICE IS DONE AT YOUR OWN DISCRETION AND RISK AND YOU WILL BE
                            SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR OTHER DEVICE OR LOSS OF DATA
                            THAT RESULTS FROM THE DOWNLOAD OF ANY SUCH MATERIAL. NO ADVICE OR INFORMATION, WHETHER ORAL
                            OR WRITTEN, OBTAINED BY YOU FROM PACL OR ITS SUCCESSORS, AFFILIATES, CONTRACTORS, EMPLOYEES,
                            SUPPLIERS, LICENSEES, PARTNERS OR AGENTS, OR THROUGH OR FROM THE COMPAREIP SERVICE SHALL
                            CREATE ANY WARRANTY NOT EXPRESSLY STATED IN THESE TERMS OF SERVICE.

                        <h4>Amendments to these Terms of Service</h4>
                        <p>We may amend, modify, change, add or remove portions of this Agreement at any time without
                            notice to you by posting a revised version on www.CompareIP.com. The revised version will
                            be effective at the time we post it. Please check this Agreement periodically for changes.
                            Your continued use of the <strong>CompareIP</strong> service after posting of the changes
                            constitutes your binding acceptance of such changes. We last modified this Agreement on
                            the date stated above. However, if the revised version includes a material change, it will
                            be effective for an existing User on the earlier of (a) the date you accept it, and (b) 30
                            days after the material changes are initially posted to www.CompareIP.com. The revised
                            version will apply to you immediately if you are a User who registers or first uses the
                            <strong>CompareIP</strong> service on or after the posting of the revised version.</p>

                        <h4>Data Protection and Privacy</h4>
                        <p>No personal information entered by a User in the course of registration is disclosed by PACL
                            to third parties as a matter of policy. No personal information collected in the course of
                            the payment process is used by PACL a matter of policy unless required for the purposes of
                            dealing with payments that have been dishonoured or made fraudulently. Users themselves are
                            responsible for keeping confidential such personal information as is made available to them
                            by other Users in the course of their use of the <strong>CompareIP</strong> service.</p>

                        <h4>Interpretation</h4>
                        <p>Headings do not form part of these terms and conditions and will not affect their
                            interpretation.</p>

                        <h4>Ownership of trademarks and other IP rights on the CompareIP service</h4>
                        <p>The trademarks, logos, names and images displayed on this website are the registered or
                            unregistered trademarks of PACL and others. Except where expressly stated to the contrary,
                            nothing in this website confers any licence or right to use any trade mark displayed on this
                            website without the prior written approval of PACL or such third-party owner of the relevant
                            trade mark.</p>

                        <h4>Applicable laws</h4>
                        <p>These terms are governed and construed in accordance with the laws of England. If any of the
                            terms is held to be unlawful, void or for any reason unenforceable, then that provision
                            will be deemed severable and will not affect the validity and enforceability of the
                            remaining terms. You agree to submit to the non-exclusive jurisdiction of the English
                            Courts.</p>

                        <ul>
                            <li><strong>Registered Name:</strong> Patent Annuity Costs Limited</li>
                            <li><strong>Registered Office:</strong> The Barn Office, Longcombe Valley Farm, Totnes,
                                Devon TQ9 6PP</li>
                            <li><strong>Registered in England No:</strong> 9323266</li>
                        </ul>
                        <br>

                    </div>
                </div>
            </div>
        </div>
        <!-- team section end -->
    </section>

@endsection