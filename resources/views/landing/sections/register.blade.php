<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div class="row">
    <div class="col-12">

        <div class='bold-line'></div>
        <div class='container-reg-form-home'>
            <div class='windows container'>
                <div class='overlay'></div>
                <div class='content'>
                    <div class='welcome welcomes'>Register</div>
                    <div class='subtitle subtitles'>Before using our service you need to create an account.</div>
                    <form method="post" action="{{ route('register') }}">

                        {{ csrf_field() }}

                       <div class="row">
    <div class="col-sm-3 col-xs-12 reg_name required">First Name</div>
    <div class="col-sm-3 col-xs-12 reg_name required">Last Name</div>
    <div class="col-sm-3 col-xs-12 reg_name required">Organisation</div>
    <div class="col-sm-3 col-xs-12 reg_name required">Email</div>
   </div>
                              <div class="row">
                           <div class="col-sm-3 col-xs-12">
                               <input id="first_name"
                                   name="first_name"
                                   value="{{ old('first_name') }}"
                                   type='text'
                                   placeholder='First name'
                                   class='input-line full-width input-lines full-widths

                                    @if ($errors->has('first_name'))
                                   {{ 'error' }}
                                   @endif
                                           '

                            @if ($errors->has('first_name'))
                                {{ 'autofocus' }}
                                    @endif
                            />
                                     </div>
                            <div class="col-sm-3 col-xs-12">
                                   <input
                                    name="last_name"
                                    value="{{ old('last_name') }}"
                                    type='text'
                                    placeholder='Last name'
                                    class='input-line full-width input-lines full-widths

                                    @if ($errors->has('last_name'))
                                    {{ 'error' }}
                                    @endif
                                            '

                            @if ($errors->has('last_name'))
                                {{ 'autofocus' }}
                                    @endif
                            />
                            </div>
                            <p class="antispam">Leave this empty: <input type="text" name="mobile" /></p>
                          <div class="col-sm-3 col-xs-12"> 
                                <input type='text'
                                   name="organisation"
                                   value="{{ old('organisation') }}"
                                   placeholder='Organisation'
                                   class='input-line full-width input-lines full-widths

                                    @if ($errors->has('organisation'))
                                   {{ 'error' }}
                                   @endif
                                           '

                            @if ($errors->has('organisation'))
                                {{ 'autofocus' }}
                                    @endif
                            />
                               </div>
                     <div class="col-sm-3 col-xs-12"> 
                                    <input
                                    name="email"
                                    value="{{ old('email') }}"
                                    type='email'
                                    placeholder='Email'
                                    class='input-line full-width input-lines full-widths

                                    @if ($errors->has('email'))
                                    {{ 'error' }}
                                    @endif

                                            '

                            @if ($errors->has('email'))
                                {{ 'autofocus' }}
                                    @endif
                            />
                        </div>
    
                       </div>
                    
                       
                    <div class="form-group row">
                    <div class="col-sm-8 col-xs-12"> 
                        <div class="g-recaptcha" data-sitekey="6LcDp7YUAAAAAHU4mOJHba3GFg-FX8Pvi-qe2TTs"></div>
                        @if ($errors->has('g-recaptcha-response'))
                            <span class="invalid-feedback" style="display: block;">
                                <strong class="g-recaptcha" >{{ $errors->first('g-recaptcha-response') }}</strong>
                            </span>
                        @endif
                    </div>
                    </div>
                       <div class="row reg_btn">
                           <div class="col-sm-4 col-xs-12"> </div>
                            <div class="col-sm-4 col-xs-12">  
                            <div>
                            <button type="submit" class='ghost-round full-width register-now register_btn'>Register Now</button>
                          </div>
                          </div>
                             <div class="col-sm-4 col-xs-12"> </div>
                        </div>
                         <div class="forgot-pass-container frgt_pass">
                            <a href="{{ url('password/reset') }}">
                                Forgot password?
                            </a>
                        </div>
                    </form>

                </div>
            </div>
        </div>

    </div>
</div>
