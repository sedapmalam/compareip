<header class="header-area">
    <div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-8">
                    <div class="header-top-left">
                        <ul class="email-phone">
                            <li>
                                <a href="mailto:{{ Config::get('app.supportEmail')  }}">
                                    <i class="fa fa-envelope"></i> <span class="hidden-xs">Email:</span>
                                    <span class="text-bold">{{ Config::get('app.supportEmail')  }}</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4">
                    <div class="header-top-right">
                        <ul class="header-social-menu">
                            {{--<li>--}}
                                {{--<a href="#">--}}
                                    {{--<i class="fa fa-facebook"></i>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            <li>
                                <a href="#">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            {{--<li>--}}
                                {{--<a href="#">--}}
                                    {{--<i class="fa fa-instagram"></i>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#">--}}
                                    {{--<i class="fa fa-google-plus"></i>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            <li>
                                <a href="#">
                                    <i class="fa fa-linkedin-square"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-header-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="logo-wrapper">
                        <a class="logo" href="{{route('home')}}">
                            CompareIP
                        </a>
                    </div>
                </div>
                <div class="col-md-9 hidden-xs hidden-sm">
                    <div class="menu-area">
                        <nav class="template-menu">
                            <ul class="main-menu">
                                <li>
                                    <a href="{{route('home')}}">Home
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('contact')}}">Contact</a>
                                </li>
                                  <li>
              <a href="http://blog.compareip.com">BLOG</a>
                                </li>
                                <li>
                                    <a href="{{route('faq')}}">FAQ</a>
                                </li>
                                <li>
                                    <a href="{{route('login')}}">Login</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- Mobile menu area start -->
                <div class="mobile-menu-area clearfix hidden-md">
                    <nav class="mobile-menu">
                        <ul class="mobile-menu-nav">
                            <li>
                                <a href="{{route('home')}}">Home</a>
                            </li>
                            <li>
                                <a href="{{route('contact')}}">contact</a>
                            </li>
                            <li>
                                <a href="{{route('faq')}}">FAQs</a>
                            </li>
                            <li>
                                <a href="{{route('login')}}">Login</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <!-- Mobile menu area end -->
            </div>
        </div>
    </div>
</header>