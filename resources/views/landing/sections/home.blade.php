<div class="top-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p>
                    <span class="main-text">welcome to CompareIP</span>
                </p>
                <h1 class="main-text">Renewals cost comparison, made&nbsp;easy</h1>
                <p>Using our free service you can compare costs quickly and anonymously.
                    <br>
                    <br>
                    Register, upload details of upcoming renewals, and invite chosen suppliers to
                    quote. Your&nbsp;information is kept confidential and your identity is not disclosed
                    to suppliers. Results&nbsp;received&nbsp;will be presented to you in a consolidated view
                    and can be downloaded.
                </p>
                <button class="btn btn-lg btn-primary register-lending-button">REGISTER NOW</button>
            </div>
            <div class="col-md-6">
                <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/277067994?autoplay=1&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>            </div>
        </div>
    </div>
</div>
