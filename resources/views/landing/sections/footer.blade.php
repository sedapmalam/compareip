<footer class="template-footer-section">

    <div class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-7">
                    <div class="footer-copyright-info">
                        <ul class="footer-social-menu text-left">
                            <li>
                                <a href="{{route('terms')}}">Terms of Service
                                </a>
                            </li>
                            <li>
                                <a href="{{route('policy')}}">Privacy Policy</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-sm-5">
                    <div class="footer-bottom-menu">
                        <ul class="footer-social-menu text-right">

                            <li>
                                <a href="#">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-linkedin-square"></i>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>