<div class="template-slider-area">
    <div class="slider-wrapper navigation-one">
        <div class="single-slider slider-1 slider-gradient-1">
            <div class="single-slider-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="slider-content-wrapper text-left">
                                <div class="slider-content">
                                    <p>
                                        <span>welcome to Bizpro</span>
                                    </p>
                                    <h1>We are creative.</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur elit. Sunt illo natus recusandae. Ex
                                        nihil et nemo sapiente culpa earum sint consectetur adipisicing elit.</p>
                                    <div class="slider-button">
                                        <a class="button btn-white" href="#">learn more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="single-slider slider-2 slider-gradient-2">
            <div class="single-slider-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="slider-content-wrapper">
                                <div class="slider-content text-center">
                                    <p>
                                        <span>welcome to Bizpro</span>
                                    </p>
                                    <h1>Bizpro multipurpose template</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit, commodi? Optio,
                                        saepe modi dolores ipsam nulla distinctio laborum aut cum.Optio, saepe modi
                                        dolores ipsam nulla distinctio laborum aut cum</p>
                                    <div class="slider-button">
                                        <a class="button" href="#">contact us</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>