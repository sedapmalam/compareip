@extends('layouts.landing')

@section('content')


    <section class="content">

        <!-- breadcrumb end -->
        <!-- team section start -->
        <div class="template-section team-area-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="area-heading text-center success-box">
                            <h2 class="area-title">suspended</h2>

                            <p>Your account has been suspended, probably because fees due have not been paid.  While your account is suspended your profile will be hidden from users; you will not receive quote requests; and you will not be able to respond to quote requests already received.</p>

                            <p>To discuss reinstatement of your account please contact peter.rouse@pacipr.com</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- team section end -->
    </section>


@endsection
