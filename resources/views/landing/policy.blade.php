@extends('layouts.landing')

@section('content')

    <section class="content">
        <!-- breadcrumb start -->
        <div class="template-section grd-1 breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <h3 class="breadcrumb-title">Privacy Policy</h3>
                    </div>
                    <div class="col-xs-6">
                        <div class="breadcrumb-wrap">
                            <ul class="breadcrumb-list">
                                <li>
                                    <a href="{{ route('home') }}">Home </a>
                                </li>
                                <li>
                                    <a href="{{ route('policy') }}">Privacy Policy </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb end -->
        <!-- team section start -->
        <div class="template-section team-area-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <strong>Dated: 25 May 2018</strong>

                        <p>Patent Annuity Costs Limited operates the service at
                            <a href="https://compareip.com">https://compareip.com</a> subject to the Terms
                            of Service (the &prime;Service&prime;).</p>

                        <p>This page informs visitors and users (including Users and Suppliers) regarding our policies
                            with regard to the collection, use, and disclosure of personal information.  If you choose
                            to use the Service, then you agree to the collection and use of information in accordance
                            with this policy.  We will not use or share your information with anyone except as
                            described in this Privacy Policy.</p>

                        <p>The terms used in this Privacy Policy have the same meanings as in our Terms of Service,
                            which are accessible at [link], unless otherwise defined in this Privacy Policy.</p>

                        <h4>Information Collection and Use</h4>
                        <p>When registering to use our Service, in the subsequent verification process and in the
                            course of providing the Service we will require you to provide us with certain personal
                            information. The information that we collect will be used only to contact or identify you.</p>

                        <p>The following types of personal information may be collected, stored, and used:</p>
                        <ul class="list-style-type-disc">
                            <li>Contact Details – name; email address; organisation name; role or title; and
                                phone number.</li>
                            <li>Account Details – username and password.</li>
                            <li>Location Details - physical address and time zone.</li>
                        </ul>
                        <br>

                        <h4>Using Your Personal Information</h4>
                        <p>Personal information submitted to us through our website will be used for the purposes
                            specified in this policy.  We may use your personal information for the following:</p>

                        <ol class="list-style-type-decimal">
                            <li>administering our website and business, including use of the live chat service
                                provided on our website;</li>
                            <li>personalizing our website for you;</li>
                            <li>enabling and supporting your use of the Service;</li>
                            <li>sending you non-marketing commercial email communications;</li>
                            <li>sending you email notifications that you have specifically requested;</li>
                            <li>sending you our email newsletter, if you have requested it (you can inform us
                                at any time if you no longer require the newsletter);</li>
                            <li>sending you marketing communications relating to our business (you can inform us at any
                                time if you no longer require marketing communications);</li>
                            <li>providing third parties with statistical information about our users (but those third
                                parties will not be able to identify any individual user from that information);</li>
                            <li>dealing with inquiries and complaints made by or about you relating to our Service;</li>
                            <li>keeping our website secure and preventing fraud;</li>
                            <li>verifying compliance with the Terms of Service governing the use of our website;</li>
                            <li>for other purposes with your consent, unless you withdraw your consent for such
                                purposes.</li>
                        </ol>

                        <p>The &prime;lawful processing&prime; grounds on which we will use personal information
                            about our users are (but are not limited to):</p>
                        <ul class="list-style-type-disc">
                            <li>when a user has given consent;</li>
                            <li>when necessary for the performance of a contract to which the user is party;</li>
                            <li>processing is necessary for compliance with our legal obligations;</li>
                        </ul>
                        <br>

                        <h4>Disclosing Personal Information</h4>
                        <p>We may disclose your personal information to any of our employees, officers, insurers,
                            professional advisers, agents, or subcontractors as reasonably necessary for
                            the purposes set out in this policy.</p>

                        <h4>Security of Your Personal Information</h4>
                        <ol class="list-style-type-decimal">
                            <li>We will take reasonable technical and organizational precautions to prevent the loss,
                                misuse, or alteration of your personal information;</li>
                            <li>We will store all the personal information you provide on our secure servers;</li>
                            <li>You acknowledge that the transmission of information over the internet is inherently
                                insecure, and we cannot guarantee the security of data sent over the internet.</li>
                            <li>You are responsible for keeping the password you use for accessing our
                                website confidential;</li>
                            <li>You have the right to access your data, rectify any incorrect data, or request
                                your data deleted by emailing
                                <a href="mailtoi:support@compareip.com">support@compareip.com</a></li>
                        </ol>

                        <strong>End</strong>

                    </div>
                </div>
            </div>
        </div>
        <!-- team section end -->
    </section>

@endsection