@extends('layouts.landing')

@section('content')


    <section class="content">
        <!-- team section start -->
        <div class="template-section team-area-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="area-heading text-center success-box">
                            <h2 class="area-title">success</h2>

                            <p>Thanks for registering to use CompareIP.  We will be in touch shortly to confirm your authorization to use our platform and give you access to your private dashboard.  </p>

                            <p>If we can be of assistance in the meantime, please use the live chat service on our website or email support@CompareIP.com and we will do all we can to help.</p>

                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- team section end -->
    </section>


@endsection
