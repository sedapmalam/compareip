@extends('layouts.landing')

@section('content')


    <section class="content">
        <!-- breadcrumb start -->
        <div class="template-section grd-1 breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <h3 class="breadcrumb-title">FAQ</h3>
                    </div>
                    <div class="col-xs-6">
                        <div class="breadcrumb-wrap">
                            <ul class="breadcrumb-list">
                                <li>
                                    <a href="{{ route('home') }}">Home </a>
                                </li>
                                <li>
                                    <a href="{{ route('faq') }}">FAQ </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb end -->
        <!-- team section start -->
        <div class="template-section team-area-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <dl>
                            <dt>Is the service free to use for IP owners?</dt>
                            <dd>Yes</dd>
                        </dl>
                        <dl>
                            <dt>What information has to be provided during the registration process?</dt>
                            <dd>We need to be sure that only bona fide IP owners are authorised to use our platform.  You will be contacted by our customer service team and asked to provide a breakdown of your portfolio of registered IP rights (i.e. how many patents, trademarks and designs you own).</dd>
                        </dl>


                        <dl>
                            <dt>Is the service confidential?</dt>
                            <dd>Yes.  Your identity is not disclosed to Suppliers, or to any third party.  The information you provide to Suppliers is deliberately limited.  The indicative pricing you receive from Suppliers is also confidential, so you can’t share it outside your business.</dd>
                        </dl>

                        <dl>
                            <dt>Can we contact Suppliers direct?</dt>
                            <dd>Yes, any time you like.</dd>
                        </dl>

                        <dl>
                            <dt>Is support provided?</dt>
                            <dd>Yes.  Use the live chat service on the website or email <a href="mailto:support@compareip.com">support@compareip.com</a></dd>
                        </dl>

                        <dl>
                            <dt>Is there a limit on the number of renewals for which a quote can be requested?</dt>
                            <dd>The limit is 100 individual renewals.</dd>
                        </dl>

                        <dl>
                            <dt>What is the best way to get renewals data into a Request?</dt>
                            <dd>The best method is to download the template, input your data and then upload.</dd>
                        </dl>

                        <dl>
                            <dt>What renewals data should be used?</dt>
                            <dd>We recommend using your most recent invoice, or renewal notice.</dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
        <!-- team section end -->
    </section>


@endsection
