@extends('layouts.landing')

@section('content')
    <!-- Slider area start -->
    @include('landing.sections.home')
    <!-- Slider area end -->

    @include('landing.sections.register')
    <!-- Page content area start -->
    <section class="content">

        <!-- template about section -->
        <div class="template-section about-section gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="about-content-wrapper">
                            <div class="area-heading about-area-heading text-left">
                                <h2 class="area-title">Many IP owners are paying too much for renewals</h2>
                                <p>This service has been created in response to huge disparities in charging between
                                    suppliers of renewal payment services. IP owners can use this platform free of
                                    charge, and without disclosing their identities to suppliers, to get a quick idea of
                                    what others would charge for the same service. The pricing provided is indicative
                                    and not a formal quotation.
                                    <br>
                                    <br>
                                    <span class="font-span">Choosing a supplier</span>
                                    <br>
                                    This service is designed to help you to decide who you might want to talk to in more
                                    detail about handling your renewals.
                                    <br>
                                    <br>
                                    Price is a major factor in choosing a supplier. However, there are many other
                                    considerations that should be taken into account, including:
                                    <br>
                                <ul>
                                    <li> - How transition would be handled</li>
                                    <li> - Tools for viewing the status of renewals, giving instructions and monitoring
                                        costs
                                    </li>
                                    <li> - Other services the supplier is able to offer that you might need to call on</li>
                                </ul>
                                <br>
                                The suppliers who have agreed to participate in this service favour choice and transparency. There will be price differences between them.  There will also be differences in the way they work.  The choice is yours.
                                <br>
                                <br>
                                If you would like to join <a href="https://compareip.com/">CompareIP.com</a> as a Supplier please get in touch using the contact form.
                                <br>
                                <br>
                                <span class="font-span">Monitoring compliance – Renewals Audit Tool</span>
                                <br>
                                IP renewals are a costly necessity, whether managed in-house and through your own agent network or through a specialist provider.  In both situations, you need to be sure that fees invoiced match the terms agreed with your suppliers.  
                                <br>
                                Hosted securely on your SharePoint or equivalent file sharing platform, our app provides a quick and simple means of checking renewal invoices. It allows you to input agreed local agent fees and other charges (such as FX mark-ups) and then check invoice items in seconds, calculating what the correct amount should be and showing you correct Official Fees and professional fees.  There is also a forecasting tool that allows you to create cost projections, making budget planning a breeze.

                                <br>
                                <br>
                                <a target="_blank"  style="color: #0b8ddd" class="font-span" href="http://www.pacipr.com/services">Read More</a>
                                </p>
                            </div>
					</div>
                    </div>
                    <div class="col-md-6">
						 <div style="padding:56.25% 0 0 0;position:relative;">
                            <iframe src="https://player.vimeo.com/video/293502908"
                                    style="position:absolute;top:0;left:0;width:100%;height:100%;"
                                    frameborder="0" webkitallowfullscreen
                                    mozallowfullscreen
                                    allowfullscreen>
                            </iframe>
                             </div>
                         <script src="https://player.vimeo.com/api/player.js"></script>
                      </div>
                </div>
            </div>
        </div>
        <!-- template about section -->
        <!-- contact information section end -->
    </section>
    <!-- Page content area end -->
@endsection
