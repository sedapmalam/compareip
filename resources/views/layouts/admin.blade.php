<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>CompareIP</title>
    <meta content="Admin Dashboard" name="description" />
    <meta content="ThemeDesign" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('/img/icons/favicon.ico')}}" type="image/x-icon">

    <!--Morris Chart CSS -->
    <link rel="stylesheet" href={{ asset('/admin/plugins/morris/morris.css') }}>

    <link href={{ asset('/admin/css/bootstrap.min.css') }} rel="stylesheet" type="text/css">
    <link href={{ asset('/admin/css/icons.css') }} rel="stylesheet" type="text/css">
    <link href={{ asset('/admin/css/style.css') }} rel="stylesheet" type="text/css">

    {{--Request page--}}
    <link href="{{ asset('/admin/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/admin/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css') }}"
          rel="stylesheet"/>
    <!-- DataTables -->
    <link href="{{ asset('/admin/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/admin/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Responsive datatable examples -->
    <link href="{{ asset('/admin/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <!-- Sweet Alert -->
    <link href="{{ asset('/admin/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css">
    {{--Request page--}}

<!-- Dropzone css -->
    <link href="{{ asset('/admin/plugins/dropzone/dist/dropzone.css') }}" rel="stylesheet" type="text/css">
    <link href="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
    <link href="{{ asset('/js/jquery-ui.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/js/jquery-ui.structure.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/js/jquery-ui.structure.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/js/jquery-ui.theme.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/js/jquery-ui.theme.css') }}" rel="stylesheet" type="text/css">
    <script src={{ asset('/js/app.js') }}></script>
    <script src={{ asset('/js/jquery-ui.min.js') }}></script>
    <script src={{ asset('/js/jquery-ui.js') }}></script>
</head>


<body class="fixed-left">

<!-- Loader -->
<div id="preloader"><div id="status"><div class="spinner"></div></div></div>

<!-- Begin page -->
<div id="wrapper">

    <!-- ========== Left Sidebar Start ========== -->
    @include('admin.sections.menu')
    <!-- Left Sidebar End -->

    <!-- Start right Content here -->

    <div class="content-page">
        <!-- Start content -->
        <div class="content">

            <!-- Top Bar Start -->
            @include('admin.sections.topbar')
            <!-- Top Bar End -->

            <div class="page-content-wrapper ">

                @yield('content')

            </div> <!-- Page content Wrapper -->

        </div> <!-- content -->

        {{--@include('admin.sections.footer')--}}

    </div>
    <!-- End Right content here -->

</div>
<!-- END wrapper -->


<!-- jQuery  -->
<script src={{ asset('/admin/js/jquery.min.js') }}></script>
<script src={{ asset('/admin/js/popper.min.js') }}></script>
<script src={{ asset('/admin/js/bootstrap.min.js') }}></script>
<script src={{ asset('/admin/js/modernizr.min.js') }}></script>
<script src={{ asset('/admin/js/detect.js') }}></script>
<script src={{ asset('/admin/js/fastclick.js') }}></script>
<script src={{ asset('/admin/js/jquery.slimscroll.js') }}></script>
<script src={{ asset('/admin/js/jquery.blockUI.js') }}></script>
<script src={{ asset('/admin/js/waves.js') }}></script>
<script src={{ asset('/admin/js/jquery.nicescroll.js') }}></script>
<script src={{ asset('/admin/js/jquery.scrollTo.min.js') }}></script>
<script src="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.js"></script>
<script src={{ asset('/admin/js/bootstrap.file-input.js') }}></script>
<!--Morris Chart-->
<script src={{ asset('/admin/plugins/morris/morris.min.js') }}></script>
<script src={{ asset('/admin/plugins/raphael/raphael-min.js') }}></script>

{{--<script src={{ asset('/admin/pages/dashborad.js') }}></script>--}}

<!-- App js -->
<script src={{ asset('/admin/js/app.js') }}></script>


{{--Request page--}}
<script src="{{ asset('/admin/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
<script src="{{ asset('/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('/admin/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/admin/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js') }}" type="text/javascript"></script>

<!-- Plugins Init js -->
<script src="{{ asset('/admin/pages/form-advanced.js') }}"></script>

<!-- Required datatable js -->
<script src="{{ asset('/admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/admin/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

<script src="{{ asset('/admin/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('/admin/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('/admin/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('/admin/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('/admin/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('/admin/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('/admin/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('/admin/plugins/datatables/buttons.colVis.min.js') }}"></script>

<!-- Responsive examples -->
<script src="{{ asset('/admin/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('/admin/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

<!-- Datatable init js -->
<script src="{{ asset('/admin/pages/datatables.init.js') }}"></script>

<!-- Sweet-Alert  -->
<script src="{{ asset('/admin/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('/admin/pages/sweet-alert.init.js') }}"></script>


<script src={{ asset('/admin/pages/request.js') }}></script>
<script src={{ asset('/admin/pages/excel.js') }}></script>

{{--Request page--}}

{{--User page--}}
<script src={{ asset('/admin/js/user.js') }}></script>
{{--User page--}}

<!-- Dropzone js -->
<script src="{{ asset('/admin/plugins/dropzone/dist/dropzone.js') }}"></script>


{{--<!-- BEGIN JIVOSITE CODE {literal} -->--}}
{{--<script type='text/javascript'>--}}
    {{--(function(){ var widget_id = '6OHItOqG06';var d=document;var w=window;function l(){--}}
        {{--var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>--}}
{{--<!-- {/literal} END JIVOSITE CODE -->--}}


</body>
</html>
