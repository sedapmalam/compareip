<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CompareIP</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('/img/icons/favicon.ico')}}" type="image/x-icon">
    <!-- All CSS files included -->
    <link rel="stylesheet" href=" {{ asset('/landing/css/elements.css')}} ">
    <link rel="stylesheet" href="{{ asset('/landing/style.css')}}">
    {{--<link rel="stylesheet" href="{{ asset('/landing/css/responsive.css')}}">--}}
    <link rel="stylesheet" href="{{ asset('/landing/css/registration.css')}}">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120529357-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-120529357-1');
    </script>
    <script src="{{ asset('/landing/js/vendor/modernizr-2.8.3.min.js')}}"></script>
</head>

<body>
@include('cookieConsent::index')
<!-- Main wrapper start -->
<div class="wrapper">

    <!-- Header area start -->
    @include('landing.sections.header')
    <!-- Header area end -->

    @yield('content')

</div>

<!-- template footer section start -->
@include('landing.sections.footer')
<!-- template footer section end -->

<!-- Main wrapper end -->
<!-- All JavaScript files included -->
<script src="{{ asset('/landing/js/vendor/jquery-1.12.4.min.js')}}"></script>
<script src="{{ asset('/landing/js/plugins.js')}}"></script>
<script src="{{ asset('/landing/js/scripts.js')}}"></script>

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
    (function(){ var widget_id = 'vVRbpOP1uE';var d=document;var w=window;function l(){
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->
<script type="text/javascript">(function(a,l,b,c,r,s){_nQc=c,r=a.createElement(l),s=a.getElementsByTagName(l)[0];r.async=1;r.src=l.src=("https:"==a.location.protocol?"https://":"http://")+b;s.parentNode.insertBefore(r,s);})(document,"script","serve.albacross.com/track.js","89541623");</script>
</body>

</html>